﻿###################################################
# Reinstall Local Running RPOS Auto-Updater Tasks #
###################################################


#Two Digit Store Number
$TwoDigPCName = "$ENV:COMPUTERNAME"

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]00') {
	$TwoDigPCName = $TwoDigPCName.Substring(6, 2) }

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]NPC') {
	$TwoDigPCName = $TwoDigPCName.Substring(7, 2) }

$task1 = schtasks /query /TN "Update RPOS Local A"
$task2 = schtasks /query /TN "Update RPOS Local B"

if ($task1 -eq $null)
{
	schtasks /delete /TN "Update RPOS Local A" /F
	
	if ($TwoDigPCName -eq "01") {
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/Update RPOS Local A PC1.xml" }
	
	if ($TwoDigPCName -eq "02") {
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/Update RPOS Local A PC2.xml" }
	
	if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02")) {
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/Update RPOS Local A PC3.xml" }
	
	Invoke-WebRequest -Uri $URL -OutFile "C:\Windows\Scripts\RPOS\update\Update RPOS Local A.xml"
	schtasks /create /RU "TBC\shprorpos" /RP "z6BX4564%C2#X9T!" /TN "Update RPOS Local A" /XML "C:\Windows\Scripts\RPOS\update\Update RPOS Local A.xml"
	icacls "C:\Windows\System32\Tasks\Update RPOS Local A" /grant Everyone:RX
	Remove-Item "C:\Windows\Scripts\RPOS\update\Update RPOS Local A.xml" -Force
}

if ($task2 -eq $null)
{
	schtasks /delete /TN "Update RPOS Local B" /F
	
	if ($TwoDigPCName -eq "01") {
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/Update RPOS Local B PC1.xml" }
	
	if ($TwoDigPCName -eq "02") {
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/Update RPOS Local B PC2.xml" }
	
	if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02")) {
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/Update RPOS Local B PC3.xml" }
	
	Invoke-WebRequest -Uri $URL -OutFile "C:\Windows\Scripts\RPOS\update\Update RPOS Local B.xml"
	schtasks /create /RU "TBC\shprorpos2" /RP "k;SW%gILuhSfZbS/" /TN "Update RPOS Local B" /XML "C:\Windows\Scripts\RPOS\update\Update RPOS Local B.xml"
	icacls "C:\Windows\System32\Tasks\Update RPOS Local B" /grant Everyone:RX
	Remove-Item "C:\Windows\Scripts\RPOS\update\Update RPOS Local B.xml" -Force
	schtasks /change /disable /TN "Update RPOS Local B"
}