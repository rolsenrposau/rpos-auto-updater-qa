﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.135
	 Created on:   	12/4/2017
	 Created by:   	TBC\rolsen
	 Organization: 	TBC Corporation
	 Filename:     	UninstallRPOSAutoUpdater.exe
	===========================================================================
	.DESCRIPTION
		This Powershell script will uninstall RPOS Auto-Updater
		and everything that is assoicated with it.
#>

net localgroup administrators TBC\shprorpos /delete
net localgroup administrators TBC\shprorpos2 /delete
schtasks /delete /TN "Rollback RPOS Local" /F
schtasks /delete /TN "RPOS AU updater" /F
schtasks /delete /TN "Update RPOS Clear Logs" /F
schtasks /delete /TN "Update RPOS Local A" /F
schtasks /delete /TN "Update RPOS Local B" /F
schtasks /delete /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix" /F
net share cs18ebyi3$ /delete
Remove-Item "C:\Windows\System32\Tasks\Microsoft\Windows\RPOS Auto-Updater" -Recurse -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Adobe DCount" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "FlowModule Checksum" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Checksum" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version Label" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUVer" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeAIRVer" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Adobe DCount" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoVersion" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoXMLchecksum" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckInternet" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckAdobeAIR" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUAdobeAIRNI" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUAdobeAIRInstalling" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckBGInfoEMV" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckSelfSubDownload" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUClosingWindows" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUUninstallRPOS" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUInstallingRPOS" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckInstall" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUFailedLocal" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AULocalReinstall" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCompleteReinstall" -Force
Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUGatherReportingData" -Force

Remove-Item "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Recurse -Force
Remove-Item "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Recurse -Force
Remove-Item "C:\Windows\Scripts\RPOS\Bak" -Recurse -Force
Remove-Item "C:\Windows\Scripts\RPOS\build" -Recurse -Force
Remove-Item "C:\Windows\Scripts\RPOS\Logs" -Recurse -Force
Remove-Item "C:\Windows\Scripts\RPOS\update" -Recurse -Force
Remove-Item "C:\Windows\Scripts\RPOS\uninstall" -Recurse -Force
Remove-Item "C:\Users\Public\Desktop\Update RPOS.lnk" -Force
Remove-Item "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Recurse -Force
Remove-Item "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Recurse -Force
[Environment]::SetEnvironmentVariable("RPOSAutoUpdater", $null, "Machine")