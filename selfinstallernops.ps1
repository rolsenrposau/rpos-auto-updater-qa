﻿################Installer v1.6#######################

New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
$date = (get-date).ToString("yyyyMMdd-hhmmss")
$Log = "C:\Windows\Scripts\RPOS\Logs\RPOS_InstallLog_$date.txt"

$computerver = (Get-WmiObject -class Win32_OperatingSystem).Caption

#Checking to see if Powershell is the latest version
$PowershellKeys = "HKLM:\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine"
$PSVersioncheck = (Get-ItemProperty -Path $PowershellKeys -Name "PowerShellVersion")."PowerShellVersion"
$PSVersion = $PSVersioncheck.Substring(0, 1)
$newver = "3.8.1d"

#Setting up checking for processes
$processpend = 0

<#if ($PSversion -ge "5")
{
	Write-Output "Your version of Powershell is $PSVersion have a version that is higher than 4.0" | Out-File -Append -FilePath $Log
}
Else
{
	wusa.exe "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3134760-x64.msu" /quiet /norestart
	while ($processpend -lt 500)
	{
		$processpend += 1
		$ProcessActive = Get-Process wusa -ErrorAction SilentlyContinue
		if ($ProcessActive -eq $null)
		{
			Write-Output "Windows Update is not running" | Out-File -Append -FilePath $Log
			Start-Sleep -seconds 10
			schtasks /create /TN "After Reboot RPOS AU Install" /XML "C:\temp\installupdaterpos\Second Install.xml"
			Restart-Computer -Force
		}
		Else
		{
			Write-Output "Windows Update is running" | Out-File -Append -FilePath $Log
			Start-Sleep -seconds 10
		}
	}
}
#>
Write-Output "RPOS Installer v1.7 (c) EOC TBC Corporation" | Out-File -Append -FilePath $Log
$PCname = "$env:COMPUTERNAME"
$PCname2 = "$env:COMPUTERNAME"

if ($PCname -match 'S[0-9][0-9][0-9]00')
{
	$PCname = $PCname.Substring(6, 2)
}

if ($PCname -match 'S[0-9][0-9][0-9]NPC')
{
	$PCname = $PCname.Substring(7, 2)
}


Write-Output "$PCName2 is the number of this PC" | Out-File -Append -FilePath $Log

$LANDeskKeys = "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
$updatekey = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Auto-Updater")."RPOS Auto-Updater"
Write-Output "$updatekey is the version of Auto-Updater running on this PC" | Out-File -Append -FilePath $Log
$datereg = (Get-Date).ToString("MM/dd/yyyy")

New-Item "C:\temp\" -ItemType Directory -Force
New-Item "C:\temp\installupdaterpos" -ItemType Directory -Force


#Starting install for new PC's and/or have older versions
if ($updatekey -match $newver)
{
	Write-Output "Setup has detected that this PC has the most current version reinstalling newer files" | Out-File -Append -FilePath $Log
	
	Write-Output "Installer has started installing files for PC1" | Out-File -Append -FilePath $Log
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$newver" -Force
	
	
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value "$datereg" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUVer" -Value "$newver" -Force
	
	
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayVersion" -Value "$newver" -Force
	
	# Fixing Windows 10 Scheduled Tasks
	if ($computerver -match "Microsoft Windows 10")
	{
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/FixWindows10tasks.exe"
		$FixWin10TasksEXELocal = "C:\temp\installupdaterpos\FixWindows10tasks.exe"
		$Webclient = New-Object System.Net.WebClient
		$Webclient.DownloadFile($URL, $FixWin10TasksEXELocal)
		
		Start-Process -FilePath "C:\temp\installupdaterpos\FixWindows10tasks.exe" -Wait
	}
	
	$rposqaprofilefolder = Test-Path "C:\TBC_Scripts\RPOSAU"
	
	If (($rposqaprofilefolder -eq $false) -or (!$rposqaprofilefolder))
	{
		New-Item -Path "C:\TBC_Scripts" -ItemType Directory -Force
		New-Item -Path "C:\TBC_Scripts\RPOSAU" -ItemType Directory -Force
		Write-Output "http://rpos.tbccorp.com" | Out-File -FilePath "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
	}
	
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
	Copy-Item "C:\temp\installupdaterpos\UninstallRPOSAU.exe" "C:\Windows\Scripts\RPOS" -Force
	
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS Auto-Updater Autofix.xml"
	Remove-Item "C:\temp\installupdaterpos\BuildUninstallers.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Clear Logs.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Rollback RPOS Local.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3134760-x64.msu" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\quietuninstall.vbs" -Force
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.bat" -Force
	Remove-Item "C:\temp\installupdaterpos\Second Install.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\rposauaf.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\FixWindows10tasks.exe"
	Exit
}

if ($updatekey -notmatch $newver)
{
	
	Write-Output "Setup has detected that this PC an older version and/or new install." | Out-File -Append -FilePath $Log
	schtasks /Delete /TN "Update RPOS Local" /F
	schtasks /Delete /TN "Update RPOS Clear Logs" /F
	schtasks /Delete /TN "Rollback RPOS Local" /F
	schtasks /Delete /TN "Update RPOS Local A" /F
	schtasks /Delete /TN "Update RPOS Local B" /F
	schtasks /Delete /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix" /F
	schtasks /create /TN "Update RPOS Clear Logs" /XML "C:\temp\installupdaterpos\Update RPOS Clear Logs.xml"
	schtasks /create /TN "Rollback RPOS Local" /XML "C:\temp\installupdaterpos\Rollback RPOS Local.xml"
	net localgroup "Administrators" /add TBC\shprorpos
	net localgroup "Administrators" /add TBC\shprorpos2
	
	Set-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name EnableLUA -Value 0 -Type DWord
	Write-Output "Installer has started installing Scheduled Tasks for PC1" | Out-File -Append -FilePath $Log
	
	if ($computerver -notmatch "Microsoft Windows 10")
	{
		schtasks /create /RU "TBC\shprorpos" /RP "z6BX4564%C2#X9T!" /TN "Update RPOS Local A" /XML "C:\temp\installupdaterpos\Update RPOS Local A PC1.xml"
		schtasks /create /RU "TBC\shprorpos2" /RP "k;SW%gILuhSfZbS/" /TN "Update RPOS Local B" /XML "C:\temp\installupdaterpos\Update RPOS Local B PC1.xml"
		schtasks /change /disable /TN "Update RPOS Local B"
	}
	
	schtasks /create /TN "RPOS AU updater" /XML "C:\temp\installupdaterpos\RPOS AU updater PC1.xml"
	Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$newver" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoXMLchecksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Value "1" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value "$datereg" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUVer" -Value "$newver" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Checksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "FlowModule Checksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Value "1" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRNI" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRInstalling" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUGatherReportingData" -Value "0" -Force
	New-Item "C:\Windows\Scripts" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\Logs" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\Bak" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\build" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\update" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\uninstall" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
	New-Item "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos"
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos"
	Copy-Item "C:\temp\installupdaterpos\UpdateRPOS.ico" "C:\Windows\Scripts\RPOS" -Force
	Copy-Item "C:\temp\installupdaterpos\rposauaf.exe" "C:\Windows\System32" -Force
	Copy-Item "C:\temp\installupdaterpos\auicon.ico" "C:\Windows\Scripts\RPOS" -Force
	Copy-Item "C:\temp\installupdaterpos\Update RPOS.lnk" "C:\Windows\Scripts\RPOS\Bak" -Force
	
	# Fixing Windows 10 Scheduled Tasks
	if ($computerver -match "Microsoft Windows 10")
	{
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/FixWindows10tasks.exe"
		$FixWin10TasksEXELocal = "C:\temp\installupdaterpos\FixWindows10tasks.exe"
		$Webclient = New-Object System.Net.WebClient
		$Webclient.DownloadFile($URL, $FixWin10TasksEXELocal)
		
		Start-Process -FilePath "C:\temp\installupdaterpos\FixWindows10tasks.exe" -Wait
	}
	
	$rposqaprofilefolder = Test-Path "C:\TBC_Scripts\RPOSAU"
	
	If (($rposqaprofilefolder -eq $false) -or (!$rposqaprofilefolder))
	{
		New-Item -Path "C:\TBC_Scripts" -ItemType Directory -Force
		New-Item -Path "C:\TBC_Scripts\RPOSAU" -ItemType Directory -Force
		Write-Output "http://rpos.tbccorp.com" | Out-File -FilePath "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
	}
	
	#Removing EXE from Auto-Updater
	Remove-Item "C:\Windows\Scripts\RPOS\RemoveLog60days.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\rollbackrpos.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\update.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\updaterpos.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\updaterposdesktop.exe" -Force
	
	net share cs18ebyi3$=C:\Windows\Scripts\RPOS\build "/grant:TBC\shprorpos,READ" "/grant:TBC\shprorpos2,READ"
	Copy-Item "C:\temp\installupdaterpos\UninstallRPOSAU.exe" "C:\Windows\Scripts\RPOS" -Force
	Copy-Item "C:\temp\installupdaterpos\quietuninstall.vbs" "C:\Windows\Scripts\RPOS\uninstall" -Force
	Copy-Item "C:\temp\installupdaterpos\BuildUninstallers.exe" "C:\WINDOWS\Scripts\RPOS\uninstall" -Force
	Copy-Item "C:\temp\installupdaterpos\Update RPOS.lnk" "C:\Users\Public\Desktop" -Force
	Copy-Item "C:\temp\installupdaterpos\Update RPOS.lnk" "C:\Windows\Scripts\RPOS\Bak" -Force
	icacls "C:\Windows\System32\Tasks\Update RPOS Local A" /grant Everyone:RX
	icacls "C:\Windows\System32\Tasks\Update RPOS Local B" /grant Everyone:RX
	icacls "C:\Windows\Temp\RPOS AU Uninstaller\quietuninstall.vbs" /grant Everyone:RX
	icacls "C:\Users\Public\Desktop\RPOS.lnk" /grant Everyone:F
	icacls "C:\Users\Public\Desktop\Update RPOS.lnk" /grant Everyone:RX
	New-Item -ItemType directory "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater"
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayIcon" -Value "C:\Windows\Scripts\RPOS\auicon.ico" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayName" -Value "RPOS Auto-Updater" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayVersion" -Value "$newver" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "EstimatedSize" -Value "2000" -PropertyType DWORD -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "InstallLocation" -Value "C:\Windows\Scripts\RPOS" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "NoModify" -Value "1" -PropertyType DWORD -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "NoRepair" -Value "1" -PropertyType DWORD -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "Publisher" -Value "TBC Corporation EOC Department" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "UninstallString" -Value 'C:\Windows\Scripts\RPOS\Uninstall\BuildUninstallers.exe' -Force
	if ($PSVersion -ne "5")
	{
		wusa.exe "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3134760-x64.msu" /quiet /norestart
		Start-Sleep -Seconds 600
	}
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Clear Logs.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Rollback RPOS Local.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3134760-x64.msu" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS Auto-Updater Autofix.xml"
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\quietuninstall.vbs" -Force
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.bat" -Force
	Remove-Item "C:\temp\installupdaterpos\Second Install.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\rposauaf.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\FixWindows10tasks.exe" -Force
	schtasks /delete /TN "After Reboot RPOS AU Install" /F
	& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N
	Exit
	
}



Remove-PSDrive -Name HKLM -Force
Remove-Item "C:\temp\installupdaterpos\Selfinstaller.exe" -Force