﻿###############################################################
#    RPOS Auto-Updater AutoFix                                #
#    Last Updated: 3.13.2018                                   #
###############################################################
$TwoDigPCName = "$ENV:COMPUTERNAME"

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]00')
{
	$script:TwoDigPCName = $TwoDigPCName.Substring(6, 2)
}

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]NPC')
{
	$script:TwoDigPCName = $TwoDigPCName.Substring(7, 2)
}


$Powershell32 = Test-Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1"
$Powershell64 = Test-Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1"
$Powershell32Folder = Test-Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos"
$Powershell64Folder = Test-Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos"
$task1 = schtasks /query /TN "RPOS AU updater"
	
	if ($Powershell32Folder -eq $false)
	{
		New-Item -Itemtype Directory -Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
	}
	
	if ($Powershell64Folder -eq $false)
	{
		New-Item -Itemtype Directory -Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
	}
	
	$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/hasher.xml"
	Invoke-WebRequest -Uri $URL -OutFile "C:\temp\hasher.xml"
	
	[xml]$xmlread = Get-Content "C:\temp\hasher.xml"
	$a = $xmlread.autofix.updaterpossys
	$b = $xmlread.autofix.updaterpossyswow
	
	$updaterpospsm32 = Get-Item -Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
	$updaterpospsm64 = Get-Item -Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
	
	if (($Powershell32 -eq $false) -or ($updaterpospsm32 -ne $a) -or ($Powershell64 -eq $false) -or ($updaterpospsm64 -ne $b))
	{
		
		if (($Powershell32 -eq $false) -or ($updaterpospsm32 -ne $a))
		{
			$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/updaterpos.zip"
			Invoke-WebRequest -Uri $URL -OutFile "C:\temp\updaterpos.zip"
			Expand-Archive "C:\temp\updaterpos.zip" -DestinationPath "C:\temp\updaterpos" -Force
			Copy-Item "C:\temp\updaterpos\updaterpos.psm1" -Destination "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" -Force
		}
		
		if (($Powershell64 -eq $false) -or ($updaterpospsm64 -ne $b))
		{
			$testupdaterposextract = Test-Path "C:\temp\updaterpos\updaterpos.psm1"
			
			if ($testupdaterposextract -eq $true)
			{
				Copy-Item "C:\temp\updaterpos\updaterpos.psm1" -Destination "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" -Force
			}
			Else
			{
				$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix/updaterpos.zip"
				Invoke-WebRequest -Uri $URL -OutFile "C:\temp\updaterpos.zip"
				Expand-Archive "C:\temp\updaterpos.zip" -DestinationPath "C:\temp\updaterpos" -Force
				Copy-Item "C:\temp\updaterpos\updaterpos.psm1" -Destination "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" -Force
			}
			
		}
		
		Remove-Item "C:\temp\updaterpos.zip" -Force
		Remove-Item "C:\temp\updaterpos" -Recurse -Force
		Remove-Item "C:\temp\hasher.xml" -Force
		Start-Process "Powershell" -ArgumentList "Get-RPOSAutoHeal" -NoNewWindow -Wait
	

	}

Start-Process "Powershell.exe" -ArgumentList "Get-RPOSAutoHeal" -NoNewWindow -Wait

Remove-Item "C:\temp\hasher.xml" -Force

