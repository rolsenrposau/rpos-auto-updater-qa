﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.135
	 Created on:   	2/12/2017 4:01 PM
	 Created by:   	TBC\rolsen
	 Organization: 	TBC Corporation
	 Filename:     	CheckAdobeSum.ps1
	===========================================================================
	.DESCRIPTION
		This script will check the Adobe AIR application upgrade checksum and file version
#>

$xml = New-Object XML
$xml.Load("C:\inetpub\wwwroot\AdobeAirCheck.xml")
$AdobeOldVer = $xml.AdobeAir.AdobeAirVer
$AdobeOldHash = $xml.AdobeAir.AdobeHashCheck
$AdobeOldVer
$AdobeOldHash

$adobever = (Get-ItemProperty "C:\inetpub\wwwroot\AdobeAIRInstaller.exe").VersionInfo | Select-Object -ExpandProperty FileVersion
$adobechecksum = Get-Item -Path "C:\inetpub\wwwroot\AdobeAIRInstaller.exe" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash

if (($AdobeOldVer -ne $adobever) -or ($AdobeOldHash -ne $adobechecksum)) {

$xml.AdobeAir.AdobeAirVer = "$adobever"
$xml.AdobeAir.AdobeAirVer
$xml.Save("C:\inetpub\wwwroot\AdobeAirCheck.xml")

$xml.AdobeAir.AdobeHashCheck = "$adobechecksum"
$xml.AdobeAir.AdobeHashCheck
$xml.Save("C:\inetpub\wwwroot\AdobeAirCheck.xml")
	
$smtp = "mailrelay.tbccorp.com"
$to = "EOC@tbccorp.com"
$from = "EOC@tbccorp.com"
$subject = "**Informational** Adobe AIR Installer has been updated on TPOSDIST01"
$body = "Good Morning/Good Afternoon,"
$body += "<br />"
$body += "<br />"
$body += "TPOSDIST01 has detected that someone has placed a newer version of Adobe AIR version $adobever."
$body += "<br />"
$body += "the pervious version of Adobe AIR installer was $AdobeOldVer."
$body += "<br />"
$body += "<br />"
$body += "If there was a change no action is needed, however if you receive this and nothing was changed"
$body += "<br />"
$body += "please logon and check file C:\inetpub\wwwroot\AdobeAIRInstaller.exe "
$body += "<br />"
$body += "<br />"
$body += "Alert: Adobe AIR upgrade report"
$body += "<br />"
$body += "This is an auto generated message."
Send-MailMessage -SmtpServer $smtp -To $to -From $from -Subject $subject -Body $body -BodyAsHtml -Priority high
	
}

Else {
Write-Output "Doing nothing no change"
}
