﻿###################################################
#       Auto-Update RPOS Powershell               #
#       Made By: Randy Olsen                      #
#       Date: 2/23/2018                           #
#       Version: 3.1                              #
###################################################

#ALL VARIABLES THAT ARE IN THIS POWERSHELL SCRIPT CAN BE CHANGED FROM THE FIRST COUPLE ROWS TO REFLECT THE ENTIRE SCRIPT!#
#Setting Up Varibles

#########################################################################################################################################
#                                                         * START OF MODULES *                                                          # 
#########################################################################################################################################
#Function Set-RPOSVar
function Set-RPOSVar
{
	$script:name = $env:COMPUTERNAME
	$script:date = (get-date).ToString("yyyyMMdd-hhmmss")
	
	#Default DIR for Auto-Updater
	$script:RPOSAUDIRHOME = "$env:RPOSAutoUpdater"
	$script:RPOSAUDIRBAK = "$env:RPOSAutoUpdater\Bak"
	$script:RPOSAUDIRBUILD = "$env:RPOSAutoUpdater\Build"
	$script:RPOSAUDIRLOGS = "$env:RPOSAutoUpdater\Logs"
	$script:RPOSAUDIRUNINSTALL = "$env:RPOSAutoUpdater\uninstall"
	$script:RPOSAUDIRUPDATE = "$env:RPOSAutoUpdater\update"
	$script:RPOSAUDIRAUTOFIX = "$env:RPOSAutoUpdater\Autofix"
	$script:RPOSAUAUTOFIX = "$env:SystemRoot\System32\rposauaf.exe"
	
	#RPOS Program Files Dirs
	$script:RPOSHOME = "${env:ProgramFiles(x86)}\RPOS"
	$script:FLOWHOME = "${env:ProgramFiles(x86)}\FlowModule"
	
	$script:autofixhttpdir = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix"
	$script:Log = "$RPOSAUDIRLOGS\RPOS_Update_$date.txt"
	$script:ReleaseInfo = "v3.1"
	$script:LANDeskKeys = "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
	$script:keyrpos = "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\com.tbccorp.rpos.RPOS"
	$script:keyflow = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\com.tbccorp.rpos.FlowModule"
	$script:nameprofile = $name.Substring(1, 3)
	$script:profilename = "store00$nameprofile"
	$script:XMLEMVfileRPOS = "C:\Users\$profilename\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
	$script:XMLEMVfileFlow = "C:\Users\$profilename\AppData\Roaming\com.tbccorp.rpos.FlowModule\Local Store\rpos-preferences.xml"
	$script:XMLEMVRPOSbak = "$RPOSAUDIRBAK\rpos-preferencesrpos.xml"
	$script:XMLEMVFLOWbak = "$RPOSAUDIRBAK\rpos-preferencesflow.xml"
	$script:XML = "$RPOSAUDIRUPDATE.xml"
	$script:XMLhash = "$RPOSAUDIRHOME\checksum.xml"
	$script:RPOSEXE = "$RPOSHOME\RPOS.exe"
	$script:RPOSDesktopIcon = "C:\Users\Public\Desktop\RPOS.lnk"
	$script:arhfile = "$RPOSAUDIRHOME\arh.exe"
	$script:RPOSfileBakair = "$RPOSAUDIRBAK\RPOS.bakair"
	$script:flowfileBakair = "$RPOSAUDIRBAK\FlowModule.bakair"
	$script:RPOSfileBakexe = "$RPOSAUDIRBAK\RPOS.bakexe"
	$script:flowfileBakexe = "$RPOSAUDIRBAK\FlowModule.bakexe"
	$script:flowfileair = "$RPOSAUDIRHOME\FlowModule.air"
	$script:RPOSfileair = "$RPOSAUDIRHOME\RPOS.air"
	$script:flowfileexe = "$RPOSAUDIRHOME\FlowModule.exe"
	$script:RPOSfileexe = "$RPOSAUDIRHOME\RPOS.exe"
	$script:RPOSVersionLabelXML = "$RPOSHOME\META-INF\AIR\application.xml"
	$script:LiveIP = "$RPOSAUDIRHOME\LiveIPs.txt"
	$script:LiveNames = "$RPOSAUDIRHOME\LiveNames.txt"
	$script:datereg = (Get-Date).ToString("MM/dd/yyyy")
	$script:dateofdelayhour = Get-Date -Format "HH"
	$script:adobesumnum = "0"
	$script:sumnum = "0"
	$script:sumnumbginfo = "0"
	$script:results = "$RPOSAUDIRHOME\results.txt"
	$script:AdobeAIRInstallRoot = "$RPOSAUDIRHOME\AdobeAIRInstaller.exe"
	$script:AdobeAIRInstallShare = "$RPOSAUDIRBUILD\AdobeAIRInstaller.exe"
	$script:AdobeInstallerFile = "AdobeAIRInstaller.exe"
	$script:BGInfoLocalFile = "C:\bginfo\TBC.bgi"
	$script:checkupdatedXMLs = "http://rpos.tbccorp.com/RPOSfileupdatechecksum.xml"
	$script:RegBGInfoXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoXMLchecksum).BGInfoXMLchecksum
	$script:RegSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	$script:ReportingMaserXMLhtml = "http://rpos.tbccorp.com/RPOSAutoUpdater/Reporting/MasterData/MasterCollection.xml"
	$script:ReportingMaserXMLlocal = "R:\MasterCollection.xml"
	$script:builddir = "$RPOSAUDIRBUILD"
	$script:flowfileair = "$RPOSAUDIRHOME\FlowModule.air"
	$script:RPOSfileair = "$RPOSAUDIRHOME\RPOS.air"
	$script:flowfileexe = "$RPOSAUDIRHOME\FlowModule.exe"
	$script:RPOSfileexe = "$RPOSAUDIRHOME\RPOS.exe"
	$script:RPOSfileBakair = "$RPOSAUDIRBAK\RPOS.bakair"
	$script:flowfileBakair = "$RPOSAUDIRBAK\FlowModule.bakair"
	$script:RPOSfileBakexe = "$RPOSAUDIRBAK\RPOS.bakexe"
	$script:flowfileBakexe = "$RPOSAUDIRBAK\FlowModule.bakexe"
	
	#Two Digit Store Number
	$TwoDigPCName = "$ENV:COMPUTERNAME"
	
	if ($TwoDigPCName -match 'S[0-9][0-9][0-9]00') {
		$script:TwoDigPCName = $TwoDigPCName.Substring(6, 2) }
	
	if ($TwoDigPCName -match 'S[0-9][0-9][0-9]NPC') {
		$script:TwoDigPCName = $TwoDigPCName.Substring(7, 2) }
	
	#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for EMV
	[xml]$xmlread = Get-Content $XMLEMVfileRPOS
	$script:Result = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
	
	if ($Result -eq $null)
	{
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for NON-EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$Read = $xmlread.ApplicationPreferences.creditCardTerminalUrl | Select-Object -ExpandProperty "#cdata-section"
		$script:Result = $Read -replace ":9001"
	}
	
	#Adding Verifone XML IP Address
	$script:XMLipa = ([ipaddress] "$Result").GetAddressBytes()[0]
	$script:XMLipb = ([ipaddress] "$Result").GetAddressBytes()[1]
	$script:XMLipc = ([ipaddress] "$Result").GetAddressBytes()[2]
	$script:XMLipd = ([ipaddress] "$Result").GetAddressBytes()[3]
	
	#Grabbing pinged IP address and gateway address
	$script:IP = Test-Connection $name -count 1 | select -ExpandProperty Ipv4Address | Select -ExpandProperty IPAddressToString
	$script:ipmain1 = ([ipaddress] "$IP").GetAddressBytes()[0]
	$script:ipmain2 = ([ipaddress] "$IP").GetAddressBytes()[1]
	$script:ipmain3 = ([ipaddress] "$IP").GetAddressBytes()[2]
	$script:ipmain4 = ([ipaddress] "$IP").GetAddressBytes()[3]
	$script:IPZero = "$ipmain1.$ipmain2.$ipmain3.0"
	
	#Testing to see if there is internet
	$script:networkcheck = Test-Connection "rpos.tbccorp.com" -count 4
	
	#Servers List
	$script:prod = "rpos.tbccorp.com"
	$script:train = "rpos-training.tbccorp.com"
	$script:pilot = "rpos-pilot.tbccorp.com"
	$script:pilot2 = "rpos-pilot2.tbccorp.com"
	$script:pilot3 = "rpos-pilot3.tbccorp.com"
	$script:qa = "rpos-qa.tbccorp.com"
	$script:qapilot = "rpos-qapilot.tbccorp.com"
	$script:qapilot2 = "rpos-qapilot2.tbccorp.com"
	$script:qapilot3 = "rpos-qapilot3.tbccorp.com"
	$script:alpha = "rpos-alpha.tbccorp.com"
	$script:dev = "rpos-dev.tbccorp.com"
	$script:devpilot = "rpos-devpilot.tbccorp.com"
	$script:devpilot2 = "rpos-devpilot2.tbccorp.com"
	$script:devpilot3 = "rpos-devpilot3.tbccorp.com"
	
	#Random Run For Inventory And Starting RPOS Auto-Updater
	$script:StartTimeDelay = Get-Random -Maximum 3600
	$script:InventoryRunTime = Get-Random -Maximum 600
	$script:RandomLetter = -join ((65 .. 90) + (97 .. 122) | Get-Random -Count 7 | % { [char]$_ })
	
	#Name of XML file that will get deposited into TPOSDIST01
	$script:XMLDeposit = "${name}_${RandomLetter}.xml"
		
}

#########################################################################################################################################
#Fuction Get-RPOSAutoHeal
function Get-RPOSAutoHeal
{
Set-RPOSVar
	
$autofixnetworkcheck = Test-Connection "rpos.tbccorp.com" -count 4
	
if ($autofixnetworkcheck -ne $null) {
		
		$AutofixDate = (Get-Date).ToString("yyyyMMdd-hhmmss")
		$Log = "$RPOSAUDIRLOGS\RPOSAutofix_$AutofixDate.txt"
		
		#Hasher XML file checker
		$hasher = "$autofixhttpdir/hasher.xml"
		$hasherlocal = "$RPOSAUDIRUPDATE\hasher.xml"
		
		$httpauicon = "$autofixhttpdir/auicon.ico"
		$httpbuildun = "$autofixhttpdir/RPOSAutoUpdater/AutoFix/BuildUninstallers.exe"
		$httpquietun = "$autofixhttpdir/quietuninstall.vbs"
		$httprollbackxml = "$autofixhttpdir/Rollback RPOS Local.xml"
		$httprposaupc1xml = "$autofixhttpdir/RPOS AU updater PC1.xml"
		$httprposaupc2xml = "$autofixhttpdir/RPOS AU updater PC2.xml"
		$httprposaupc3xml = "$autofixhttpdir/RPOS AU updater PC3.xml"
		$httpuninstallrposau = "$autofixhttpdir/UninstallRPOSAU.exe"
		$httprposclearlogsxml = "$autofixhttpdir/Update RPOS Clear Logs.xml"
		$httplocalrposapc1xml = "$autofixhttpdir/Update RPOS Local A PC1.xml"
		$httplocalrposapc2xml = "$autofixhttpdir/Update RPOS Local A PC2.xml"
		$httplocalrposapc3xml = "$autofixhttpdir/Update RPOS Local A PC3.xml"
		$httplocalrposbpc1xml = "$autofixhttpdir/Update RPOS Local B PC1.xml"
		$httplocalrposbpc2xml = "$autofixhttpdir/Update RPOS Local B PC2.xml"
		$httplocalrposbpc3xml = "$autofixhttpdir/Update RPOS Local B PC3.xml"
		$httprposauautohealxml = "$autofixhttpdir/RPOS Auto-Updater Autofix.xml"
		$httpupdaterposdtlnk = "$autofixhttpdir/Update RPOS.lnk"
		$httpupdaterposico = "$autofixhttpdir/UpdateRPOS.ico"
		$httpupdaterpospsm1 = "$autofixhttpdir/updaterpos.psm1"
		$httprposdesktoplnk = "$autofixhttpdir/RPOS.lnk"
		$httpfixabtasks = "$autofixhttpdir/FixUpdateRPOSABTasks.exe"
		$httpautofixzip = "$autofixhttpdir/Autofix.zip"
		
		$localauicon = "$RPOSAUDIRHOME\auicon.ico"
		$localuninstallau = "$RPOSAUDIRUNINSTALLRPOSAU.exe"
		$localupdaterposicon = "$RPOSAUDIRUPDATERPOS.ico"
		$localrposlnkbak = "$RPOSAUDIRBAK\RPOS.lnk"
		$localupdaterposlnkbak = "$RPOSAUDIRBAK\Update RPOS.lnk"
		$localbuilduninstall = "$RPOSAUDIRUNINSTALL\BuildUninstallers.exe"
		$localquietuninstall = "$RPOSAUDIRUNINSTALL\quietuninstall.vbs"
		$localupdaterpospubdesk = "C:\Users\Public\Desktop\Update RPOS.lnk"
		$localpowershell32 = "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1"
		$localpowershell64 = "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1"
		$localrollbackxml = "$RPOSAUDIRUPDATE\Rollback RPOS Local.xml"
		$localauupdaterxml = "$RPOSAUDIRUPDATE\RPOS AU updater.xml"
		$localclearlogsxml = "$RPOSAUDIRUPDATE\Update RPOS Clear Logs.xml"
		$localfixabtasks = "$RPOSAUDIRUPDATE\FixUpdateRPOSABTasks.exe"
		$localautoupdaterautohealxml = "$RPOSAUDIRUPDATE\RPOS Auto-Updater Autofix.xml"
		$localautofixzip = "$RPOSAUDIRUPDATE\AutoFix.zip"
		
		#Keys
		$uninstallRegFolder = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater"
		
		#Uninstaller Registry Values
		$DisplayIcon = "$RPOSAUDIRHOME\auicon.ico"
		$DisplayName = "RPOS Auto-Updater"
		$DisplayVersion = "$ReleaseInfo"
		$EstimatedSize = "2000"
		$InstallLocation = "$RPOSAUDIRHOME"
		$NoModify = "1"
		$NoRepair = "1"
		$Publisher = "TBC Corporation EOC Department"
		$UninstallString = "$RPOSAUDIRUNINSTALL\BuildUninstallers.exe"
		
		#LANDesk Registry Values
		#Checking Registry Keys
		$ADcount = (Get-ItemProperty -Path $LANDeskKeys -Name "Adobe DCount")."Adobe DCount"
		$AdobeAirVer = (Get-ItemProperty -Path $LANDeskKeys -Name "AdobeAIRVer")."AdobeAIRVer"
		$AdobeDistro = (Get-ItemProperty -Path $LANDeskKeys -Name "AdobeDistro")."AdobeDistro"
		$AuVerUpdateDate = (Get-ItemProperty -Path $LANDeskKeys -Name "AuVer UpdateDate")."AuVer UpdateDate"
		$BGInfoVer = (Get-ItemProperty -Path $LANDeskKeys -Name "BGInfoVersion")."BGInfoVersion"
		$BGInfoXMLCS = (Get-ItemProperty -Path $LANDeskKeys -Name "BGInfoXMLchecksum")."BGInfoXMLchecksum"
		$EMVGateway = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVGateway")."EMVGateway"
		$EMVIPAddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress")."EMVIPaddress"
		$FMChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "FlowModule Checksum")."FlowModule Checksum"
		$InstallComplete = (Get-ItemProperty -Path $LANDeskKeys -Name "InstallComplete")."InstallComplete"
		$RPOSAUVerPC = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Auto-Updater")."RPOS Auto-Updater"
		$RPOSChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Checksum")."RPOS Checksum"
		$RPOSDCount = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS DCount")."RPOS DCount"
		$RPOSDLS = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS DL Source")."RPOS DL Source"
		$RPOSUDate = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Update Date")."RPOS Update Date"
		$RPOSVersion = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version")."RPOS Version"
		$RPOSVersionLabel = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version Label")."RPOS Version Label"
		$RPOSAUVer = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOSAUVer")."RPOSAUVer"
		$SwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "SwitchXMLchecksum")."SwitchXMLchecksum"
		
		#Testing Powershell Module Folder exists
		$Powershell32 = Test-Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos"
		$Powershell64 = Test-Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos"
		
		#Getting Folder in RPOS AU Directory Properties
		$scriptfltest = Test-Path "C:\Windows\Scripts\"
		$scriptflhid = Get-ItemProperty "C:\Windows\Scripts" | Select-Object -ExpandProperty Attributes
		
		$RPOSfltest = Test-Path "$RPOSAUDIRHOME"
		$RPOSflhid = Get-ItemProperty "$RPOSAUDIRHOME" | Select-Object -ExpandProperty Attributes
		
		$bakfltest = Test-Path "$RPOSAUDIRBAK"
		$bakflhid = Get-ItemProperty "$RPOSAUDIRBAK" | Select-Object -ExpandProperty Attributes
		
		$buildfltest = Test-Path "$RPOSAUDIRBUILD"
		$buildflhid = Get-ItemProperty "$RPOSAUDIRBUILD" | Select-Object -ExpandProperty Attributes
		
		$logfltest = Test-Path "$RPOSAUDIRLOGS"
		$logflhid = Get-ItemProperty "$RPOSAUDIRLOGS" | Select-Object -ExpandProperty Attributes
		
		$uninstallfltest = Test-Path "$RPOSAUDIRUNINSTALL"
		$uninstallflhid = Get-ItemProperty "$RPOSAUDIRUNINSTALL" | Select-Object -ExpandProperty Attributes
		
		$updatefltest = Test-Path "$RPOSAUDIRUPDATE"
		$updateflhid = Get-ItemProperty "$RPOSAUDIRUPDATE" | Select-Object -ExpandProperty Attributes
		
		#Checking for share folder
		$checkshare = get-WmiObject -class Win32_Share -computer $Env:COMPUTERNAME | Where-Object { $_.Name -eq "cs18ebyi3$" } | Select-Object -ExpandProperty Path
		
		$auicon = Test-Path "$RPOSAUDIRHOME\auicon.ico"
		$updaterposicon = Test-Path "$RPOSAUDIRHOME\UpdateRPOS.ico"
		$RPOSlnk = Test-Path "$RPOSAUDIRBAK\RPOS.lnk"
		$updaterposlnk = Test-Path "$RPOSAUDIRBAK\Update RPOS.lnk"
		$builduninstallers = Test-Path "$RPOSAUDIRUNINSTALL\BuildUninstallers.exe"
		$quietuninstallvbs = Test-Path "$RPOSAUDIRUNINSTALL\quietuninstall.vbs"
		$publicdesktopicon = Test-Path "C:\Users\Public\Desktop\Update RPOS.lnk"
		$updaterposico = Test-Path "$RPOSAUDIRHOME\UpdateRPOS.ico"
		$autoupdateautofix = Test-Path "$RPOSAUAUTOFIX"
		
		#Checking Last Modified Time of Test-Path files
		$updaterposlnkhash = Get-Item -Path "$RPOSAUDIRBAK\Update RPOS.lnk" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$publicdesktopiconhash = Get-Item -Path "C:\Users\Public\Desktop\Update RPOS.lnk" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$builduninstallershash = Get-Item -Path "$RPOSAUDIRUNINSTALL\BuildUninstallers.exe" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$quietuninstallvbshash = Get-Item -Path "$RPOSAUDIRUNINSTALL\quietuninstall.vbs" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$auiconhash = Get-Item -Path "$RPOSAUDIRHOME\auicon.ico" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$rposbaklnkhash = Get-Item -Path "$RPOSAUDIRBAK\RPOS.lnk" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$updaterpospsm32 = Get-Item -Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$updaterpospsm64 = Get-Item -Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$updaterposico = Get-Item -Path "$RPOSAUDIRHOME\UpdateRPOS.ico" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$rposauautofixhash = Get-Item -Path "$RPOSAUAUTOFIX" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		
		#Checking Local Administrators on PC
		$admins = (Gwmi win32_groupuser | ? { $_.groupcomponent -like '*"Administrators"' } | Select-Object -ExpandProperty PartComponent | Select-String -Pattern "shprorpos").Count
		
		#Checking Scheduled Tasks
		$task1 = schtasks /query /TN "Rollback RPOS Local"
		$task2 = schtasks /query /TN "RPOS AU updater"
		$task3 = schtasks /query /TN "Update RPOS Clear Logs"
		$task4 = schtasks /query /TN "Update RPOS Local A"
		$task5 = schtasks /query /TN "Update RPOS Local B"
		$task6 = schtasks /query /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix"
		
		#Uninstall registry keys
		$uninstall = Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater"
		$undisplayicon = (Get-ItemProperty -Path $uninstallRegFolder -Name "DisplayIcon")."DisplayIcon"
		$undisplayname = (Get-ItemProperty -Path $uninstallRegFolder -Name "DisplayName")."DisplayName"
		$undisplayver = (Get-ItemProperty -Path $uninstallRegFolder -Name "DisplayVersion")."DisplayVersion"
		$uninstallloc = (Get-ItemProperty -Path $uninstallRegFolder -Name "InstallLocation")."InstallLocation"
		$unestimatedsize = (Get-ItemProperty -Path $uninstallRegFolder -Name "EstimatedSize")."EstimatedSize"
		$unnomodify = (Get-ItemProperty -Path $uninstallRegFolder -Name "NoModify")."NoModify"
		$unnorepair = (Get-ItemProperty -Path $uninstallRegFolder -Name "NoRepair")."NoRepair"
		$unpublisher = (Get-ItemProperty -Path $uninstallRegFolder -Name "Publisher")."Publisher"
		$ununinstallstring = (Get-ItemProperty -Path $uninstallRegFolder -Name "UninstallString")."UninstallString"
		
		#Check ExecutionPolicy
		$executionpolicy = Get-ExecutionPolicy
		
		#Checking System Variable
		$envrposautoupdater = $env:RPOSAutoUpdater
		
		########################################################################################################
		#Checking Varibles and fixing if needed
		#Checking to see if RPOS directories exist on PC.
		Write-Output "RPOS Auto-Updater $ReleaseInfo AutoFix Module Starting" | Out-File -Append -FilePath $Log
		
		
		if (($scriptfltest -ne $true) -or ($scriptflhid -ne "Hidden, Directory") -or ($RPOSfltest -ne $true) -or ($RPOSflhid -ne "Hidden, Directory") -or ($bakfltest -ne $true) -or ($bakflhid -ne "Hidden, Directory") -or ($buildfltest -ne $true) -or ($buildflhid -ne "Hidden, Directory") -or ($logfltest -ne $true) -or ($logflhid -ne "Hidden, Directory") -or ($uninstallfltest -ne $true) -or ($uninstallflhid -ne "Hidden, Directory") -or ($updatefltest -ne $true) -or ($updateflhid -ne "Hidden, Directory") -or ($Powershell32 -ne $true) -or ($Powershell64 -ne $true))
		{
			
			if (($scriptfltest -ne $true) -or ($scriptflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged C:\Windows\Scripts" | Out-File -Append -FilePath $Log
				Remove-Item "C:\Windows\Scripts" -Recurse -Force
				New-Item "C:\Windows\Scripts" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRHOME" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRLOGS" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBAK" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBUILD" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRUPDATE" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRUNINSTALL" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($RPOSfltest -ne $true) -or ($RPOSflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRHOME" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRHOME" -Recurse -Force
				New-Item "$RPOSAUDIRHOME" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRLOGS" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBAK" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBUILD" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRUPDATE" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRUNINSTALL" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($bakfltest -ne $true) -or ($bakflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRBAK" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRBAK" -Recurse -Force
				New-Item "$RPOSAUDIRBAK" -ItemType Directory | %{ $_.Attributes = "hidden" }
				net share cs18ebyi3$=C:\Windows\Scripts\RPOS\build "/grant:TBC\shprorpos,READ" "/grant:TBC\shprorpos2,READ"
			}
			
			if (($buildfltest -ne $true) -or ($buildflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRBUILD" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRBUILD" -Recurse -Force
				New-Item "$RPOSAUDIRBUILD" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($logfltest -ne $true) -or ($logflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRLOGS" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRLOGS" -Recurse -Force
				New-Item "$RPOSAUDIRLOGS" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($uninstallfltest -ne $true) -or ($uninstallflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRUNINSTALL" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRUNINSTALL" -Recurse -Force
				New-Item "$RPOSAUDIRUNINSTALL" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($updatefltest -ne $true) -or ($updateflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRUPDATE" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRUPDATE" -Recurse -Force
				New-Item "$RPOSAUDIRUPDATE" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if ($Powershell32 -ne $true)
			{
				Write-Output "Following folder was missing and/or damaged C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" | Out-File -Append -FilePath $Log
				New-Item -ItemType Directory "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
			}
			
			if ($Powershell64 -ne $true)
			{
				Write-Output "Following folder was missing and/or damaged C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" | Out-File -Append -FilePath $Log
				New-Item -ItemType Directory "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
			}
		}
		Else
		{
			Write-Output "CHECKING FOLDER AUTO UPDATE FOLDERS                                      : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Grabbing values from hash file to ensure proper files are installed on the local PC
		$URL = $hasher
		Invoke-WebRequest -Uri $URL -OutFile $hasherlocal
		
		[xml]$xmlread = Get-Content $hasherlocal
		$a = $xmlread.autofix.bakupdaterpos
		$b = $xmlread.autofix.publicupdaterpos
		$c = $xmlread.autofix.builduninstaller
		$d = $xmlread.autofix.quietuninstall
		$e = $xmlread.autofix.auicon
		$f = $xmlread.autofix.bakrposlnk
		$g = $xmlread.autofix.updaterpossys
		$h = $xmlread.autofix.updaterpossyswow
		$i = $xmlread.autofix.updaterposico
		$j = $xmlread.autofix.autoupdaterautoheal
		
		if (($a -ne $updaterposlnkhash) -or ($b -ne $publicdesktopiconhash) -or ($c -ne $builduninstallershash) -or ($d -ne $quietuninstallvbshash) -or ($e -ne $auiconhash) -or ($f -ne $rposbaklnkhash) -or ($g -ne $updaterpospsm32) -or ($h -ne $updaterpospsm64) -or ($i -ne $updaterposico))
		{
			#Downloading AutoFix Zip file and extracting it
			Write-Output "Downloading Autofix Files so Auto-Updater can be fixed" | Out-File -Append -FilePath $Log
			$URL = $httpautofixzip
			Invoke-WebRequest -Uri $URL -OutFile $localautofixzip
			Expand-Archive -Path $localautofixzip -DestinationPath $RPOSAUDIRAUTOFIX
			
			
			#Checking $RPOSAUDIRBAK\Update RPOS.lnk file
			if ($a -ne $updaterposlnkhash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRBAK\Update RPOS.lnk" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\Update RPOS.lnk" -Destination "$RPOSAUDIRBAK\Update RPOS.lnk" -Force
				icacls $localupdaterposlnkbak /grant Everyone:RX
			}
			
			#Checking C:\Users\Public\Desktop\Update RPOS.lnk file
			if ($b -ne $publicdesktopiconhash)
			{
				Write-Output "Following file was missing and/or damaged C:\Users\Public\Desktop\Update RPOS.lnk" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\Update RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force
				icacls $localupdaterpospubdesk /grant Everyone:RX
			}
			
			#Checking $RPOSAUDIRUNINSTALL\BuildUninstallers.exe file
			if ($c -ne $builduninstallershash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRUNINSTALL\BuildUninstallers.exe" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\BuildUninstallers.exe" -Destination "$RPOSAUDIRUNINSTALL\BuildUninstallers.exe" -Force
			}
			
			#Checking $RPOSAUDIRUNINSTALL\quietuninstall.vbs file
			if ($d -ne $quietuninstallvbshash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRUNINSTALL\quietuninstall.vbs" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\quietuninstall.vbs" -Destination "$RPOSAUDIRUNINSTALL\quietuninstall.vbs" -Force
			}
			
			#Checking $RPOSAUDIRHOME\auicon.ico file
			if ($e -ne $auiconhash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRHOME\auicon.ico" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\auicon.ico" -Destination "$RPOSAUDIRHOME\auicon.ico" -Force
			}
			
			#Checking $RPOSAUDIRBAK\RPOS.lnk file
			if ($f -ne $rposbaklnkhash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRBAK\RPOS.lnk" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\RPOS.lnk" -Destination "$RPOSAUDIRBAK\RPOS.lnk" -Force
				icacls $localrposlnkbak /grant Everyone:RX
			}
			
			#Checking C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1 file
			if ($g -ne $updaterpospsm32)
			{
				Write-Output "Following file was missing and/or damaged C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\updaterpos.psm1" -Destination "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
			}
			
			#Checking C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1 file
			if ($h -ne $updaterpospsm64)
			{
				Write-Output "Following file was missing and/or damaged C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\updaterpos.psm1" -Destination "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" -Force
			}
			
			#Checking C:\Windows\Scripts\RPOS\UpdateRPOS.ico file
			if ($i -ne $updaterposico)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRHOME\UpdateRPOS.ico" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\UpdateRPOS.ico" -Destination "$RPOSAUDIRHOME\UpdateRPOS.ico" -Force
			}
			
			if ($j -ne $updaterposico)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUAUTOFIX" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRAUTOFIX\rposauaf.exe" -Destination "$env:SystemRoot\system32" -Force
			}
			
			#Removing AutoFix Directory and Files after reinstall
			Remove-Item "$RPOSAUDIRAUTOFIX" -Recurse -Force
			Remove-Item "$localautofixzip" -Force
			
		}
		Else
		{
			Write-Output "CHECKING FOR MISSING FILES IN AUTO UPDATER                               : PASSED" | Out-File -Append -FilePath $Log
		}
		#Checking ExecutionPolicy
		if ($executionpolicy -ne "RemoteSigned")
		{
			Write-Output "Execution Policy is not set to RemotelySigned Setting Policy Now." | Out-File -Append -FilePath $Log
			Set-ExecutionPolicy RemoteSigned -Force
		}
		Else
		{
			Write-Output "CHECKING POWERSHELL EXECUTION POLICY                                     : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Checking SystemVarible
		if ($envrposautoupdater -ne "C:\Windows\Scripts\RPOS")
		{
			Write-Output "env:RPOSAutoUpdater is not on the PC resetting env varible" | Out-File -Append -FilePath $Log
			[Environment]::SetEnvironmentVariable("RPOSAutoUpdater", "$env:SystemRoot\Scripts\RPOS", "Machine")
		}
		Else
		{
			Write-Output "CHECKING ENV:RPOSAUTOUPDATER system varible                              : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Checking registry keys and uninstall process
		if (($uninstall -eq $null) -or ($undisplayicon -eq $null) -or ($undisplayicon -ne $DisplayIcon) -or ($undisplayname -eq $null) -or ($undisplayname -ne $DisplayName) -or ($undisplayver -eq $null) -or ($undisplayver -ne $DisplayVersion) -or ($unestimatedsize -eq $null) -or ($unestimatedsize -ne $EstimatedSize) -or ($uninstallloc -eq $null) -or ($uninstallloc -ne $InstallLocation) -or ($unnomodify -eq $null) -or ($unnomodify -ne $NoModify) -or ($unnorepair -eq $null) -or ($unnorepair -ne $NoRepair) -or ($unpublisher -eq $null) -or ($unpublisher -ne $Publisher) -or ($ununinstallstring -eq $null) -or ($ununinstallstring -ne $UninstallString))
		{
			
			if ($uninstall -eq $false)
			{
				Write-Output "RPOS Auto-Updater Uninstaller was not found creating folder" | Out-File -Append -FilePath $Log
				New-Item -ItemType Directory $uninstallRegFolder -Force
			}
			
			if (($undisplayicon -eq $null) -or ($undisplayicon -ne $DisplayIcon))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$DisplayIcon" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "DisplayIcon" -Value $DisplayIcon -Force
			}
			
			if (($undisplayname -eq $null) -or ($undisplayname -ne $DisplayName))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$DisplayName" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "DisplayName" -Value $DisplayName -Force
			}
			
			if (($undisplayver -eq $null) -or ($undisplayver -ne $DisplayVersion))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$DisplayVersion" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "DisplayVersion" -Value $DisplayVersion -Force
			}
			
			if (($unestimatedsize -eq $null) -or ($unestimatedsize -ne $EstimatedSize))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$EstimatedSize" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "EstimatedSize" -Value $EstimatedSize -PropertyType DWORD -Force
			}
			
			if (($uninstallloc -eq $null) -or ($uninstallloc -ne $InstallLocation))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$InstallLocation" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "InstallLocation" -Value $InstallLocation -Force
			}
			
			if (($unnomodify -eq $null) -or ($unnomodify -ne $NoModify))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$NoModify" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "NoModify" -Value $NoModify -PropertyType DWORD -Force
			}
			
			if (($unnorepair -eq $null) -or ($unnorepair -ne $NoRepair))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$NoRepair" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "NoRepair" -Value $NoRepair -PropertyType DWORD -Force
			}
			
			if (($unpublisher -eq $null) -or ($unpublisher -ne $Publisher))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$Publisher" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "Publisher" -Value $Publisher -Force
			}
			
			if (($ununinstallstring -eq $null) -or ($ununinstallstring -ne $UninstallString))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$UninstallString" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "UninstallString" -Value $UninstallString -Force
			}
		}
		Else
		{
			Write-Output "CHECKING UNINSTALL REGISTRY KEYS                                         : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Checking Scheduled Tasks to make sure they are correctly setup
		if ($task1 -eq $null)
		{
			Write-Output "PC is missing Rollback RPOS Local Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			schtasks /delete /TN "Rollback RPOS Local" /F
			$URL = $httprollbackxml
			Invoke-WebRequest -Uri $URL -OutFile $localrollbackxml
			schtasks /create /TN "Rollback RPOS Local" /XML "$localrollbackxml"
			Remove-Item "$localrollbackxml" -Force
		}
		Else
		{
			Write-Output "CHECKING ROLLBACK RPOS SCHEDULED TASK                                    : PASSED" | Out-File -Append -FilePath $Log
		}
		
		if ($task2 -eq $null)
		{
			Write-Output "PC is missing RPOS AU updater Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			
			if ($TwoDigPCName -eq "01")
			{
				$URL = $httprposaupc1xml
			}
			
			if ($TwoDigPCName -eq "02")
			{
				$URL = $httprposaupc2xml
			}
			
			if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02"))
			{
				$URL = $httprposaupc3xml
			}
			
			schtasks /delete /TN "RPOS AU updater" /F
			Invoke-WebRequest -Uri $URL -OutFile $localauupdaterxml
			schtasks /create /TN "RPOS AU updater" /XML "$localauupdaterxml"
			Remove-Item "$localauupdaterxml" -Force
			
		}
		Else
		{
			Write-Output "CHECKING UPDATE AUTO UPDATE UPDATER SCHEDULED TASK                       : PASSED" | Out-File -Append -FilePath $Log
		}
		
		if ($task3 -eq $null)
		{
			Write-Output "PC is missing RPOS AU Clear Logs Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			schtasks /delete /TN "Update RPOS Clear Logs" /F
			$URL = $httprposclearlogsxml
			Invoke-WebRequest -Uri $URL -OutFile $localclearlogsxml
			schtasks /create /TN "Update RPOS Clear Logs" /XML "$localclearlogsxml"
			Remove-Item "$localclearlogsxml" -Force
		}
		Else
		{
			Write-Output "CHECKING UPDATE AUTO UPDATE CLEAR LOGS SCHEDULED TASK                    : PASSED" | Out-File -Append -FilePath $Log
		}
		
		if (($task4 -eq $null) -or ($task5 -eq $null))
		{
			Write-Output "PC is Update RPOS A or B Task Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			$URL = $httpfixabtasks
			Invoke-WebRequest -Uri $URL -OutFile $localfixabtasks
			Start-Process $localfixabtasks -Wait
			Start-Sleep -Seconds 10
			Remove-Item "$localfixabtasks" -Force
		}
		
		if ($task6 -eq $null)
		{
			Write-Output "PC RPOS AutoFix Is missing Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			$URL = $httprposauautohealxml
			Invoke-WebRequest -Uri $URL -OutFile $localautoupdaterautohealxml
			schtasks /create /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix" /XML "$localautoupdaterautohealxml"
			Remove-Item "$localautoupdaterautoheal" -Force
		}
		Else
		{
			Write-Output "CHECKING UPDATE RPOS A & B SCHEDULED TASKS                               : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Checking to make sure that the two shoprpos users are installed correctly on the PC
		if ($admins -ne "2")
		{
			Write-Output "One or more of the ShopRPOS Users are not part of the administrator group" | Out-File -Append -FilePath $Log
			net localgroup "Administrators" /add TBC\shprorpos
			net localgroup "Administrators" /add TBC\shprorpos2
		}
		Else
		{
			Write-Output "CHECKING SHOPRPOS ADMINISTRATORS                                         : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Checking Ivanti Registry Keys
		
		if (($ADcount -eq $null) -or ($AdobeAirVer -eq $null) -or ($AdobeDistro -eq $null) -or ($AuVerUpdateDate -eq $null) -or ($BGInfoVer -eq $null) -or ($BGInfoXMLCS -eq $null) -or ($EMVGateway -eq $null) -or ($EMVIPAddress -eq $null) -or ($FMChecksum -eq $null) -or ($InstallComplete -eq $null) -or ($RPOSAUVerPC -eq $null) -or ($RPOSChecksum -eq $null) -or ($RPOSDCount -eq $null) -or ($RPOSDLS -eq $null) -or ($RPOSUDate -eq $null) -or ($RPOSVersion -eq $null) -or ($RPOSVersionLabel -eq $null) -or ($RPOSAUVer -eq $null) -or ($SwitchXMLchecksum -eq $null))
		{
			
			if ($ADcount -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\Adobe DCount" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "Adobe DCount" -Value "0" -Force
			}
			
			if ($AdobeAirVer -eq $null)
			{
				
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AdobeAIRVer" | Out-File -Append -FilePath $Log
				$AdobeAIRFinalverLocal = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -like "*Adobe AIR*" } | Select-Object -ExpandProperty Version
				New-ItemProperty -Path "$LANDeskKeys" -Name "AdobeAIRVer" -Value "$AdobeAIRFinalverLocal" -Force
			}
			
			if ($AdobeDistro -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AdobeDistro" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AdobeDistro" -Value "0" -Force
			}
			
			if ($AuVerUpdateDate -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AuVer UpdateDate" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AuVer UpdateDate" -Value "0" -Force
			}
			
			if ($BGInfoVer -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\BGInfoVersion" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "BGInfoVersion" -Value "0" -Force
			}
			
			if ($BGInfoXMLCS -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\BGInfoXMLchecksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "BGInfoXMLchecksum" -Value "0" -Force
			}
			
			if ($EMVGateway -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\EMVGateway" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "EMVGateway" -Value "0" -Force
			}
			
			if ($EMVIPAddress -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\EMVIPaddress" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "EMVIPaddress" -Value "0" -Force
			}
			
			if ($FMChecksum -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\FlowModule Checksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "FlowModule Checksum" -Value "0" -Force
			}
			
			if ($InstallComplete -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\InstallComplete" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "InstallComplete" -Value "0" -Force
			}
			
			if ($RPOSAUVerPC -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Auto-Updater" | Out-File -Append -FilePath $Log
				$rposauversion = $ReleaseInfo.Substring(1)
				if ($TwoDigPCName -eq "01")
				{
					New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Auto-Updater" -Value "$rposauversion.1" -Force
				}
				
				if ($TwoDigPCName -eq "02")
				{
					New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Auto-Updater" -Value "$rposauversion.2" -Force
				}
				
				if (($TwoDigPCName -ne "01") -or ($TwoDigPCName -ne "02"))
				{
					New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Auto-Updater" -Value "$rposauversion.3" -Force
				}
				
			}
			
			if ($RPOSChecksum -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Checksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Checksum" -Value "0" -Force
			}
			
			if ($RPOSDCount -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS DCount" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS DCount" -Value "0" -Force
			}
			
			if ($RPOSDLS -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS DL Source" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS DL Source" -Value "0" -Force
			}
			
			if ($RPOSUDate -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Update Date" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Update Date" -Value "0" -Force
			}
			
			if ($RPOSVersion -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Version" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Version" -Value "0" -Force
			}
			
			if ($RPOSVersionLabel -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Version Label" | Out-File -Append -FilePath $Log
				[xml]$xmlreadRPOSversion = Get-Content $RPOSVersionLabelXML
				$RPOSxmlversion = $xmlreadRPOSversion.application.versionLabel
				
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Version Label" -Value "$RPOSxmlversion" -Force
			}
			
			if ($RPOSAUVer -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOSAUVer" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOSAUVer" -Value "$ReleaseInfo" -Force
			}
			
			if ($SwitchXMLchecksum -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\SwitchXMLchecksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "SwitchXMLchecksum" -Value "0" -Force
			}
		}
		Else
		{
			Write-Output "CHECKING IVANTI REGISTRY KEYS                                            : PASSED" | Out-File -Append -FilePath $Log
		}
	}
	Else
	{
		$Log = "$RPOSAUDIRLOGS\RPOSAutofix_$AutofixDate.txt"
		Write-Output "There is currently no internet connection on this PC skipping AutoFix check" | Out-File -Append -FilePath $Log
	}
	
}



#########################################################################################################################################
#Function CheckRPOSIcon
function Set-RPOSModuleCheckRPOSIcon
{
	#Checking to see if RPOS Icon exists in $RPOSAUDIRHOME
	If (Test-Path -Path "$RPOSAUDIRBAK\RPOS.lnk") {
		Write-Output "RPOS.lnk is in the RPOS Auto-Updater folder, no action needed" | Out-File -Append -FilePath $Log }
	else {
		Write-Output "RPOS.lnk not found in $RPOSAUDIRBAK coping over from Desktop" | Out-File -Append -FilePath $Log
		Copy-Item "C:\Users\Public\Desktop\RPOS.lnk" -Destination "$RPOSAUDIRBAK" -Force }
	
	#Checking to see if RPOS Icon exists in C:\Users\Public\Desktop
	If (Test-Path -Path "C:\Users\Public\Desktop\RPOS.lnk") {
		Write-Output "RPOS.lnk is in the Public Desktop folder, no action needed" | Out-File -Append -FilePath $Log }
	
	Else {
		Write-Output "RPOS.lnk not found in Public Desktop coping it over to Public Desktop" | Out-File -Append -FilePath $Log
		Copy-Item "$RPOSAUDIRBAK\RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force }
	
	#Checking Permissions of Desktop Icon
	$RPOSPer = icacls "C:\Users\Public\Desktop\RPOS.lnk" | Select-String "Everyone"
	if ($RPOSPer -eq $null) {
		Write-Output "RPOS.lnk on the public desktop does not have everyone permissions, granting permissions" | Out-File -Append -FilePath $Log
		icacls "C:\Users\Public\Desktop\RPOS.lnk" /grant Everyone:F }
	Else {
		Write-Output "RPOS.lnk has everyone permissions not doing anything at this time." | Out-File -Append -FilePath $Log }
	
	#checking to see if Update RPOS is on all users desktop
	If (Test-Path -Path "C:\Users\Public\Desktop\Update RPOS.lnk") {
		Write-Output "Update RPOS.lnk is showing on the public desktop no action is needed" | Out-File -Append -FilePath $Log
	}
	Else {
		Write-Output "Update RPOS.lnk is showing missing at the moment copying it over from backup folder" | Out-File -Append -FilePath $Log
		Copy-Item "$RPOSAUDIRBAK\Update RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force
		icacls "C:\Users\Public\Desktop\Update RPOS.lnk" /grant Everyone:RX
	}
	#Checking to see if FlowModule Desktop Icon Exists
	If (Test-Path -Path "C:\Users\Public\Desktop\FlowModule.lnk") {
		Write-Output "FlowModule Icon was found on the public desktop removing it, to aviod confusion" | Out-File -Append -FilePath $Log
		Remove-Item "C:\Users\Public\Desktop\FlowModule.lnk" -Force }
	Else {
		Write-Output "There was no FlowModule Icon found on the public desktop" | Out-File -Append -FilePath $Log
	}
}

#########################################################################################################################################
#Function FindVersion
function Set-RPOSModuleFindVersion
{
	#Grabbing actual version of RPOS and FlowModule
	[xml]$xmlreadRPOSversion = Get-Content $RPOSVersionLabelXML
	$RPOSxmlversion = $xmlreadRPOSversion.application.versionLabel
	
	#Setting values for RPOS Versions in Registry
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version Label" -Value $RPOSxmlversion -Force
	$rposname2 = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version" -Value $rposname2 -Force
}

#########################################################################################################################################
#Function ThreeDigitIPfromXML
function Set-RPOSModuleThreeDigitIPfromXML
{
	
	$XMLipa1 = $XMLipa | Measure-Object -Character | Select-Object -ExpandProperty Characters
	$XMLipb2 = $XMLipb | Measure-Object -Character | Select-Object -ExpandProperty Characters
	$XMLipc3 = $XMLipc | Measure-Object -Character | Select-Object -ExpandProperty Characters
	$XMLipd4 = $XMLipd | Measure-Object -Character | Select-Object -ExpandProperty Characters
	
	if ($XMLipa1 -eq "1") {
		$script:XMLip1 = "00${XMLipa}." }
	
	if ($XMLipa1 -eq "2") {
		$script:XMLip1 = "0${XMLipa}." }
	
	if ($XMLipa1 -eq "3") {
		$script:XMLip1 = "${XMLipa}." }
	
	if ($XMLipb2 -eq "1") {
		$script:XMLip2 = "00${XMLipb}." }
	
	if ($XMLipb2 -eq "2") {
		$script:XMLip2 = "0${XMLipb}." }
	
	if ($XMLipb2 -eq "3") {
		$script:XMLip2 = "${XMLipb}." }
	
	if ($XMLipc3 -eq "1") {
		$script:XMLip3 = "00${XMLipc}." }
	
	if ($XMLipc3 -eq "2") {
		$script:XMLip3 = "0${XMLipc}." }
	
	if ($XMLipc3 -eq "3") {
		$script:XMLip3 = "${XMLipc}." }
	
	if ($XMLipd4 -eq "1") {
		$script:XMLip4 = "00${XMLipd}" }
	
	if ($XMLipd4 -eq "2") {
		$script:XMLip4 = "0${XMLipd}" }
	
	if ($XMLipd4 -eq "3") {
		$script:XMLip4 = "${XMLipd}" }
	
}

#####################################################################################################################################
# Function CheckReportXML
function Set-RPOSModuleCheckReportXML
{
	
	#Get Date for last inventory scan
	$InventoryTimeScan = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"
	
	#Getting Host IP address
	$HostIPAddress = "$ipmain1.$ipmain2.$ipmain3.$ipmain4"
	
	#Setting Up Network Share on TPOSDIST01
	New-PSDrive -Name O -PSProvider FileSystem -Root "\\tposdist01\MbPnTRr7DR$\Bucket"
	
	Write-Output "Grabbing Registry Value Data to send data back to XML Database on TPOSDIST01 for processing" | Out-File -Append -FilePath $Log
	
	#Grabbing Registry Values	
	$XMLName = $Env:COMPUTERNAME
	$RegAdobeAIRVer = (Get-ItemProperty -Path $LANDeskKeys -Name AdobeAIRVer).AdobeAIRVer
	$RegAdobeAIRDistro = (Get-ItemProperty -Path $LANDeskKeys -Name AdobeDistro).AdobeDistro
	$RegAuVerUpdateDate = (Get-ItemProperty -Path $LANDeskKeys -Name "AuVer UpdateDate")."AuVer UpdateDate"
	$RegBGInfoVersion = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoVersion).BGInfoVersion
	$RegBGInfoXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoXMLchecksum).BGInfoXMLchecksum
	$RegEMVIPaddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress")."EMVIPaddress"
	$RegEMVGateway = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVGateway")."EMVGateway"
	$RegFlowModulechecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "FlowModule Checksum")."FlowModule Checksum"
	$RegRPOSAutoUpdater = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Auto-Updater")."RPOS Auto-Updater"
	$RegRPOSChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Checksum")."RPOS Checksum"
	$RegRPOSDCount = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Dcount")."RPOS Dcount"
	$RegRPOSDLSource = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS DL Source")."RPOS DL Source"
	$RegRPOSUpdatedate = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Update Date")."RPOS Update Date"
	$RegRPOSVersion = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version")."RPOS Version"
	$RegRPOSVersionLabel = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version Label")."RPOS Version Label"
	$RegRPOSAUVer = (Get-ItemProperty -Path $LANDeskKeys -Name RPOSAUVer).RPOSAUVer
	$RegRPOSSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	$RegAdobeDownloadCount = (Get-ItemProperty -Path $LANDeskKeys -Name "Adobe DCount")."Adobe DCount"
	$RegEMVInstalled = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVVeriFoneInstalled")."EMVVeriFoneInstalled"
	
	Write-Output "Starting to build XML file to send over to TPOSDIST01" | Out-File -Append -FilePath $Log
	
	#Creating New XML for Data Deposits
	[System.XML.XMLDocument]$RPOSInventory = New-Object System.XML.XMLDocument
	
	#Creating first Root Element	
	[System.XML.XMLElement]$RPOSRoot = $RPOSInventory.CreateElement("RPOS")
	$RPOSInventory.appendChild($RPOSRoot)
	
	# Append as child to an existing node
	[System.XML.XMLElement]$RPOSAttributes1 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("name"))
	$RPOSAttributes1.InnerText = "$XMLName"
	[System.XML.XMLElement]$RPOSAttributes2 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("address"))
	$RPOSAttributes2.InnerText = "$HostIPAddress"
	[System.XML.XMLElement]$RPOSAttributes3 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSVersion"))
	$RPOSAttributes3.InnerText = "$RegRPOSVersion"
	[System.XML.XMLElement]$RPOSAttributes4 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AutoUpdateVersion"))
	$RPOSAttributes4.InnerText = "$RegRPOSAUVer"
	[System.XML.XMLElement]$RPOSAttributes5 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSUpdateDate"))
	$RPOSAttributes5.InnerText = "$RegRPOSUpdatedate"
	[System.XML.XMLElement]$RPOSAttributes6 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSDownloadSource"))
	$RPOSAttributes6.InnerText = "$RegRPOSDLSource"
	[System.XML.XMLElement]$RPOSAttributes7 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSCheckSum"))
	$RPOSAttributes7.InnerText = "$RegRPOSChecksum"
	[System.XML.XMLElement]$RPOSAttributes8 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("FlowModuleCheckSum"))
	$RPOSAttributes8.InnerText = "$RegFlowModulechecksum"
	[System.XML.XMLElement]$RPOSAttributes9 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSVersionLabel"))
	$RPOSAttributes9.InnerText = "$RegRPOSVersionLabel"
	[System.XML.XMLElement]$RPOSAttributes10 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeAIRVersion"))
	$RPOSAttributes10.InnerText = "$RegAdobeAIRVer"
	[System.XML.XMLElement]$RPOSAttributes11 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeDownloadDistro"));
	$RPOSAttributes11.InnerText = "$RegAdobeAIRDistro"
	[System.XML.XMLElement]$RPOSAttributes12 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeDownloadCount"));
	$RPOSAttributes12.InnerText = "$RegAdobeDownloadCount"
	[System.XML.XMLElement]$RPOSAttributes13 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVInstalled"));
	$RPOSAttributes13.InnerText = "$RegEMVInstalled"
	[System.XML.XMLElement]$RPOSAttributes14 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVIP"));
	$RPOSAttributes14.InnerText = "$RegEMVIPaddress"
	[System.XML.XMLElement]$RPOSAttributes15 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVGatewayIP"));
	$RPOSAttributes15.InnerText = "$RegEMVGateway"
	[System.XML.XMLElement]$RPOSAttributes16 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSDownloadCt"));
	$RPOSAttributes16.InnerText = "$RegRPOSDCount"
	[System.XML.XMLElement]$RPOSAttributes17 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("BGInfoVersion"));
	$RPOSAttributes17.InnerText = "$RegBGInfoVersion"
	[System.XML.XMLElement]$RPOSAttributes18 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("BGInfoXMLChecksum"));
	$RPOSAttributes18.InnerText = "$RegBGInfoXMLchecksum"
	[System.XML.XMLElement]$RPOSAttributes19 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("SwitchXMLChecksum"));
	$RPOSAttributes19.InnerText = "$RegRPOSSwitchXMLchecksum"
	[System.XML.XMLElement]$RPOSAttributes20 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AuUpdateDate"));
	$RPOSAttributes20.InnerText = "$RegAuVerUpdateDate"
	[System.XML.XMLElement]$RPOSAttributes21 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("InventoryDate"));
	$RPOSAttributes21.InnerText = "$InventoryTimeScan"
	
	#Save File
	$RPOSInventory.Save("$RPOSAUDIRHOME\$XMLDeposit")
	
	#Copying Data file To TPOSDIST01
	Copy-Item "$RPOSAUDIRHOME\$XMLDeposit" -Destination "\\tposdist01\MbPnTRr7DR$\Bucket" -Force
	Write-Output "File $RPOSAUDIRHOME\$XMLDeposit has been processed and moved to TPOSDIST01" | Out-File -Append -FilePath $Log
	
	#Removing File from Local PC.
	Remove-Item "$RPOSAUDIRHOME\$XMLDeposit" -Force
	Write-Output "Removed $RPOSAUDIRHOME\$XMLDeposit from Local PC" | Out-File -Append -FilePath $Log
	
	Remove-PSDrive -Name O -Force
}

#####################################################################################################################################
# Function CheckBGInfoUpdatewithEMV
function Set-RPOSModuleCheckBGInfoUpdateandEMV
{
	
	#Reading to see if EMV is currently installed on the PC, if it is nothing will happen, otherwise it will start recording the proper reg keys
	[xml]$xmlread = Get-Content $XMLEMVfileRPOS
	$script:Reademv = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
	
	#Checking to see if EMV has been installed already on PC
	$script:ValidateEMV = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVVeriFoneInstalled")."EMVVeriFoneInstalled"
	Write-Output "Does the PC have the EMV files installed which is $ValidateEMV" | Out-File -Append -FilePath $Log
	Write-Output "Auto-Updater is checking to see if EMV XML files and BGInfo needs to be updated" | Out-File -Append -FilePath $Log
	
	#Checking to see if the site is complete and has the correct information in RPOS XML
	if (($ValidateEMV -eq $null) -and ($Result -ne $IPZero) -and ($Reademv -eq $null)) {
		Write-Output "Setup has determained the RPOS needs to switch over to EMV for chip and pin processing." | Out-File -Append -FilePath $Log
		Write-Output "Starting setupEMVxml fuction" | Out-File -Append -FilePath $Log
		Set-RPOSModulesetupEMVxml }
	Else {
		Write-Output "RPOS has already installed EMV on this PC" | Out-File -Append -FilePath $Log }
	
	#Checking to see if the checksum of UpdatedXML's is updated or not.
	if ($checkBGInfoxml -ne $RegBGInfoXMLchecksum)
	{
		
		#adding newer update hash in registry
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoXMLchecksum" -Value $checkBGInfoxml -Force
		
		Write-Output "Checking to see if BGInfo is at the most current verion" | Out-File -Append -FilePath $Log
		Write-Output "Auto-Updater is proceeding with check an newer version of BGInfo EMV Complete" | Out-File -Append -FilePath $Log
		Write-Output "RPOS Auto-Updater is starting to check to see if there is an update for BGInfo" | Out-File -Append -FilePath $Log
		$BGInfoUpdateKey = (Get-ItemProperty -Path $LANDeskKeys -Name "BGInfoVersion")."BGInfoVersion"
		Write-Output "The current version of BGInfo on this PC is $BGInfoUpdateKey" | Out-File -Append -FilePath $Log
		
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/BGInfo/bginfoupdater.xml"
		
		$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
		$m = $xmlread.BGInfoUpdate.$switchvalue.version
		$n = $xmlread.BGInfoUpdate.$switchvalue.hash
		$o = $xmlread.BGInfoUpdate.$switchvalue.URL
		
		Write-Output "Current version of BGinfo on rpos.tbccorp.com is $m" | Out-File -Append -FilePath $Log
		
		if ($BGInfoUpdateKey -ne $m)
		{
			Write-Output "Powershell has identified that PC is not running the current version of BGinfo from TBC starting backing up and downloading" | Out-File -Append -FilePath $Log
			Remove-Item "$RPOSAUDIRBAK\TBC.bgi.bak" -Force
			Write-Output "Removed old backup of BGInfo in backup folder" | Out-File -Append -FilePath $Log
			Copy-Item -Path "C:\bginfo\TBC.bgi" -Destination "$RPOSAUDIRBAK\TBC.bgi.bak" -PassThru -Force | Out-File -Append -FilePath $Log
			Write-Output "Backed up previous version of BGInfo to $RPOSAUDIRBAK"
			Remove-Item $BGInfoLocalFile -Force
			
			Write-Output "Starting download of New BGInfo file" | Out-File -Append -FilePath $Log
			
			#Downloading hash checksum for swutch value
			Copy-Item -Path "$o" -Destination "$BGInfoLocalFile" -Force
			Write-Output "TBC.bgi has been downloaded" | Out-File -Append -FilePath $Log
			
			Write-Output "Checking file hash of the new bginfo file" | Out-File -Append -FilePath $Log
			$LocalBGInfoHash = Get-FileHash -Path $BGInfoLocalFile -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$LocalBGInfoHash is the hash of the BGInfo file that was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$n is the hash of the BGInfo file on the server" | Out-File -Append -FilePath $Log
			
			if ($LocalBGInfoHash -ne $n)
			{
				while ($sumnumbginfo -lt 5)
				{
					$sumnumbginfo += 1
					Write-Output "The BGInfo file that was download is not the correct hash trying to re-download count equals $sumnumbginfo" | Out-File -Append -FilePath $Log
					Write-Output "Starting download of New BGInfo file" | Out-File -Append -FilePath $Log
					
					#Downloading hash checksum for swutch value
					Copy-Item -Path "$o" -Destination "$BGInfoLocalFile" -Force
					Write-Output "TBC.bgi has been downloaded" | Out-File -Append -FilePath $Log
					$LocalBGInfoHash = Get-FileHash -Path $BGInfoLocalFile -Algorithm MD5 | select -ExpandProperty hash
					if ($LocalBGInfoHash -eq $n) { break }
				}
			}
			
			
			
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoVersion" -Value $m -Force
			
			#Restarting PC to ensure that the newer BGInfo installs correctly
			Write-Output "Restarting PC to ensure settings on BGInfo display on store PC desktop Be Right Back!!!" | Out-File -Append -FilePath $Log
			Restart-Computer -Force
			Start-Sleep -Seconds 60
		}
		Else
		{
			Write-Output "Powershell has identified this PC is running the current version of BGinfo which is $BGInfoUpdateKey" | Out-File -Append -FilePath $Log
		}
		
	}
	Else
	{
		Write-Output "Auto-Updater doesn't see any changes to BGInfo.xml files bypassing BGInfo check" | Out-File -Append -FilePath $Log
	}
	
	if ($Result -ne $IPZero)
	{
		Write-Output "Starting to check to see if any IP changes were made for the RPOS-Preferences file to BGInfo" | Out-File -Append -FilePath $Log
		$BGEMVIPAddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress")."EMVIPaddress"
		$EMVXMLVeriFone3digitIP = "$XMLip1$XMLip2$XMLip3$XMLip4"
		Write-Output "IP address of the RPOS-Preferences XML is $EMVXMLVeriFone3digitIP and current registry IP is $BGEMVIPAddress" | Out-File -Append -FilePath $Log
		
		#Checking to see if the IP address in registry for BGInfo EMV needs to be changed from the XML
		if ($BGEMVIPAddress -ne $EMVXMLVeriFone3digitIP)
		{
			Write-Output "BGInfo Registry key for IP address desktop was not matching updating with newer IP address" | Out-File -Append -FilePath $Log
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Value $EMVXMLVeriFone3digitIP -Force
			$RPOSEMVxmlgateway = "$XMLip1$XMLip2${XMLip3}001"
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Value $RPOSEMVxmlgateway -Force
		}
		Else
		{
			Write-Output "BGInfo Reigstry key for IP address desktop was matching the current RPOSXML" | Out-File -Append -FilePath $Log
		}
		
		#Checking to see if XML ETIM IP address is current with EMV Ip address in RPOS-preferences.xml
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$script:ResultEMV = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
		
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for NON-EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$Read = $xmlread.ApplicationPreferences.creditCardTerminalUrl | Select-Object -ExpandProperty "#cdata-section"
		$script:ResultNonEMV = $Read -replace ":9001"
		
		#If the IP address for the verifone device has changed change all the rest of the xml files and values.
		if (($ResultEMV -ne $ResultNonEMV) -and ($ResultEMV -ne $null))
		{
			Write-Output "Setup has found there has been a change in the RPOS-Perferences Verifone EMV IP address" | Out-File -Append -FilePath $Log
			Write-Output "Changing IP addresses from $ResultNonEMV to $ResultEMV" | Out-File -Append -FilePath $Log
			
			$ResultEMVtoNonEMV = "${ResultEMV}:9001"
			
			#putting current EMV IP address in Flow RPOS-Preferences.xml
			[xml]$Flowxml = get-content "$XMLEMVfileFlow"
			$Flowxml.applicationpreferences.pointSCAPaymentDeviceIPAddress.'#cdata-section' = "$ResultEMV"
			$Flowxml.Save("$XMLEMVfileFlow")
			
			#putting current EMV IP address with :9001 in RPOS RPOS-Preferences.xml
			[xml]$RPOSxml = get-content "$XMLEMVfileRPOS"
			$RPOSxml.applicationpreferences.creditcardterminalurl.'#cdata-section' = "$ResultEMVtoNonEMV"
			$RPOSxml.Save("$XMLEMVfileRPOS")
			
			#putting current EMV IP address with :9001 in FLOW RPOS-Preferences.xml
			[xml]$Flowxml = get-content "$XMLEMVfileFlow"
			$Flowxml.applicationpreferences.creditcardterminalurl.'#cdata-section' = "$ResultEMVtoNonEMV"
			$Flowxml.Save("$XMLEMVfileFlow")
			
		}
	}
	
	Write-Output "BGInfo IP address for RPOS Preferences and New BGInfo is complete exiting this section of the Auto-Updater" | Out-File -Append -FilePath $Log
}

#####################################################################################################################################
#Function setupEMVxml
function Set-RPOSModulesetupEMVxml
{
	
	Write-Output "Starting RPOS EMV preferences XML Encoding" | Out-File -Append -FilePath $Log
	Write-Output "Name of the user profile, in which PowerShell will be working with is $profilename" | Out-File -Append -FilePath $Log
	if ($Result -eq $IPZero)
	{
		Write-Output "The RPOS XML has an IP address of $Result not changing the xml's on the PC. Exiting the portion of RPOS Auto-Updater EMV XML Edit" | Out-File -Append -FilePath $Log
	}
	Else
	{
		Write-Output "RPOS preferences file is $XMLEMVfileRPOS" | Out-File -Append -FilePath $Log
		#Starting To Backup Preferences Files
		Write-Output "PowerShell is now backing up Store User Profile RPOS Preferences files" | Out-File -Append -FilePath $Log
		Copy-Item -Path $XMLEMVfileRPOS -Destination $XMLEMVRPOSbak -PassThru -Force | Out-File -Append -FilePath $Log
		
		if (Test-Path $XMLEMVfileFlow)
		{
			Write-Output "Flow RPOS preferences file was found" | Out-File -Append -FilePath $Log
			Write-Output "Flow RPOS preferences file is $XMLEMVfileFlow" | Out-File -Append -FilePath $Log
			#Starting To Backup Preferences Files
			Write-Output "PowerShell is now backing up Store User Profile RPOS Flow Preferences files" | Out-File -Append -FilePath $Log
			Copy-Item -Path $XMLEMVfileFlow -Destination $XMLEMVFLOWbak -PassThru -Force | Out-File -Append -FilePath $Log
		}
		Else
		{
			Write-Output "Flow RPOS preferences file was not found no backup needed" | Out-File -Append -FilePath $Log
		}
		
		#Starting To pull XML's into PowerShell to start editing
		Write-Output "$script:Result is the IP address that will be inserted into pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
		Write-Output "Starting to edit RPOS Preferences file with $Result IP address in Append Child pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
		
		#Starting to edit RPOS Preferences File
		$xmlrpos = [xml](Get-Content $XMLEMVfileRPOS)
		$child1 = $xmlrpos.CreateElement("pointSCAPaymentDeviceIPAddress")
		$child1.InnerXml = "<![CDATA[$Result]]>"
		$xmlrpos.ApplicationPreferences.AppendChild($child1)
		$xmlrpos.Save("$XMLEMVfileRPOS")
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Value "true" -Force
		
		if (Test-Path $XMLEMVfileFlow)
		{
			Write-Output "Starting to edit RPOS Flow Preferences file with $Result IP address in Append Child pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
			#Starting to edit RPOS Flow Preferences File
			$xmlflow = [xml](Get-Content $XMLEMVfileFlow)
			$child2 = $xmlflow.CreateElement("pointSCAPaymentDeviceIPAddress")
			$child2.InnerXml = "<![CDATA[$Result]]>"
			$xmlflow.ApplicationPreferences.AppendChild($child2)
			$xmlflow.Save("$XMLEMVfileFlow")
			
			
		}
		Else
		{
			
			Write-Output "Flow RPOS preferences file was not found no editing needed" | Out-File -Append -FilePath $Log
		}
		
		Write-Output "Script is done editing preferences files, it is complete" | Out-File -Append -FilePath $Log
		
		#Grabbing the IP address from perviously used value above
		$RPOSEMVxmlIPaddress = "$XMLip1$XMLip2$XMLip3$XMLip4"
		$RPOSEMVxmlgateway = "$XMLip1$XMLip2${XMLip3}001"
		
		#Exporting IP address to BGInfo text file
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Value $RPOSEMVxmlIPaddress -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Value $RPOSEMVxmlgateway -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Value "true" -Force
	}
}

#########################################################################################################################################
#Function InstallAIR
function Set-RPOSModuleInstallAIR
{
	#Checking to see what version of Adobe AIR is installed on PC
	$AdobeAIRverLocal = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -like "*Adobe AIR*" } | Select-Object -ExpandProperty Version
	Write-Output "The current version of Adobe AIR is $AdobeAIRverLocal" | Out-File -Append -FilePath $Log
	
	#Getting most current version of Adobe AIR from rpos.tbccorp.com
	Write-Output "Checking $prod to see what version of Adobe AIR is the current" | Out-File -Append -FilePath $Log
	$AdobeURL = "http://$prod/AdobeAirCheck.xml"
	$adobexmlread = [xml](New-Object System.Net.WebClient).downloadstring($AdobeURL)
	$adobever = $adobexmlread.AdobeAir.AdobeAirVer
	$adobehash = $adobexmlread.AdobeAir.AdobeHashCheck
	Write-Output "The most current version of Adobe AIR on server is $adobever" | Out-File -Append -FilePath $Log
	
	if (($AdobeAIRverLocal -ne "$adobever") -or ($AdobeAIRverLocal -eq $null))
	{
		#Removing local copy of Adobe AIR on RPOS Script Local Folder
		Remove-Item $AdobeAIRInstallRoot -Force
		Write-Output "The current version of Adobe AIR is the incorrect and out of date version proceeding with upgrade" | Out-File -Append -FilePath $Log
		Write-Output "Proceeding to check to see most current version of Adobe AIR is on Self elected subnet" | Out-File -Append -FilePath $Log
		
		#Starting Self Electing function
		Set-RPOSModuleStartSelfElect
		$selected = Get-Content $LiveIP
		
		foreach ($electedPC in $selected)
		{
			$path = "\\$electedPC\cs18ebyi3$"
			$pathresults = Test-Path -path $path
			if ($pathresults -eq $false)
			{
				Write-Output "Powershell was unable to find network share on $electedPC trying next PC" | Out-File -Append -FilePath $log
			}
			Else
			{
				New-PSDrive -Name M FileSystem "\\$electedPC\cs18ebyi3$"
				$adobeshare01 = Test-Path "M:\$AdobeInstallerFile"
				$adobesharehash01 = Get-Item -Path "M:\$AdobeInstallerFile" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
				if (($adobeshare01 -eq $true) -and ($adobesharehash01 -eq $adobehash))
				{
					Write-Output "Powershell has found correct Adobe AIR Installer on local PC $electedPC starting transfer" | Out-File -Append -FilePath $Log
					Copy-Item "M:\$AdobeInstallerFile" -Destination $AdobeAIRInstallRoot -PassThru -Force | Out-File -Append -FilePath $Log
					Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Value "$electedPC" -Force
					Write-Output "Powershell succssfully copied Adobe Air Installer on local PC $electedPC" | Out-File -Append -FilePath $Log
					Remove-PSDrive -Name M; break
				}
				Else
				{
					Write-Output "Powershell was unable to find correct matching version of Adobe AIR on $electedPC trying different PC" | Out-File -Append -FilePath $log
					Remove-PSDrive -Name M
				}
			}
		}
		
		#Running MD5 checksum to ensure files are NOT! corrupt
		$adobelocalhash = Get-FileHash -Path $AdobeAIRInstallRoot -Algorithm MD5 | select -ExpandProperty hash
		
		#If Hash files are not matching trying to re-download
		if ($adobelocalhash -ne $adobehash)
		{
			while ($adobesumnum -lt 5)
			{
				$adobesumnum += 1
				Write-Output "$adobesumnum is the current count of the amount of times script has tried redownloading max 5 times" | Out-File -Append -FilePath $Log
				#Setting download counter due to download count		
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Adobe DCount" -Value $adobesumnum -Force
				Write-Output "Starting re-download of Adobe AIR for the $adobesumnum time" | Out-File -Append -FilePath $Log
				
				#Removing Local copy of Adobe AIR
				Write-Output "Removing current local copy of Adobe AIR if it exists" | Out-File -Append -FilePath $Log
				Remove-Item $AdobeAIRInstallRoot -Force
				
				#Downloading RPOS and Flow Files
				Write-Output "Starting to download never version of Adobe AIR"
				$downloadAIR = "http://$prod/$AdobeInstallerFile"
				Invoke-WebRequest -Uri $downloadAIR -OutFile $AdobeAIRInstallRoot
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Value "DistroServer" -Force
				Write-Output "$AdobeInstallerFile has been downloaded" | Out-File -Append -FilePath $Log
				
				#Running MD5 checksum to ensure files are NOT! corrupt
				$adobelocalhash = Get-FileHash -Path $AdobeAIRInstallRoot -Algorithm MD5 | select -ExpandProperty hash
				Write-Output "$adobelocalhash is the MD5 hash of Adobe AIR of what was downloaded" | Out-File -Append -FilePath $Log
				Write-Output "$adobehash is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
				if ($adobehash -eq $adobelocalhash) { Break }
			}
		}
		
		if (($adobesumnum -ne "5") -and ($adobehash -eq $adobelocalhash))
		{
			#Starting install of adobe air
			Write-Output "Adobe AIR is the correct hash continuing" | Out-File -Append -FilePath $Log
			Write-Output "$adobelocalhash is the MD5 hash of Adobe AIR of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$adobehash is the MD5 has of Adobe AIR that server has" | Out-File -Append -FilePath $Log
			
			#Coping file over to subnet file share
			Remove-Item $AdobeAIRInstallShare -Force
			Write-Output "Starting to copy over for subnet sharing" | Out-File -Append -FilePath $Log
			Copy-Item $AdobeAIRInstallRoot -Destination $AdobeAIRInstallShare -PassThru -Force | Out-File -Append -FilePath $Log
			Write-Output "$AdobeAIRInstallRoot has been copied for sharing on $AdobeAIRInstallShare" | Out-File -Append -FilePath $Log
			
			#Killing RPOS and Flow if running
			Write-Output "Closing RPOS and FlowModule to start the install"
			Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
			Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
			
			#Uninstalling Adobe AIR
			$app = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -match "Adobe AIR" }
			$app.Uninstall()
			Write-Output "Adobe AIR has been uninstalled" | Out-File -Append -FilePath $Log
			
			#Installing Adobe AIR with Newer Version
			Start-Process $AdobeAIRInstallRoot -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
			Write-Output "Adobe AIR has been installed for brand new install or upgrade" | Out-File -Append -Filepath $Log
			Write-Output "Adobe Installer Portion of RPOS Auto-Updater is exiting" | Out-File -Append -Filepath $Log
		}
	}
	Else {
		Write-Output "Adobe AIR on this PC is $AdobeAIRverLocal which is the most current verison exiting and processing with RPOS Auto-Updater" | Out-File -Append -FilePath $Log }
	
	#Scanning PC for the most current version of Adobe AIR
	$AdobeAIRFinalverLocal = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -like "*Adobe AIR*" } | Select-Object -ExpandProperty Version
	
	#Putting the most current version of Adobe AIR in LANDesk Registry Key
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeAIRVer" -Value "$AdobeAIRFinalverLocal" -Force
}

##########################################################################################################################################
#Function What PC Am I?
function Set-RPOSModuleWhatPCAmI
{
	
	If ($name -match "S[0-9][0-9][0-9]") {
		Write-Output "Powershell has found out that this PC is Store PC" | Out-File -Append -FilePath $Log
		$script:pname = $name
		$script:pname = $pname.Substring(1, 3)
		$script:switchvalue = "store${pname}"
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchstore.xml"
		$script:XMLswitchchecksum = "switchstore"
		$script:XMLbginfochecksum = "switchbginfo" }
	
	if ($name -match "STAC[0-9][0-9][0-9]") {
		Write-Output "Powershell has found out that this PC is TAC Warehouse PC" | Out-File -Append -FilePath $Log
		$script:taclist = $name
		$script:taclist = $taclist.Substring(0, 7)
		$script:switchvalue = $taclist
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchtac.xml"
		$script:XMLswitchchecksum = "switchtac"
		$script:XMLbginfochecksum = "switchbginfo" }
	
	if ($name -match "CT[0-9][0-9]") {
		Write-Output "Powershell has found out that this PC is Carroll Tire PC" | Out-File -Append -FilePath $Log
		$script:ctpc = $name
		$script:ctpc = $ctpc.Substring(0, 4)
		$script:switchvalue = $ctpc
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchct.xml"
		$script:XMLswitchchecksum = "switchct"
		$script:XMLbginfochecksum = "switchbginfo" }
	
	if (($name -notmatch "S[0-9][0-9][0-9]") -and ($name -notmatch "STAC[0-9][0-9][0-9]") -and ($name -notmatch "CT[0-9][0-9]")) {
		Write-Output "Powershell has found out that this PC is Corporate, Personal and/or out of the normal PC naming scheme." | Out-File -Append -FilePath $Log
		$script:switchvalue = $name
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchcorp.xml"
		$script:XMLswitchchecksum = "switchcorp"
		$script:XMLbginfochecksum = "switchbginfo" }
}

##########################################################################################################################################
#Function Start Self Elect Process
function Set-RPOSModuleStartSelfElect
{
	#Checking to see if LiveNames.txt is already there
	
	If (Test-Path $LiveIP)
	{
		Write-Output "LiveIP text file was found in the auto-updater grabbing PC names from file" | Out-File -Append -FilePath $Log
		$selected = Get-Content $LiveIP
		Write-Output "$selected PC discovered" | Out-File -Append -FilePath $Log
	}
	Else
	{
		#Checking local PC IP address, then scan network, then convert IP Addresses to PC Names
		$finalIP = "$ipmain1.$ipmain2.$ipmain3."
		Write-Output "$finalIP is the subnet that this PC will be scanning" | Out-File -Append -FilePath $Log
		$ping = New-Object System.Net.Networkinformation.ping
		
		Write-Output "Starting scan of PC's on the subnet" | Out-File -Append -FilePath $Log
		1 .. 254 | % { $ping.Send("${finalIP}$_", 500) } | select-object -ExpandProperty Address | Select -ExpandProperty IPAddressToString | Out-File -Append -FilePath $LiveIP
		#Getting getting rid of all the 10.x.x.1 addresses
		$getrid = Get-Content $LiveIP
		$getrid | Where-Object { $_ -notcontains "${finalIP}1" } | Set-Content $LiveIP
		#########################################################
		#Changed on 2-21-2018 NetBIOS Names have been disabled  #
		#########################################################
		#End of getting rid of all the 10.x.x.1 addresses
		#$IPs = Get-Content $LiveIP
		#Write-Output "Starting to get PC names from IP list" | Out-File -Append -FilePath $Log
		#$IPs | ForEach-Object { ([system.net.dns]::GetHostByAddress($_)).hostname } | Out-File -Append -FilePath $LiveNames
		#########################################################
		$script:selected = Get-Content $LiveIP
		Write-Output "$script:selected PC discovered" | Out-File -Append -FilePath $Log
	}
}

##########################################################################################################################################
#Function Install POS - Default Installer Process for RPOS
function Set-RPOSModuleInstallPOS
{
	
	#Switching registry key to zero that tells the Auto-Updater desktop if it was a successful install or not
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "0" -Force
	
	#Adding updated date of RPOS update in registry for LANDesk Inventory
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Value $datereg -Force
	
	#Downloading Adobe Arh file if needed
	if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
	Else
	{
		$arh = "$server/arh.exe"
		Invoke-WebRequest -Uri $arh -OutFile $arhfile
		Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
	}
	
	#Backing Up Current RPOS Build determaining if it is EXE or AIR file.
	Write-Output "Removing old bak files if there are in the BAK directory" | Out-File -Append -FilePath $Log
	Remove-Item "$RPOSfileBakair" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$flowfileBakair" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$RPOSfileBakexe" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$flowfileBakexe" -Force | Out-File -Append -FilePath $Log
	
	if ($drfile -eq "RPOS.air")
	{
		Write-Output "Powershell has found an older version of RPOS AIR on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$drfile" -Destination $RPOSfileBakair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$drfile has been copied to $RPOSfileBakair Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq "FlowModule.air")
	{
		Write-Output "Powershell has found an older version of FlowModule AIR on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$dffile" -Destination $flowfileBakair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$dffile has been copied to $flowfileBakair Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($drfile -eq "RPOS.exe")
	{
		Write-Output "Powershell has found an older version of RPOS EXE on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$drfile" -Destination $RPOSfileBakexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$drfile has been copied to $RPOSfileBakexe Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq "FlowModule.exe")
	{
		Write-Output "Powershell has found an older version of FlowModule EXE on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$dffile" -Destination $flowfileBakexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$dffile has been copied to $flowfileBakexe Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($drfile -eq $null)
	{
		Write-Output "Powershell did not find an older version of RPOS on this PC" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq $null)
	{
		Write-Output "Powershell did not find an older version of FlowModule on this PC" | Out-File -Append -FilePath $Log
	}
	
	#Removing Previous build files
	#If there is an EXE file delete AIR file and if there is an AIR file delete EXE files	
	if (($rfile -ne $drfile) -and ($drfile -eq "RPOS.air"))
	{
		Write-Output "Script found it is downloading EXE files deleting all AIR Files" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\rpos.air" -Force
		Remove-Item "$RPOSAUDIRHOME\FlowModule.air" -Force
		Remove-Item "$RPOSAUDIRBUILD\rpos.air" -Force
		Remove-Item "$RPOSAUDIRBUILD\FlowModule.air" -Force
	}
	if (($rfile -ne $drfile) -and ($drfile -eq "RPOS.exe"))
	{
		Write-Output "Script found it is downloading AIR files deleting all EXE Files" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\rpos.exe" -Force
		Remove-Item "$RPOSAUDIRHOME\FlowModule.exe" -Force
		Remove-Item "$RPOSAUDIRBUILD\rpos.exe" -Force
		Remove-Item "$RPOSAUDIRBUILD\FlowModule.exe" -Force
	}
	if (($rfile -eq $drfile) -and ($ffile -eq $dffile))
	{
		Write-Output "Script found the same AIR or EXE files deleting the same file" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\$drfile" -Force
		Remove-Item "$RPOSAUDIRHOME\$dffile" -Force
	}
	if (($drfile -eq $null) -and ($dffile -eq $null))
	{
		Write-Output "Powershell didn't find any files FlowModule or RPOS no need to delete" | Out-File -Append -FilePath $Log
	}
	
	#Testing to see if there was already a subnet scan completed, if so it will use the current LiveNames file and run it against it
	if (Test-Path $LiveIP)
	{
		Write-Output "LiveIP.txt has been found on the PC at this time proceeding grabbing content from text file" | Out-File -Append -FilePath $Log
		$selected = Get-Content $LiveIP
	}
	Else
	{
		Write-Output "There was no subnet scanning for Adobe AIR previously starting subnet scan" | Out-File -Append -FilePath $Log
		#Starting Self-Electing Process
		Set-RPOSModuleStartSelfElect
		$selected = Get-Content $LiveIP
	}
	
	
	#For each PC in the list of Livenames.txt start checking for network share and local files
	foreach ($electedPC in $selected)
	{
		$path = "\\$electedPC\cs18ebyi3$"
		$pathresults = Test-Path -path $path
		if ($pathresults -eq $false)
		{
			Write-Output "Powershell was unable to find network share on $electedPC trying next PC" | Out-File -Append -FilePath $log
		}
		Else
		{
			New-PSDrive -Name M FileSystem "\\$electedPC\cs18ebyi3$"
			$rshare01 = Test-Path "M:\$rfile"
			$fshare01 = Test-Path "M:\$ffile"
			$rsharehash01 = Get-Item -Path "M:\$rfile" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
			$fsharehash01 = Get-Item -Path "M:\$ffile" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
			if (($rshare01 -eq $true) -and ($fshare01 -eq $true) -and ($rsharehash01 -eq $xr) -and ($fsharehash01 -eq $xf))
			{
				Write-Output "Powershell has found correct RPOS and FLOW on local PC $electedPC starting transfer" | Out-File -Append -FilePath $Log
				Copy-Item "M:\$rfile" -Destination $RPOSfile -PassThru -Force | Out-File -Append -FilePath $Log
				Copy-Item "M:\$ffile" -Destination $flowfile -PassThru -Force | Out-File -Append -FilePath $Log
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Value "$electedPC" -Force
				Write-Output "Powershell succssfully copied RPOS and FLOW on local PC $electedPC" | Out-File -Append -FilePath $Log
				Remove-PSDrive -Name M; break
			}
			Else
			{
				Write-Output "Powershell was unable to find correct matching version of RPOS on $electedPC trying different PC" | Out-File -Append -FilePath $log
				Remove-PSDrive -Name M
			}
		}
	}
	
	#Running MD5 checksum to ensure files are NOT! corrupt
	$nhashRPOS = Get-FileHash -Path $RPOSfile -Algorithm MD5 | select -ExpandProperty hash
	$nhashFlow = Get-FileHash -Path $flowfile -Algorithm MD5 | select -ExpandProperty hash
	
	#If Hash files are not matching trying to re-download
	if (($nhashRPOS -ne $xr) -or ($nhashFlow -ne $xf))
	{
		while ($sumnum -lt 5)
		{
			$sumnum += 1
			Write-Output "$sumnum is the current count of the amount of times script has tried redownloading max 5 times" | Out-File -Append -FilePath $Log
			#Setting download counter due to download count		
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value $sumnum -Force
			Write-Output "Starting re-download of RPOS and FlowModule for the $sumnum time" | Out-File -Append -FilePath $Log
			Set-RPOSModuledownloadRPOS
			#Running MD5 checksum to ensure files are NOT! corrupt
			$nhashRPOS = Get-FileHash -Path $RPOSfile -Algorithm MD5 | select -ExpandProperty hash
			$nhashFlow = Get-FileHash -Path $flowfile -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$nhashRPOS is the MD5 hash of RPOS of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$nhashFlow is the MD5 hash of FlowModule of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$xr is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
			Write-Output "$xf is the MD5 has of FlowModule that server has" | Out-File -Append -FilePath $Log
			if (($xr -eq $nhashRPOS) -and ($xf -eq $nhashFlow)) { Break }
		}
	}
	
	if (($sumnum -ne "5") -and ($xr -eq $nhashRPOS) -and ($xf -eq $nhashFlow))
	{
		Write-Output "RPOS and FlowModule are matching the correct hash continuing" | Out-File -Append -FilePath $Log
		Write-Output "$nhashRPOS is the MD5 hash of RPOS of what was downloaded" | Out-File -Append -FilePath $Log
		Write-Output "$nhashFlow is the MD5 hash of FlowModule of what was downloaded" | Out-File -Append -FilePath $Log
		Write-Output "$xr is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
		Write-Output "$xf is the MD5 has of FlowModule that server has" | Out-File -Append -FilePath $Log
		
		#Coping file over to subnet file share
		Remove-Item $ShareFlow -Force
		Remove-Item $ShareRPOS -Force
		Write-Output "Starting to copy over for subnet sharing" | Out-File -Append -FilePath $Log
		Copy-Item $RPOSfile -Destination $ShareRPOS -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSfile has been copied for sharing on $ShareRPOS" | Out-File -Append -FilePath $Log
		Copy-Item $flowfile -Destination $ShareFlow -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "$flowfile has been copied for sharing on $ShareFlow" | Out-File -Append -FilePath $Log
		
		#Killing RPOS and Flow if running
		Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		
		#Uninstalling RPOS
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sur = $p.ExitCode
		Write-Output "Arh silent RPOS uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		#Uninstalling Flow
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:suf = $p.ExitCode
		Write-Output "Arh silent FlowModule uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		if ($rfile -eq "RPOS.air")
		{
			#Installing RPOS AIR
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfile" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		if ($rfile -eq "RPOS.exe")
		{
			#Installing RPOS EXE
			$p = Start-Process "$RPOSAUDIRHOME\RPOS.exe" -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		Start-Sleep -s 15
		
		if ($ffile -eq "FlowModule.air")
		{
			#Installing FlowModule AIR
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfile" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		if ($ffile -eq "FlowModule.exe")
		{
			#Installing FlowModule EXE
			$p = Start-Process "$RPOSAUDIRHOME\FlowModule.exe" -ArgumentList "-silent" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		
		#Logging amount of times download failed and file hashes to LANDesk Inventory scan and version of RPOS running on PC
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value $sumnum -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Checksum" -Value $nhashRPOS -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "FlowModule Checksum" -Value $nhashFlow -Force
		
		#Setting RPOS Version Labels in Registry
		Set-RPOSModuleFindVersion
		
		#Removing IP address text files and DNS names of PC's
		Remove-Item $LiveIP -Force
		Remove-Item $LiveNames -Force
	}
	Else
	{
		Write-Output "RPOS was not successful on downloading files correctly - no install or action will be taken" | Out-File -Append -Filepath $Log
	}
}

##########################################################################################################################################
#Fuction Install POS Without backing up previous build - Default Installer Process for RPOS
function Set-RPOSModuleInstallPOSnobackup
{
	
	#Adding updated date of RPOS update in registry for LANDesk Inventory
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Value $datereg -Force
	
	#Downloading Adobe Arh file if needed
	if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
	Else
	{
		$arh = "$server/arh.exe"
		Invoke-WebRequest -Uri $arh -OutFile $arhfile
		Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
	}
	
	#Killing RPOS and Flow if running
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	
	#Uninstalling RPOS
	$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
	$p.HasExited
	$script:sur = $p.ExitCode
	Write-Output "Arh silent RPOS uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
	
	Start-Sleep -s 15
	
	#Uninstalling Flow
	$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
	$p.HasExited
	$script:suf = $p.ExitCode
	Write-Output "Arh silent FlowModule uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
	
	Start-Sleep -s 15
	
	if ($rfile -eq "RPOS.air")
	{
		#Installing RPOS AIR
		$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfile" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sir = $p.ExitCode
		Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
	}
	
	if ($rfile -eq "RPOS.exe")
	{
		#Installing RPOS EXE
		$p = Start-Process "$RPOSAUDIRHOME\RPOS.exe" -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sir = $p.ExitCode
		Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
	}
	
	Start-Sleep -s 15
	
	if ($ffile -eq "FlowModule.air")
	{
		#Installing FlowModule AIR
		$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfile" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sif = $p.ExitCode
		Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
	}
	
	if ($ffile -eq "FlowModule.exe")
	{
		#Installing Flow
		$p = Start-Process "$RPOSAUDIRHOME\FlowModule.exe" -ArgumentList "-silent" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sif = $p.ExitCode
		Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
	}
	
	#Grabbing Values of versions for registry
	Set-RPOSModuleFindVersion

}

##########################################################################################################################################
#Fuction Remove RPOS Manually - This will remove all Registry Keys and files/folders if installer will not work
function Set-RPOSModuleManualRemove
{
	$RegRPOS64 = @("HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegRPOS64App = Get-ItemProperty $RegRPOS64 -EA 0
	$WantedRPOS64 = $RegRPOS64App | Where { $_.DisplayName -like "*RPOS*" }
	$WantedRPOS64 = $WantedRPOS64.PSChildName
	
	$RegRPOS32 = @("HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegRPOS32App = Get-ItemProperty $RegRPOS32 -EA 0
	$WantedRPOS32 = $RegRPOS32App | Where { $_.DisplayName -like "*RPOS*" }
	$wantedRPOS32 = $WantedRPOS32.PSChildName
	
	$RegRPOSInstall = @("HKLM:\Software\Classes\Installer\Products\*")
	$RegRPOSInstallApp = Get-ItemProperty $RegRPOSInstall -EA 0
	$WantedRPOSInstall = $RegRPOSInstallApp | Where { $_.ProductName -like "*RPOS*" }
	$WantedRPOSInstall = $WantedRPOSInstall.PSChildName
	
	$RegFLOW64 = @("HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegFLOW64App = Get-ItemProperty $RegFLOW64 -EA 0
	$WantedFlow64 = $RegFLOW64App | Where { $_.DisplayName -like "*FlowModule*" }
	$WantedFlow64 = $WamtedFlow64.PSChildName
	
	$RegFLOW32 = @("HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegFLOW32App = Get-ItemProperty $RegFLOW32 -EA 0
	$WantedFlow32 = $RegFLOW32App | Where { $_.DisplayName -like "*FlowModule*" }
	$wantedFlow32 = $WantedFlow32.PSChildName
	
	foreach ($key in $WantedRPOS64) {
		if ($key -eq "RPOSAutoUpdater") {
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log }
		Else {
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log }
		}
	
	foreach ($key in $WantedRPOS32) {
		if ($key -eq "RPOSAutoUpdater")
		{
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else
		{
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}	
	}
	
	foreach ($key in $WantedRPOSInstall) {
		
		Remove-Item "HKLM:\Software\classes\Installer\Products\$key" -Recurse -ErrorAction SilentlyContinue -Force
		Write-Output "Registry Key HKLM:\Software\classes\Installer\Products\$key was deleted" | Out-File -Append -FilePath $Log
	}
	
	foreach ($key in $WantedFlow64) {
		if ($key -eq "RPOSAutoUpdater")
		{
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else
		{
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\classes\Installer\Products\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	foreach ($key in $WantedFlow32) {
		if ($key -eq "RPOSAutoUpdater")
		{
			Write-Output "Found HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else
		{
			Remove-Item "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	Remove-Item -Path "$RPOSHOME" -Recurse -Force
	Write-OutPut "Folder C:\Program Files (x86\RPOS was deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "$FLOWHOME" -Recurse -Force
	Write-OutPut "Folder C:\Program Files (x86\FlowModule was deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "C:\Users\Public\Desktop\RPOS.lnk" -Force
	Write-Output "C:\Users\Public\Desktop\RPOS.lnk has been deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "C:\Users\Public\Desktop\FlowModule.lnk" -Force
	Write-Output "C:\Users\Public\Desktop\FlowModule.lnk has been deleted" | Out-File -Append -FilePath $Log
}

##########################################################################################################################################
#Fuction check install - Checking to make sure no bad installs are there
function Set-RPOSModulecheckinstall
{
	Write-Output "Checking to see if Install was successful" | Out-File -Append -FilePath $Log
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	#If RPOS or FlowModule are blank reinstall them if not blank do nothing
	If (($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($verRPOS -ne $x) -or ($verFLOW -ne $x))
	{
		Write-Output "Found Install was not successful" | Out-File -Append -FilePath $Log
		Write-Output "After install RPOS and Flow Status" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is what the check pulled for RPOS" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is what the check pulled for FlowModule" | Out-File -Append -FilePath $Log
		Write-Output "Running installer again for Flow and RPOS" | Out-File -Append -FilePath $Log
		Set-RPOSModuleInstallPOS
	}
	Else
	{
		Write-Output "RPOS and FlowModule Installed checkinstall is complete and passed" | Out-File -Append -FilePath $Log
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "1" -Force
	}
}

##########################################################################################################################################
#Fuction download RPOS - actual download process for RPOS and FlowModule
function Set-RPOSModuledownloadRPOS
{
	
	#Removing Previous build files
	Remove-Item $flowfile -Force
	Remove-Item $RPOSfile -Force
	
	#Removing IP scan and DNS names text files	
	Remove-Item $LiveIP -Force
	Remove-Item $LiveNames -Force
	
	#Downloading RPOS and Flow Files
	$RPOS = "$server/$rfile"
	Invoke-WebRequest -Uri $RPOS -OutFile $RPOSfile
	Write-Output "$rfile has been downloaded" | Out-File -Append -FilePath $Log
	$Flow = "$server/$ffile"
	Invoke-WebRequest -Uri $Flow -OutFile $flowfile
	Write-Output "$ffile has been downloaded" | Out-File -Append -FilePath $Log
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Value "DistroServer" -Force
}

##########################################################################################################################################
#Fuction switchbuild - This controls the switching of RPOS builds
function Set-RPOSModuleswitchbuild
{
	#Checking to see if there is a switch in the builds
	$URL = $XMLSwitchURL
	
	#Getting switch build response
	$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
	$w = $xmlread.serverswitch.$switchvalue.name
	$x = $xmlread.serverswitch.$switchvalue.statement
	$y = $xmlread.serverswitch.$switchvalue.server
	Write-Output "Switch build revision equals $x" | Out-File -Append -FilePath $Log
	
	#If switch build equals true start downloading distrubtion package on specified server if false continue with version check and install on same server.
	if (($x -eq "true") -and ($rposname -ne $w))
	{
		Write-Output "Switching RPOS Distros was detected switching to $w on $y on $switchvalue" | Out-File -Append -FilePath $Log
		#Setting switch server from XML file
		$server = "http://$y"
		
		#Downloading hash checksum for swutch value
		$URL = "$server/checksum.xml"
		Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash
		
		#Getting HD5 hash numbers
		[xml]$xmlread = Get-Content $XMLhash
		$xr = $xmlread.checksum.RPOS
		[xml]$xmlread = Get-Content $XMLhash
		$xf = $xmlread.checksum.Flow
		Write-Output "$xr is the checksum of RPOS version on $server" | Out-File -Append -FilePath $Log
		Write-Output "$xf is the checksum of FlowModule version on $server" | Out-File -Append -FilePath $Log
		
		#Downloading update XML for switch build
		$URL = "$server/update.xml"
		Invoke-WebRequest -Uri $URL -OutFile $XML
		[xml]$xmlread = Get-Content $xml
		$script:x = $xmlread.update.versionNumber
		Write-Output "$x is the version pulled on $server" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is the version of RPOS pulled on PC" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is the version of Flow pulled on PC" | Out-File -Append -FilePath $Log
		
		#Finding out if RPOS and FlowModule are AIR or EXE
		Invoke-Expression "Invoke-webrequest $server/RPOS.exe -DisableKeepAlive -UseBasicParsing -Method head" -ErrorVariable resultexeair
		$resultexeair | Out-File -FilePath $results
		
		#If statement for EXE or AIR files	
		If ((Get-Content $results) -eq $null)
		{
			$rfile = "RPOS.exe"
			$ffile = "FlowModule.exe"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		If ((Get-Content $results) -ne $null)
		{
			$rfile = "RPOS.air"
			$ffile = "FlowModule.air"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		
		#Creating values for the default RPOS folder and Default share folder	
		$RPOSfile = "$RPOSAUDIRHOME\$rfile"
		$flowfile = "$RPOSAUDIRHOME\$ffile"
		$ShareRPOS = "$RPOSAUDIRBUILD\$rfile"
		$ShareFlow = "$RPOSAUDIRBUILD\$ffile"
		
		#Starting to install RPOS
		Set-RPOSModuleInstallPOS
		if (($sir -gt "0") -or ($sif -gt "0"))
		{
			Write-Output "Where was an error detected with the automatic installer - switching to manual mode and reinstalling" | Out-File -Append -FilePath $Log
			Set-RPOSModuleManualRemove
			Set-RPOSModuleInstallPOSnobackup
			Set-RPOSModulecheckinstall
			#Setting Checksum registry Key if passed
			{ Break }
		}
		Else
		{
			Write-Output "Version passed validation of successful install" | Out-File -Append -FilePath $Log
			Set-RPOSModulecheckinstall
			#Setting Checksum registry Key if passed
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value $checkswitchxml -Force
			{ Break }
		}
	}
	Else
	{
		Write-Output "There was a newer version of switchxml, however the site was not matching what was changed" | Out-File -Append -FilePath $Log
		#Setting Checksum registry Key if passed
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value $checkswitchxml -Force
		$script:RegSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	}
	
	#CheckRPOSIcon Checking Desktop Icon
	Set-RPOSModuleCheckRPOSIcon
}

#########################################################################################################################################
#                                                         * END OF MODULES *                                                            # 
#########################################################################################################################################


###############################################################################################################
# Function RPOS-UpdatePOS
function Get-RPOSUpdateRPOS
{
#Setting up varibles from above
Set-RPOSVar
	
##########################################################################################################################################
#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d" | Out-File -Append -FilePath $Log
	Write-Output "RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
	
	Write-Output "Checking internet connection, please wait......" | Out-File -Append -FilePath $Log
	
	If ($networkcheck -eq $null) {
		Write-Output "There is currnetly no internet connection RPOS Auto-Updater will check again for an update another time" | Out-File -Append -FilePath $Log
		
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N /W=$InventoryRunTime
		Write-Output "Waiting $InventoryRunTime until starting the LANDesk Inventory Scan" | Out-File -Append -FilePath $Log
		
		#Getting time of end of script for log file
		$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
		Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log }
	Else {
		
		Write-Output "There is currently an active and running internet connection on this PC proceeding with setup" | Out-File -Append -FilePath $Log
		
		if ($dateofdelayhour -ge "00" -and $dateofdelayhour -le "06") {
			#Starting Random Delay Up To One Hour
			Write-Output "Starting delay of $StartTimeDelay seconds before running RPOS Auto-Updater due to after hours" | Out-File -Append -FilePath $Log
			Start-Sleep -Seconds $StartTimeDelay }
		
		Write-Output "Starting Auto-Updater during normal business hours without the delay" | Out-File -Append -FilePath $Log
		
		#Grabbing three digit IP address for RPOS-Preferences XML for later use
		Set-RPOSModuleThreeDigitIPfromXML
		
		#Starting to check for Adobe AIR
		Write-Output "Starting to check for Adobe AIR Install" | Out-File -Append -FilePath $Log
		Set-RPOSModuleInstallAIR
		
		#setting up registry search
		New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
		
		#Grabbing version from RPOS and FLOW Registry
		$script:rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
		$script:verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$script:verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		
		#Grabbing files names in RPOS directory
		$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
		$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
		
		#Checking to see what PC name is assoicated with a Store, TAC, Carroll Tire, Or Corprate Owned
		Set-RPOSModuleWhatPCAmI
		
		#Checking XML update files checksums
		$URL = $checkupdatedXMLs
		$script:xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
		$checkswitchxml = $xmlread.filechecksum.switchxml.$XMLswitchchecksum
		$checkBGInfoxml = $xmlread.filechecksum.BGInfoxml.$XMLbginfochecksum
		
		
		#Preventing install of BGInfo and EMV on Carroll Tire, TAC, Personal PC - Checking to see if EMV is installed and BGinfo is up to date.
		if ($name -match "S[0-9][0-9][0-9]")
		{
			
			Write-Output "Checking for EMV install and current BGInfo" | Out-File -Append -FilePath $Log
			Set-RPOSModuleCheckBGInfoUpdateandEMV
		}
		
		Write-Output "$switchvalue is the switch value." | Out-File -Append -FilePath $Log
		Write-Output "$rposname is the version of RPOS running on this PC" | Out-File -Append -FilePath $Log
		
		#checking to see if switch file has changed and if it has to switch the RPOS version
		if ($checkswitchxml -ne $RegSwitchXMLchecksum)
		{
			
			Write-Output "There is a different switchxml on RPOS.tbccorp.com checking xml and seeing if there is a new distro change" | Out-File -Append -FilePath $Log
			Set-RPOSModuleswitchbuild
		}
		
		if ($checkswitchxml -eq $RegSwitchXMLchecksum)
		{
			Write-Output "There is no new switch.xml on RPOS.tbccorp.com, bypassing switching distro to checking for newer version of RPOS without switching." | Out-File -Append -FilePath $Log
			Write-Output "Powershell has found no distro change needed. Version upgrade check starting" | Out-File -Append -FilePath $Log
			
			#Pulling update xml file from server to compare local and server update.xml
			if ($rposname -eq "RPOS - PRODUCTION") {
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				Invoke-WebRequest -Uri "$URL" -OutFile $XML }
			if ($rposname -eq "RPOS - TRAINING") {
				$URL = "http://$train/update.xml"
				$server = "http://$train"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - PILOT") {
				$URL = "http://$pilot/update.xml"
				$server = "http://$pilot"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - PILOT2") {
				$URL = "http://$pilot2/update.xml"
				$server = "http://$pilot2"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - PILOT3") {
				$URL = "http://$pilot3/update.xml"
				$server = "http://$pilot3"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QA") {
				$URL = "http://$qa/update.xml"
				$server = "http://$qa"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq $null) {
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QAPILOT") {
				$URL = "http://$qapilot/update.xml"
				$server = "http://$qapilot"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QAPILOT2") {
				$URL = "http://$qapilot2/update.xml"
				$server = "http://$qapilot2"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QAPILOT3") {
				$URL = "http://$qapilot3/update.xml"
				$server = "http://$qapilot3"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - ALPHA") {
				$URL = "http://$alpha/update.xml"
				$server = "http://$alpha"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEV") {
				$URL = "http://$dev/update.xml"
				$server = "http://$dev"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEVPILOT") {
				$URL = "http://$devpilot/update.xml"
				$server = "http://$devpilot"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEVPILOT2") {
				$URL = "http://$devpilot2/update.xml"
				$server = "http://$devpilot2"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEVPILOT3") {
				$URL = "http://$devpilot3/update.xml"
				$server = "http://$devpilot3"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			
			#Getting version number 
			[xml]$xmlread = Get-Content $xml
			$x = $xmlread.update.versionNumber
			Write-Output "$x is the version pulled on $server" | Out-File -Append -FilePath $Log
			Write-Output "$verRPOS is the version of RPOS pulled on PC" | Out-File -Append -FilePath $Log
			Write-Output "$verFLOW is the version of Flow pulled on PC" | Out-File -Append -FilePath $Log
			
			#Finding out if RPOS and FlowModule are AIR or EXE
			Invoke-Expression "Invoke-webrequest $server/RPOS.exe -DisableKeepAlive -UseBasicParsing -Method head" -ErrorVariable resultexeair
			$resultexeair | Out-File -FilePath $results
			
			#If statement for EXE or AIR files	
			If ((Get-Content $results) -eq $null) {
				$rfile = "RPOS.exe"
				$ffile = "FlowModule.exe"
				Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log }
			If ((Get-Content $results) -ne $null) {
				$rfile = "RPOS.air"
				$ffile = "FlowModule.air"
				Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log }
			
			#Creating values for the default RPOS folder and Default share folder	
			$RPOSfile = "$RPOSAUDIRHOME\$rfile"
			$flowfile = "$RPOSAUDIRHOME\$ffile"
			$ShareRPOS = "$RPOSAUDIRBUILD\$rfile"
			$ShareFlow = "$RPOSAUDIRBUILD\$ffile"
			
			#Pulling checksum xml file from server to compare the downloaded checksum to ensure download was downloaded correctly
			if ($rposname -eq "RPOS - PRODUCTION") {
				$URL = "http://$prod/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - TRAINING") {
				$URL = "http://$train/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - PILOT") {
				$URL = "http://$pilot/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - PILOT2") {
				$URL = "http://$pilot2/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - PILOT3") {
				$URL = "http://$pilot3/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QA") {
				$URL = "http://$qa/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq $null) {
				$URL = "http://$prod/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QAPILOT") {
				$URL = "http://$qapilot/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QAPILOT2") {
				$URL = "http://$qapilot2/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QAPILOT3") {
				$URL = "http://$qapilot3/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - ALPHA") {
				$URL = "http://$alpha/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEV") {
				$URL = "http://$dev/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEVPILOT") {
				$URL = "http://$devpilot/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEVPILOT2") {
				$URL = "http://$devpilot2/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEVPILOT3") {
				$URL = "http://$devpilot3/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			
			#Getting HD5 hash numbers
			[xml]$xmlread = Get-Content $XMLhash
			$xr = $xmlread.checksum.RPOS
			[xml]$xmlread = Get-Content $XMLhash
			$xf = $xmlread.checksum.Flow
			Write-Output "$xr is the checksum of RPOS version on $server" | Out-File -Append -FilePath $Log
			Write-Output "$xf is the checksum of FlowModule version on $server" | Out-File -Append -FilePath $Log
			
			#Downloading Adobe Arh file if needed
			if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
			Else
			{
				$arh = "$server/arh.exe"
				Invoke-WebRequest -Uri $arh -OutFile $arhfile
				Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
			}
			
			#Checking if RPOS and Flow Equal or Not Equal to server
			if (($x -eq $verRPOS) -and ($x -eq $verFLOW))
			{
				Write-Output "Nothing to download versions of RPOS and flow are current" | Out-File -Append -FilePath $Log
				Set-RPOSModulecheckinstall
				Set-RPOSModuleFindVersion
			}
			Else
			{
				#Starting RPOS and Flow Module Install
				Write-Output "RPOS and Flow are different versions - Starting New Install" | Out-File -Append -FilePath $Log
				Set-RPOSModuleInstallPOS
				
				$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
				$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
				
				if (($sir -gt "0") -or ($sif -gt "0") -or ($verRPOS -eq $null) -or ($verFLOW -eq $null))
				{
					Write-Output "Where was an error detected with the automatic installer - switching to manual mode and reinstalling" | Out-File -Append -FilePath $Log
					Set-RPOSModuleManualRemove
					Set-RPOSModuleInstallPOSnobackup
					Set-RPOSModulecheckinstall
				}
				Else
				{
					Write-Output "Version passed validation of successful install" | Out-File -Append -FilePath $Log
					Set-RPOSModulecheckinstall
					Set-RPOSModuleFindVersion
				}
			}
		}
		
		#Checking Install One last time
		$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		$testpospath = (Test-Path "$RPOSHOME\RPOS.exe")
		$testflowpath = (Test-Path "$FLOWHOME\FlowModule.exe")
		
		if (($verRPOS -ne $x) -or ($verFLOW -ne $x) -or ($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($testflowpath -eq $false) -or ($testpospath -eq $false))
		{
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "0" -Force
		}
		Else
		{
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "1" -Force
		}
		
		#Checking Reporting XML
		Set-RPOSModuleCheckReportXML
		
		#CheckRPOSIcon Checking Desktop Icon
		Set-RPOSModuleCheckRPOSIcon
		
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N /W=$InventoryRunTime
		#Write-Output "Waiting $InventoryRunTime until starting the LANDesk Inventory Scan" | Out-File -Append -FilePath $Log
		
		#Getting time of end of script for log file
		$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
		Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log
		
	}
	#Starting LANDesk Inventory Scan
	#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N /W=$InventoryRunTime
	
	#Removing IP address text files and DNS names of PC's
	Remove-Item $LiveIP -Force
	Remove-Item $LiveNames -Force
	#cleaning house
	Remove-PSDrive -Name HKLM -Force
	Remove-Item $XMLhash -Force
	Remove-Item $results -Force
}

#######################################################################################################
#Function RPOS-RemoveAULogs
function Get-RPOSRemoveAULogs {
	$Path = "$RPOSAUDIRLOGS"
	$Daysback = "-60"
	
	$CurrentDate = Get-Date
	$DatetoDelete = $CurrentDate.AddDays($Daysback)
	Get-ChildItem $Path | Where-Object { $_.LastWriteTime -lt $DatetoDelete } | Remove-Item
}

#########################################################################################################
#Function RPOS-Rollback
function Get-RPOSRollback
{
	#Setting up varibles from above
	Set-RPOSVar
	
	#ALL VARIABLES THAT ARE IN THIS POWERSHELL SCRIPT CAN BE CHANGED FROM THE FIRST COUPLE ROWS TO REFLECT THE ENTIRE SCRIPT!#
	#Setting Up Varibles
	$date = (get-date).ToString("yyyyMMdd-hhmmss")
	$Log = "$RPOSAUDIRLOGS\RPOS_Rollback_$date.txt"
	$builddir = "$RPOSAUDIRBUILD"
	
	#Fuction Install POS - Default Installer Process for RPOS
	$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
	$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
	$script:drfilebak = Get-ChildItem -path "$RPOSAUDIRBAK" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
	$script:dffilebak = Get-ChildItem -path "$RPOSAUDIRBAK" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
	
	function InstallPOS
	{
		#Removing old RPOS and FlowModule files
		if (($drfile -eq $null) -and ($dffile -eq $null))
		{
			Write-Output "There are no files found in the root directory of $RPOSAUDIRHOME not deleting anything" | Out-File -Append -FilePath $Log
		}
		if (($drfile -eq "RPOS.exe") -or ($dffile -eq "FlowModule.exe") -or ($drfile -eq "RPOS.air") -or ($dffile -eq "FlowModule.air"))
		{
			Write-Output "Deleting RPOS and FlowModule from $RPOSAUDIRHOME" | Out-File -Append -FilePath $Log
			Remove-Item "$RPOSAUDIRHOME\$drfile" -Force
			Remove-Item "$RPOSAUDIRHOME\$dffile" -Force
			Remove-Item "$RPOSAUDIRBUILD\*" -Recurse -Force
		}
		
		#Copying old RPOS Build to install directory and back to the Build directory
		if ($drfilebak -eq "RPOS.bakair")
		{
			Copy-Item -Path $RPOSfileBakair -Destination $RPOSfileair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "RPOS.bakair has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $flowfileBakair -Destination $flowfileair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "FlowModule.bakair has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $RPOSfileair -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
			Copy-Item -Path $flowfileair -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
		}
		
		if ($drfilebak -eq "RPOS.bakexe")
		{
			Copy-Item -Path $RPOSfileBakexe -Destination $RPOSfileexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "RPOS.bakexe has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $flowfileBakexe -Destination $flowfileexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "FlowModule.bakexe has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $RPOSfileexe -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
			Copy-Item -Path $flowfileexe -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
		}
		
		#Downloading Adobe Arh file if needed
		if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
		Else
		{
			$arh = "http://tposdist01.tbccorp.com/arh.exe"
			Invoke-WebRequest -Uri $arh -OutFile $arhfile
			Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
		}
		
		#Killing RPOS and Flow if running
		Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		
		#Uninstalling RPOS
		Write-Output "Starting Uninstall of RPOS" | Out-File -Append -FilePath $Log
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
		$p.HasExited
		$sur = $p.ExitCode
		Write-Output "RPOS silent uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		#Uninstalling Flow
		Write-Output "Starting Uninstall of FlowModule" | Out-File -Append -FilePath $Log
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
		$p.HasExited
		$suf = $p.ExitCode
		Write-Output "FlowModule silent uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		if ($drfilebak -eq "RPOS.bakair")
		{
			#Installing RPOS AIR
			Write-Output "Starting install of RPOS.air" | Out-File -Append -FilePath $Log
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfileair" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "RPOS.air silent install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		if ($drfilebak -eq "RPOS.bakexe")
		{
			#Installing RPOS EXE
			Write-Output "Starting install of RPOS.exe" | Out-File -Append -FilePath $Log
			$p = Start-Process $RPOSfileexe -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "RPOS.exe silent install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		Start-Sleep -s 15
		
		if ($dffilebak -eq "FlowModule.bakair")
		{
			#Installing Flow AIR
			Write-Output "Starting install of FlowModule.air" | Out-File -Append -FilePath $Log
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfileair" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "FlowModule.air silent install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		if ($dffilebak -eq "FlowModule.bakexe")
		{
			#Installing Flow EXE
			Write-Output "Starting install of FlowModule.exe" | Out-File -Append -FilePath $Log
			$p = Start-Process $flowfileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "FlowModule.exe silent install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /V /F
	}
	
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d" | Out-File -Append -FilePath $Log
	
	#setting up registry search
	Set-Location Registry::\HKEY_LOCAL_MACHINE
	New-PSDrive HKLM Registry HKEY_LOCAL_MACHINE
	Set-Location HKLM:
	
	#Grabbing version from RPOS and FLOW Registry
	$rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	#Starting RPOS and Flow Module Install
	Write-Output "Starting Roll Back UI Install" | Out-File -Append -FilePath $Log
	InstallPOS
	
	#Checking for successful install
	Write-Output "Checking to see if Install was successful" | Out-File -Append -FilePath $Log
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	If (($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($sif -ne "0") -or ($sir -ne "0"))
	{
		Write-Output "Found Install was not successful" | Out-File -Append -FilePath $Log
		Write-Output "After install RPOS and Flow Status" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is what the check pulled for RPOS" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is what the check pulled for FlowModule" | Out-File -Append -FilePath $Log
		Write-Output "Running installer again for Flow and RPOS" | Out-File -Append -FilePath $Log
		Remove-Item $flowfileair -ErrorAction SilentlyContinue -Force
		Remove-Item $flowfileexe -ErrorAction SilentlyContinue -Force
		Remove-Item $RPOSfileair -ErrorAction SilentlyContinue -Force
		Remove-Item $RPOSfileexe -ErrorAction SilentlyContinue -Force
		InstallPOS
	}
	Else
	{
		Write-Output "RPOS and FlowModule Installed correctly exiting script" | Out-File -Append -FilePath $Log
	}
	
	#Checking the version of RPOS on PC and importing them into the registry
	Set-RPOSModuleFindVersion
	
	#Checking Reporting XML
	Set-RPOSModuleCheckReportXML
	
	#Checking the RPOS Icons on the desktop
	Set-RPOSModuleCheckRPOSIcon
	
	#Getting time of end of script for log file
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log
	
	#cleaning house
	Remove-PSDrive -Name HKLM -Force
	cd C:
	
}

#########################################################################################################
#function RPOS-UpdateAU
function Get-RPOSUpdateAU
{
	#Setting up varibles from above
	Set-RPOSVar
	
	$pcname = $env:COMPUTERNAME
	$pcname = $pcname -replace 'S[0-9][0-9][0-9]'
	$date = (get-date).ToString("yyyyMMdd-hhmmss")
	$Log = "$RPOSAUDIRLOGS\UpdateCheck_$date.txt"
	$install = "$RPOSAUDIRUPDATE\setup.exe"
	$regpath = "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
	New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
	$ver = (Get-ItemProperty -Path $regpath -Name RPOSAUVer).RPOSAUVer
	$dateinstall = (Get-Date).ToString("MM/dd/yyyy")
	
	#Function download update
	function redownload
	{
		Write-Output "Downloading update for RPOS AU" | Out-File -Append -FilePath $Log
		Write-Output "Downloading File $setup from updater server" | Out-File -Append -FilePath $Log
		Invoke-WebRequest -Uri "$setup" -OutFile $install
	}
	
	#Getting Update.xml from TLDMS02 to check status
	Write-Output "AutoUpdater for RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
	Write-Output "Downloading Update.xml to check for new install or not" | Out-File -Append -FilePath $Log
	$xml = "$RPOSAUDIRUPDATE\updater.xml"
	$URL = "http://rpos.tbccorp.com/rposautoupdater/updatefiles/updater.xml"
	Invoke-WebRequest -Uri "$URL" -OutFile $xml
	
	#Getting update status TRUE or FALSE
	Write-Output "Starting to read xml files to see if there is an update" | Out-File -Append -FilePath $Log
	[xml]$xmlread = Get-Content $xml
	$x = $xmlread.update.statement
	$y = $xmlread.update.version
	$setup = $xmlread.update.setup
	$hash = $xmlread.update.hash
	
	Write-Output "$x is the statment of what was pulled from updater.exe" | Out-File -Append -FilePath $Log
	Write-Output "$y is the update version that it should be running" | Out-File -Append -FilePath $Log
	Write-Output "$ver is the update verison that is running on the PC" | Out-File -Append -FilePath $Log
	Write-Output "$hash is the hash that has to match the server for PCs to download" | Out-File -Append -FilePath $Log
	
	
	if (($x -eq "true") -and ($y -ne $ver))
	{
		Write-Output "Updater has found that a new version needs to be updated starting download" | Out-File -Append -FilePath $Log
		#Starting download and hash checks for PC1
		
		Write-Output "Starting to download newer copy of RPOS Auto" | Out-File -Append -FilePath $Log
		redownload
		$pchash = Get-FileHash -Path $install -Algorithm MD5 | select -ExpandProperty hash
		Write-Output "$pchash is the file hash that was just downloaded from update server" | Out-File -Append -FilePath $Log
		if ($pchash -eq $hash)
		{
			Write-Output "PC hash equals download has download was successful!" | Out-File -Append -FilePath $Log
			Write-Output "Starting install from update file........." | Out-File -Append -FilePath $Log
			Start-Process $install
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value $dateinstall -Force
		}
		Else
		{
			Write-Output "Hash of the download file was not correct attempting to re-download" | Out-File -Append -FilePath $Log
			Start-Sleep -Seconds 60
			redownload
			$pchash = Get-FileHash -Path $install -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$pchash is the file hash that was just re-downloaded from update server" | Out-File -Append -FilePath $Log
			if ($pchash -eq $hash)
			{
				Write-Output "PC hash equals download has download was successful!" | Out-File -Append -FilePath $Log
				Write-Output "Starting install from update file........." | Out-File -Append -FilePath $Log
				Start-Process $install
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value $dateinstall -Force
			}
			Else
			{
				Write-Output "Redownload was not successful twice, updater will wait until tomorrow" | Out-File -Append -FilePath $Log
			}
		}
	}
	Else
	{
		Write-Output "Updater has not found an update at this time exiting updater" | Out-File -Append -FilePath $Log
	}
	
	#Checking RPOS AutoHeal to make sure that the install is broken
	Get-RPOSAutoHeal
	
	Remove-PSDrive -Name HKLM -Force

}

################################################################################################################
#function RPOS-DesktopUpdate
function Get-RPOSDesktopUpdate
{
	#Setting up varibles from above
	Set-RPOSVar
	
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
	
	##########################################################################################################################################
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d"
	Write-Output "RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
	
	#Finding out if task is enabled or disabled in scheduled task
	$showtaska = schtasks /Query /TN "Update RPOS Local A" | Select-String -Pattern "Disabled"
	$showtaskb = schtasks /Query /TN "Update RPOS Local B" | Select-String -Pattern "Disabled"
	
	if ($showtaska -eq $null) {
	   $task = "A"}
	
	if ($showtaskb -eq $null) {
		$task = "B" }
	
	#setting up registry search
	New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
	
	#Checking to see if the desktop installer is running already to not create a duplicate install
	$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
	
	if ($processcheck -ne $null)
	{
		[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
	}
	if ($processcheck -eq $null)
	{
		#Grabbing version from RPOS and FLOW Registry
		$script:rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
		$script:verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$script:verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		
		#Grabbing files names in RPOS directory
		$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
		$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
		
		#Checking to see what PC name is assoicated with a Store, TAC, Carroll Tire, Or Corprate Owned
		Set-RPOSModuleWhatPCAmI
		
		#Checking XML update files checksums
		$URL = $checkupdatedXMLs
		$script:xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
		$checkswitchxml = $xmlread.filechecksum.switchxml.$XMLswitchchecksum
		$checkBGInfoxml = $xmlread.filechecksum.BGInfoxml.$XMLbginfochecksum
		
		
		Write-Output "$switchvalue is the switch value."
		Write-Output "$rposname is the version of RPOS running on this PC"
		
		#checking to see if switch file has changed and if it has to switch the RPOS version
		if ($checkswitchxml -ne $RegSwitchXMLchecksum)
		{
			
			Write-Output "There is a different switchxml on RPOS.tbccorp.com checking xml and seeing if there is a new distro change"
			
			#Checking to see if there is a switch in the builds
			$URL = $XMLSwitchURL
			
			#Getting switch build response
			$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			$w = $xmlread.serverswitch.$switchvalue.name
			$x = $xmlread.serverswitch.$switchvalue.statement
			$y = $xmlread.serverswitch.$switchvalue.server
			Write-Output "Switch build revision equals $x" | Out-File -Append -FilePath $Log
			
			if ($w -ne $rposname)
			{
				[System.Windows.Forms.MessageBox]::Show("WARNING!!!`n `nRPOS needs to be updated to $w. `n `nComplete all work before starting the update process. `n `nRPOS will be closed and unavailable during this update. `n `nAllow 3-7 minutes for the process to complete. `nA message will be displayed when the update is completed. `nClick OK to begin the update process.", "Update RPOS")
			}
			
			$usersprofile1 = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
			foreach ($userprofile1 in $usersprofile1)
			{
				Remove-Item "C:\Users\$userprofile1\Desktop\RPOS.lnk"
				Remove-Item "C:\Users\$userprofile1\Desktop\FlowModule.lnk"
				
			}
			
			schtasks /Run /TN "Update RPOS Local $task"
			Start-Sleep -Seconds 120
			$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			#$process = Get-Process updaterpos | Select-Object CPU, Name, Starttime
			while ($true)
			{
				if ($process -eq $null)
				{
					$installsuccess1 = (Get-ItemProperty -Path $LANDeskKeys -Name InstallComplete).InstallComplete
					
					if ($installsuccess1 -eq "1")
					{
						[System.Windows.Forms.MessageBox]::Show("RPOS was successfully updated to the latest version. ", "Update RPOS")
						Break
					}
					if ($installsuccess1 -eq "0")
					{
						[System.Windows.Forms.MessageBox]::Show("RPOS was NOT successfully updated to the latest version. Please try again in a few minutes.", "Update RPOS")
						Break
					}
				}
				Else
				{
					#Starting sleepto check the scheduled task again and if RPOS and/or FlowModule are running to close it.
					Start-Sleep -Seconds 30
					$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
				}
			}
		}
		
		if ($checkswitchxml -eq $RegSwitchXMLchecksum)
		{
			Write-Output "There is no new switch.xml on RPOS.tbccorp.com, bypassing switching distro to checking for newer version of RPOS without switching."
			Write-Output "Powershell has found no distro change needed. Version upgrade check starting"
			
			#Pulling update xml file from server to compare local and server update.xml
			if ($rposname -eq "RPOS - PRODUCTION")
			{
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - TRAINING")
			{
				$URL = "http://$train/update.xml"
				$server = "http://$train"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - PILOT")
			{
				$URL = "http://$pilot/update.xml"
				$server = "http://$pilot"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - PILOT2")
			{
				$URL = "http://$pilot2/update.xml"
				$server = "http://$pilot2"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - PILOT3")
			{
				$URL = "http://$pilot3/update.xml"
				$server = "http://$pilot3"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QA")
			{
				$URL = "http://$qa/update.xml"
				$server = "http://$qa"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq $null)
			{
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QAPILOT")
			{
				$URL = "http://$qapilot/update.xml"
				$server = "http://$qapilot"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QAPILOT2")
			{
				$URL = "http://$qapilot2/update.xml"
				$server = "http://$qapilot2"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QAPILOT3")
			{
				$URL = "http://$qapilot3/update.xml"
				$server = "http://$qapilot3"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - ALPHA")
			{
				$URL = "http://$alpha/update.xml"
				$server = "http://$alpha"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEV")
			{
				$URL = "http://$dev/update.xml"
				$server = "http://$dev"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEVPILOT")
			{
				$URL = "http://$devpilot/update.xml"
				$server = "http://$devpilot"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEVPILOT2")
			{
				$URL = "http://$devpilot2/update.xml"
				$server = "http://$devpilot2"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEVPILOT3")
			{
				$URL = "http://$devpilot3/update.xml"
				$server = "http://$devpilot3"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			
			#Getting version number 
			$x = $xmlread.update.versionNumber
			$y = $xmlread.update.versionLabel
			Write-Output "$x is the version pulled on $server"
			Write-Output "$verRPOS is the version of RPOS pulled on PC"
			Write-Output "$verFLOW is the version of Flow pulled on PC"
			
			
			
			#Checking if RPOS and Flow Equal or Not Equal to server
			if (($x -eq $verRPOS) -and ($x -eq $verFLOW))
			{
				[System.Windows.Forms.MessageBox]::Show("No new version available. ", "Update RPOS")
			}
			Else
			{
				
				[System.Windows.Forms.MessageBox]::Show("WARNING!!!`n `nRPOS needs to be updated to the latest version. $y `n `nComplete all work before starting the update process. `n `nRPOS will be closed and unavailable during this update. `n `nAllow 3-7 minutes for the process to complete. `nA message will be displayed when the update is completed. `nClick OK to begin the update process.", "Update RPOS")
				Stop-Process -force -passthru -processname rpos*
				Stop-Process -force -passthru -processname FlowModule*
				
				$usersprofile1 = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
				foreach ($userprofile1 in $usersprofile1)
				{
					Remove-Item "C:\Users\$userprofile1\Desktop\RPOS.lnk"
					Remove-Item "C:\Users\$userprofile1\Desktop\FlowModule.lnk"
					
				}

				schtasks /Run /TN "Update RPOS Local $task"
				Start-Sleep -Seconds 120
				$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
				
				#$process = Get-Process updaterpos | Select-Object CPU, Name, Starttime
				while ($true)
				{
					if ($process -eq $null)
					{
						$installsuccess1 = (Get-ItemProperty -Path $LANDeskKeys -Name InstallComplete).InstallComplete
						
						if ($installsuccess1 -eq "1")
						{
							[System.Windows.Forms.MessageBox]::Show("RPOS was successfully updated to the latest version. ", "Update RPOS")
							Break
						}
						if ($installsuccess1 -eq "0")
						{
							[System.Windows.Forms.MessageBox]::Show("RPOS was NOT successfully updated to the latest version. Please try again in a few minutes.", "Update RPOS")
							Break
						}
					}
					Else
					{
						#Starting sleepto check the scheduled task again and if RPOS and/or FlowModule are running to close it.
						Start-Sleep -Seconds 30
						$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
						
					}
					
				}
			}
		}
		
		
		#Removing IP address text files and DNS names of PC's
		Remove-Item $LiveIP -Force
		Remove-Item $LiveNames -Force
		#cleaning house
		Remove-PSDrive -Name HKLM -Force
		Remove-Item $XMLhash -Force
		Remove-Item $results -Force
	}
}