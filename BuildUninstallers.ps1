﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.141
	 Created on:   	12/29/2017 12:38 PM
	 Created by:   	Randy Olsen TBC EOC
	 Organization: 	TBC Corporation
	 Filename:     	BuildUninstallers.ps1
	===========================================================================
	.DESCRIPTION
		This script will build all the uninstaller files for the RPOS Auto-Updater
#>

Write-Output 'CreateObject("Wscript.Shell").Run """" & WScript.Arguments(0) & """", 0, False' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\quietuninstall.vbs"

Write-Output '@echo off' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\UninstallRPOSAU.bat"
Write-Output 'wscript.exe "C:\WINDOWS\Temp\quietuninstall.vbs" "C:\Windows\Scripts\RPOS\UninstallRPOSAU.exe"' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\UninstallRPOSAU.bat"
Write-Output 'ping 127.0.0.1 -n 30 > nul' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\UninstallRPOSAU.bat"
Write-Output 'rd /s /q "C:\Windows\Scripts\RPOS"' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\UninstallRPOSAU.bat"
Write-Output 'rd /s /q "C:\Windows\Scripts"' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\UninstallRPOSAU.bat"
Write-Output 'del /f "C:\Windows\Temp\quietuninstall.vbs"' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\UninstallRPOSAU.bat"
Write-Output '(goto) 2>nul & del "%~f0"' | Out-File -Append -Encoding ASCII -FilePath "C:\WINDOWS\Temp\UninstallRPOSAU.bat"

icacls "C:\WINDOWS\Temp\UninstallRPOSAU.bat" /grant Everyone:RX
icacls "C:\WINDOWS\Temp\quietuninstall.vbs" /grant Everyone:RX

C:\WINDOWS\System32\wscript.exe "C:\Windows\Temp\quietuninstall.vbs" "C:\Windows\Temp\UninstallRPOSAU.bat"
