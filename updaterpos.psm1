﻿###################################################
#       Auto-Update RPOS Powershell               #
#       Made By: Randy Olsen                      #
#       Date: 3/5/2020                            #
#       Version: 3.8.1d                           #
###################################################

#ALL VARIABLES THAT ARE IN THIS POWERSHELL SCRIPT CAN BE CHANGED FROM THE FIRST COUPLE ROWS TO REFLECT THE ENTIRE SCRIPT!#
#Setting Up Varibles

#########################################################################################################################################
#                                                         * START OF MODULES *                                                          # 
#########################################################################################################################################
#Function Set-RPOSVar
function Set-RPOSVar
{
	$script:name = $env:COMPUTERNAME
	$script:date = (get-date).ToString("yyyyMMdd-hhmmss")
	
	#Default DIR for Auto-Updater
	$script:RPOSAUDIRHOME = "$env:SystemRoot\Scripts\RPOS"
	$script:RPOSAUDIRBAK = "$env:SystemRoot\Scripts\RPOS\Bak"
	$script:RPOSAUDIRBUILD = "$env:SystemRoot\Scripts\RPOS\Build"
	$script:RPOSAUDIRLOGS = "$env:SystemRoot\Scripts\RPOS\Logs"
	$script:RPOSAUDIRUNINSTALL = "$env:SystemRoot\Scripts\RPOS\uninstall"
	$script:RPOSAUDIRUPDATE = "$env:SystemRoot\Scripts\RPOS\update"
	$script:RPOSAUDIRAUTOFIX = "$env:SystemRoot\Scripts\RPOS\Autofix"
	$script:RPOSAUAUTOFIX = "$env:SystemRoot\System32\rposauaf.exe"
	
	#RPOS Program Files Dirs
	$script:RPOSHOME = "${env:ProgramFiles(x86)}\RPOS"
	$script:FLOWHOME = "${env:ProgramFiles(x86)}\FlowModule"
	
	$script:autofixhttpdir = "http://rpos.tbccorp.com/RPOSAutoUpdater/AutoFix"
	$script:Log = "$RPOSAUDIRLOGS\RPOS_Update_$date.txt"
	$script:ReleaseInfo = "3.8.1d"
	$script:LANDeskKeys = "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
	$script:keyrpos = "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\com.tbccorp.rpos.RPOS"
	$script:keyflow = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\com.tbccorp.rpos.FlowModule"
	$script:nameprofile = $name.Substring(1, 3)
	$script:profilename = "store00$nameprofile"
	$script:XMLEMVfileRPOS = "C:\Users\$profilename\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
	$script:XMLEMVfileFlow = "C:\Users\$profilename\AppData\Roaming\com.tbccorp.rpos.FlowModule\Local Store\rpos-preferences.xml"
	$script:XMLEMVRPOSbak = "$RPOSAUDIRBAK\rpos-preferencesrpos.xml"
	$script:XMLEMVFLOWbak = "$RPOSAUDIRBAK\rpos-preferencesflow.xml"
	$script:XML = "$RPOSAUDIRUPDATE.xml"
	$script:XMLhash = "$RPOSAUDIRHOME\checksum.xml"
	$script:RPOSEXE = "$RPOSHOME\RPOS.exe"
	$script:RPOSDesktopIcon = "C:\Users\Public\Desktop\RPOS.lnk"
	$script:arhfile = "$RPOSAUDIRHOME\arh.exe"
	$script:RPOSfileBakair = "$RPOSAUDIRBAK\RPOS.bakair"
	$script:flowfileBakair = "$RPOSAUDIRBAK\FlowModule.bakair"
	$script:RPOSfileBakexe = "$RPOSAUDIRBAK\RPOS.bakexe"
	$script:flowfileBakexe = "$RPOSAUDIRBAK\FlowModule.bakexe"
	$script:flowfileair = "$RPOSAUDIRHOME\FlowModule.air"
	$script:RPOSfileair = "$RPOSAUDIRHOME\RPOS.air"
	$script:flowfileexe = "$RPOSAUDIRHOME\FlowModule.exe"
	$script:RPOSfileexe = "$RPOSAUDIRHOME\RPOS.exe"
	$script:RPOSVersionLabelXML = "$RPOSHOME\META-INF\AIR\application.xml"
	$script:LiveIP = "$RPOSAUDIRHOME\LiveIPs.txt"
	$script:LiveNames = "$RPOSAUDIRHOME\LiveNames.txt"
	$script:datereg = (Get-Date).ToString("MM/dd/yyyy")
	$script:dateofdelayhour = Get-Date -Format "HH"
	$script:adobesumnum = "0"
	$script:sumnum = "0"
	$script:sumnumbginfo = "0"
	$script:results = "$RPOSAUDIRHOME\results.txt"
	$script:AdobeAIRInstallRoot = "$RPOSAUDIRHOME\AdobeAIRInstaller.exe"
	$script:AdobeAIRInstallShare = "$RPOSAUDIRBUILD\AdobeAIRInstaller.exe"
	$script:AdobeInstallerFile = "AdobeAIRInstaller.exe"
	$script:BGInfoLocalFile = "C:\bginfo\TBC.bgi"
	$script:checkupdatedXMLs = "http://rpos.tbccorp.com/RPOSfileupdatechecksum.xml"
	$script:RegBGInfoXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoXMLchecksum).BGInfoXMLchecksum
	$script:RegSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	$script:ReportingMaserXMLhtml = "http://rpos.tbccorp.com/RPOSAutoUpdater/Reporting/MasterData/MasterCollection.xml"
	$script:ReportingMaserXMLlocal = "R:\MasterCollection.xml"
	$script:builddir = "$RPOSAUDIRBUILD"
	$script:flowfileair = "$RPOSAUDIRHOME\FlowModule.air"
	$script:RPOSfileair = "$RPOSAUDIRHOME\RPOS.air"
	$script:flowfileexe = "$RPOSAUDIRHOME\FlowModule.exe"
	$script:RPOSfileexe = "$RPOSAUDIRHOME\RPOS.exe"
	$script:RPOSfileBakair = "$RPOSAUDIRBAK\RPOS.bakair"
	$script:flowfileBakair = "$RPOSAUDIRBAK\FlowModule.bakair"
	$script:RPOSfileBakexe = "$RPOSAUDIRBAK\RPOS.bakexe"
	$script:flowfileBakexe = "$RPOSAUDIRBAK\FlowModule.bakexe"
	$script:teslafileexe = "$RPOSAUDIRHOME\Tesla.exe"
	$script:teslafileBakexe = "$RPOSAUDIRBAK\TeslaBak.exe"
	$script:TeslaInstallerFile = "tesla-electron-latest.exe"
	$script:TeslaShareFile = "$RPOSAUDIRBUILD\Tesla.exe"
	$script:TeslaPublicFold = "C:\Users\Public\AppData\Local\tesla-electron"
	$script:PublicAppData = "C:\Users\Public\AppData"
	$script:PublicAppDataLocalFolder = "C:\Users\Public\AppData\Local"
	$script:TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
	$script:TeslaProtonversion = "$RPOSAUDIRHOME\TESLA-PROTON.VERSION"
	$script:TeslaElectronversion = "$RPOSAUDIRHOME\TESLA-ELECTRON.VERSION"
	$script:TeslaElectronversionstandard = "C:\Users\Public\AppData\Local\TESLA-ELECTRON.VERSION"
	$script:TeslaProtonversionstandard = "C:\Users\Public\AppData\Local\TESLA-PROTON.VERSION"
	$script:CiscoStatus = get-wmiobject win32_networkadapter | select name, netconnectionstatus | Where-Object { $_.name -match "Cisco" } | Select-Object -ExpandProperty netconnectionstatus
	$script:CiscoExists = get-wmiobject win32_networkadapter | select name | Where-Object { $_.name -match "Cisco" } | Select-Object -ExpandProperty Name
	
	#Two Digit Store Number
	$TwoDigPCName = "$ENV:COMPUTERNAME"
	
	if ($TwoDigPCName -match 'S[0-9][0-9][0-9]00') {
		$script:TwoDigPCName = $TwoDigPCName.Substring(6, 2) }
	
	if ($TwoDigPCName -match 'S[0-9][0-9][0-9]NPC') {
		$script:TwoDigPCName = $TwoDigPCName.Substring(7, 2) }
	
	if (($name -match 'S[0-9][0-9][0-9]00') -or ($name -match 'S[0-9][0-9][0-9]NPC')) {
	
	#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for EMV
	[xml]$xmlread = Get-Content $XMLEMVfileRPOS
	$script:Result = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
	
	if ($Result -eq $null)
	{
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for NON-EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$Read = $xmlread.ApplicationPreferences.creditCardTerminalUrl | Select-Object -ExpandProperty "#cdata-section"
		$script:Result = $Read -replace ":9001"
	}
	
	
		#Adding Verifone XML IP Address
		$script:XMLipa = ([ipaddress] "$Result").GetAddressBytes()[0]
		$script:XMLipb = ([ipaddress] "$Result").GetAddressBytes()[1]
		$script:XMLipc = ([ipaddress] "$Result").GetAddressBytes()[2]
		$script:XMLipd = ([ipaddress] "$Result").GetAddressBytes()[3]
	}
	
	if (($CiscoExists -ne $null) -and ($CiscoStatus -eq "2"))
	{
	#Write-Output "Cisco AnyConnect is connected bypassing Set-Var Get-IP Address for Subnet Sharing" | Out-File -Append -FilePath $Log
	}
	Else
	{
		#Grabbing pinged IP address and gateway address
		$script:IP = Test-Connection $name -count 1 | select -ExpandProperty Ipv4Address | Select -ExpandProperty IPAddressToString
		$script:ipmain1 = ([ipaddress] "$IP").GetAddressBytes()[0]
		$script:ipmain2 = ([ipaddress] "$IP").GetAddressBytes()[1]
		$script:ipmain3 = ([ipaddress] "$IP").GetAddressBytes()[2]
		$script:ipmain4 = ([ipaddress] "$IP").GetAddressBytes()[3]
		$script:IPZero = "$ipmain1.$ipmain2.$ipmain3.0"
	}
	
	#Testing to see if there is internet
	$script:networkcheck = Test-Connection "rpos.tbccorp.com" -count 4 -Quiet
	
	#Servers List
	$script:prod = "rpos.tbccorp.com"
	$script:train = "rpos-training.tbccorp.com"
	$script:pilot = "rpos-pilot.tbccorp.com"
	$script:pilot2 = "rpos-pilot2.tbccorp.com"
	$script:pilot3 = "rpos-pilot3.tbccorp.com"
	$script:qa = "rpos-qa.tbccorp.com"
	$script:qapilot = "rpos-qapilot.tbccorp.com"
	$script:qapilot2 = "rpos-qapilot2.tbccorp.com"
	$script:qapilot3 = "rpos-qapilot3.tbccorp.com"
	$script:alpha = "rpos-alpha.tbccorp.com"
	$script:dev = "rpos-dev.tbccorp.com"
	$script:devpilot = "rpos-devpilot.tbccorp.com"
	$script:devpilot2 = "rpos-devpilot2.tbccorp.com"
	$script:devpilot3 = "rpos-devpilot3.tbccorp.com"
	
	#Random Run For Inventory And Starting RPOS Auto-Updater
	$script:StartTimeDelay = Get-Random -Maximum 3600
	$script:InventoryRunTime = Get-Random -Maximum 600
	$script:RandomLetter = -join ((65 .. 90) + (97 .. 122) | Get-Random -Count 7 | % { [char]$_ })
	
	#Name of XML file that will get deposited into TPOSDIST01
	$script:XMLDeposit = "${name}_${RandomLetter}.xml"
	

}

#########################################################################################################################################
# Function Set-RPOSModuleTeslaCheckSymbolicLink
function Set-RPOSModuleTeslaCheckSymbolicLink
{
	
	$SymbolicLinkUsers = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
	
	foreach ($SymbolicLinkUser in $SymbolicLinkUsers)
	{
		if (($SymbolicLinkUser -eq "shprorpos") -or ($SymbolicLinkUser -eq "shprorpos2") -or ($SymbolicLinkUser -eq "Public"))
		{
			Write-Output "$SymbolicLinkUser is a system profile NO Link Needed" | Out-File -Append -FilePath $Log
			
		}
		Else
		{
			$TestUserSymbolicLink = Get-Item "C:\Users\$SymbolicLinkUser\AppData\Local\tesla-electron" | Select-Object -ExpandProperty LinkType
			if ($TestUserSymbolicLink -eq "SymoblicLink")
			{
				Write-Output "$SymbolicLinkUser has link already installed proceeding to next profile" | Out-File -Append -FilePath $Log
			}
			Else
			{
				Write-Output "$SymbolicLinkUser had no link installed, installing symbolic link on profile" | Out-File -Append -FilePath $Log
				Remove-Item "C:\Users\$SymbolicLinkUser\AppData\Local\tesla-electron" -Recurse -Force
				New-Item -ItemType SymbolicLink "C:\Users\$SymbolicLinkUser\AppData\Local" -Name tesla-electron -Value "$TeslaPublicFold" -Force
			}
		}
		
	}
	
	icacls "C:\Users\Public\AppData\Local\tesla-electron" /grant Everyone:F
}

#########################################################################################################################################
# Function Set-RPOSModuleTeslaDeleteProfile
function Set-RPOSModuleTeslaDeleteProfile
{
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	#Start uninstalling and installing tesla
	Remove-Item "C:\Users\Public\AppData\Local\tesla-electron" -Recurse -Force
	Write-Output "Tesla has been removed from Public AppData folder" | Out-File -Append -Filepath $Log
	Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force
	
	$TeslaProfiles = Get-ChildItem "C:\Users\" | Select-Object -ExpandProperty Name
	
	foreach ($TeslaProfile in $TeslaProfiles)
	{
		Remove-Item "C:\Users\$TeslaProfile\AppData\Local\tesla-electron" -Recurse -Force
		Write-Output "Tesla-electron folder has been deleted from $TeslaProfile" | Out-File -Append -Filepath $Log
	}
	
}


#########################################################################################################################################
#Function Set-RPOSModuleTeslaClearDatabase
function Set-RPOSModuleTeslaClearDatabase
{
	#Shutting down Tesla, FlowModule, RPOS before clearing the Database
	Write-Output "Shutting down RPOS, FlowModule, RPOS before clearing the database for the morning" | Out-File -Append -FilePath $Log
	Write-Output "Closing Tesla.." | Out-File -Append -FilePath $Log
	Write-Output "Was Electron running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was Proton Running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	Write-Output "Closing FlowModule.." | Out-File -Append -FilePath $Log
	Write-Output "Was FlowModule running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Closing RPOS.." | Out-File -Append -FilePath $Log
	Write-Output "Was RPOS Running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName rpos* | Out-File -Append -Filepath $Log
	
	#Clearing out the database files everynight to prevent any issues the next business day.
	Write-Output "Clearing out Tesla Database at midnight to improve preformace in the morning." | Out-File -Append -FilePath $Log
	
	Remove-Item "C:\Users\Public\AppData\Local\tesla-electron\resources\db" -Recurse -Force
	Write-Output "Finished clearing out database files." | Out-File -Append -FilePath $Log
}

#########################################################################################################################################
#Function Set-RPOSModuleTeslaInstall
function Set-RPOSModuleTeslaInstall
{

	#Grabbing server address from the previous selection in the menu launch.
	$script:server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
	
	#Setting small tesla file values
	$tfile = "Tesla.exe"
	
	#Starting to Install RPOS Tesla MiddleLayer Module
	Write-Output "########################################" | Out-File -Append -FilePath $Log
	Write-Output "#           Starting TESLA             #" | Out-File -Append -FilePath $Log
	Write-Output "########################################" | Out-File -Append -FilePath $Log
	Write-Output "Starting the installtion of RPOS Tesla Middleware Application" | Out-File -Append -FilePath $Log
	Write-Output "Copying over the older version of Tesla, just in case needing to rollback" | Out-File -Append -FilePath $Log
	Copy-Item -Path $teslafileexe -Destination $teslafileBakexe -PassThru -Force | Out-File -Append -FilePath $Log
	Write-Output "Removing older Tesla files from the root directory of RPOS Folder" | Out-File -Append -FilePath $Log
	Remove-Item $teslafileexe -Force
	
	#Grabbing current tesla installer hash from the server
	$TeslaHashURL = "$server/checksum.xml"
	$Teslaxmlread = [xml](New-Object System.Net.WebClient).downloadstring($TeslaHashURL)
	$teslahash = $Teslaxmlread.checksum.Tesla
	
	#Removing old Tesla Files from RPOS Installer Root
	Remove-Item "$teslafileexe" -Force
	Remove-Item "$teslaShareFile" -Force
	

	while ($teslasumnum -lt 5)
	{
			$teslasumnum += 1
			Write-Output "$teslasumnum is the current count of the amount of times script has tried redownloading max 5 times" | Out-File -Append -FilePath $Log
			#Setting download counter due to download count		
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Tesla DCount" -Value $teslasumnum -Force
			Write-Output "Starting re-download of Tesla for the $teslasumnum time" | Out-File -Append -FilePath $Log
			
			#Removing Local copy of Adobe AIR
			Write-Output "Removing current local copy of Tesla if it exists" | Out-File -Append -FilePath $Log
			Remove-Item $teslafileexe -Force
			
			#Downloading RPOS and Flow Files
			Write-Output "Starting to download never version of Tesla"
			$downloadTesla = "$server/$TeslaInstallerFile"
			Invoke-WebRequest -Uri $downloadTesla -OutFile $teslafileexe
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaDistro" -Value "DistroServer" -Force
			Write-Output "$TeslaInstallerFile has been downloaded" | Out-File -Append -FilePath $Log
			
			#Running MD5 checksum to ensure files are NOT! corrupt
			$teslalocalhash = Get-FileHash -Path $teslafileexe -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$teslalocalhash is the MD5 hash of Tesla file of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$teslahash is the MD5 has of Tesla file that server has" | Out-File -Append -FilePath $Log
			if ($teslahash -eq $teslalocalhash) { Break }
	}

	if (($teslasumnum -ne "5") -and ($teslahash -eq $teslalocalhash))
		{
			#Starting the install of Tesla
			Write-Output "Tesla is the correct hash continuing" | Out-File -Append -FilePath $Log
			Write-Output "$teslalocalhash is the MD5 hash of Tesla of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$teslahash is the MD5 has of Tesla that server has" | Out-File -Append -FilePath $Log
			
			#Copying over Tesla for subnet sharing to rest of the PCs
			Remove-Item $TeslaShareFile -Force
			Write-Output "Starting to copy over for subnet sharing" | Out-File -Append -FilePath $Log
			Copy-Item $teslafileexe -Destination $TeslaShareFile -PassThru -Force | Out-File -Append -FilePath $Log
			Write-Output "$TeslaShareFile has been copied for sharing on $TeslaShareFile" | Out-File -Append -FilePath $Log
			
			#Killing RPOS and Flow if running
			Write-Output "Closing RPOS and FlowModule to start the install" | Out-File -Append -FilePath $Log
			Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
			Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
			
			Start-Sleep -Seconds 5
			
			#Killing Tesla if running in the backgroud
			Write-Output "Closing Tesla to start the install" | Out-File -Append -FilePath $Log
			Write-Output "Was Electron running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
			Write-Output "Was Proton Running?" | Out-File -Append -FilePath $Log
			Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
			
			Start-Sleep -Seconds 10
			Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
			Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
			
			#Start uninstalling and installing tesla
			Remove-Item "C:\Users\Public\AppData\Local\tesla-electron" -Recurse -Force
			Write-Output "Tesla has been removed from Public AppData folder" | Out-File -Append -Filepath $Log
			Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force
		
			$TeslaProfiles = Get-ChildItem "C:\Users\" | Select-Object -ExpandProperty Name
		
			foreach ($TeslaProfile in $TeslaProfiles)
			{
				Remove-Item "C:\Users\$TeslaProfile\AppData\Local\tesla-electron" -Recurse -Force
				Write-Output "Tesla-electron folder has been deleted from $TeslaProfile" | Out-File -Append -Filepath $Log
			}
			
			#Starting to install tesla
			Start-Process $teslafileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
			Write-Output "Tesla has completed installing" | Out-File -Append -Filepath $Log
			
			Start-Sleep -Seconds 10
			Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
			Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
			
			If (Test-Path $PublicAppData) {
			Write-Output "The Public AppData Folder Exists, No need to make one" | Out-File -Append -FilePath $Log	
			}
			Else
			{
				New-Item "$PublicAppData" -ItemType Directory -Force
				icacls "C:\Users\Public\AppData" /grant Everyone:F
				Write-Output "Public AppData Folder wasn't on this PC made one" | Out-File -Append -FilePath $Log
			}
		
			if (Test-Path $PublicAppDataLocalFolder)
			{
			Write-Output "The Public AppData Local Folder Exists, No need to make one" | Out-File -Append -FilePath $Log
			}
			Else
			{
				New-Item "$PublicAppDataLocalFolder" -ItemType Directory -Force
				icacls "C:\Users\Public\AppData\Local" /grant Everyone:F
				Write-Output "Public AppData Local Folder wasn't on the PC made one" | Out-File -Append -FilePath $Log
			}
		
			
			Copy-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Destination $PublicAppDataLocalFolder -Recurse -Force | Out-File -Append -FilePath $Log
			Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force | Out-File -Append -FilePath $Log
		
			Write-Output "Moving Tesla install over to Public AppData folder for sharing" | Out-File -Append -FilePath $Log
		
			$teslausers = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
			
			foreach ($teslauser in $teslausers) {
			
				$TestUserSymbolicLink = Get-Item "C:\Users\$teslauser\AppData\Local\tesla-electron" | Select-Object -ExpandProperty LinkType
				if ($TestUserSymbolicLink -eq "SymbolicLink")
				{
				Write-Output "There is symbolic link created for Tesla to the public AppData folder for user $teslauser not needed" | Out-File -Append -FilePath $Log
				}
				Else
				{
					if (Test-Path "C:\Users\$teslauser\AppData\Local")
					{
					Write-Output "There was a AppData Local folder in $teslauser account" | Out-File -Append -FilePath $Log
					}
					Else {
					New-Item "C:\Users\$teslauser\AppData\Local" -ItemType Directory -Force
					Write-Output "There was no AppDataLocal folder in $teslauser account creating folder" | Out-File -Append -FilePath $Log
					}
				
				
				if (($teslauser -eq "shprorpos") -or ($teslauser -eq "shprorpos2") -or ($teslauser -eq "Public"))
				{
					Write-Output "These are RPOS Auto-Updater system profiles, do not create a symoblic link for these profiles" | Out-File -Append -FilePath $Log
				}
				Else
				{
					New-Item -ItemType SymbolicLink "C:\Users\$teslauser\AppData\Local" -Name tesla-electron -Value "$TeslaPublicFold" -Force
					Write-Output "Tesla Electrong AppData Symbolic Link has been created for $teslauser" | Out-File -Append -FilePath $Log
				}
			}
			
			
			
		}
		
			icacls "C:\Users\Public\AppData\Local\tesla-electron" /grant Everyone:F
		
	}
		$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		$versionnumberTesla = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaVer" -Value "$versionnumberTesla" -Force
	
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingTesla" -Value "1" -Force
		
		Write-Output "Tesla Install is complete for this newer build" | Out-File -Append -FilePath $Log
		Write-Output "########################################" | Out-File -Append -FilePath $Log
		Write-Output "#           End Of TESLA               #" | Out-File -Append -FilePath $Log
		Write-Output "########################################" | Out-File -Append -FilePath $Log
	}



#########################################################################################################################################
#Function CheckRPOSIcon
function Set-RPOSModuleCheckRPOSIcon
{
	#Checking to see if RPOS Icon exists in $RPOSAUDIRHOME
	If (Test-Path -Path "$RPOSAUDIRBAK\RPOS.lnk") {
		Write-Output "RPOS.lnk is in the RPOS Auto-Updater folder, no action needed" | Out-File -Append -FilePath $Log }
	else {
		Write-Output "RPOS.lnk not found in $RPOSAUDIRBAK coping over from Desktop" | Out-File -Append -FilePath $Log
		Copy-Item "C:\Users\Public\Desktop\RPOS.lnk" -Destination "$RPOSAUDIRBAK" -Force }
	
	#Checking to see if RPOS Icon exists in C:\Users\Public\Desktop
	If (Test-Path -Path "C:\Users\Public\Desktop\RPOS.lnk") {
		Write-Output "RPOS.lnk is in the Public Desktop folder, no action needed" | Out-File -Append -FilePath $Log }
	
	Else {
		Write-Output "RPOS.lnk not found in Public Desktop coping it over to Public Desktop" | Out-File -Append -FilePath $Log
		Copy-Item "$RPOSAUDIRBAK\RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force }
	
	#Checking Permissions of Desktop Icon
	$RPOSPer = icacls "C:\Users\Public\Desktop\RPOS.lnk" | Select-String "Everyone"
	if ($RPOSPer -eq $null) {
		Write-Output "RPOS.lnk on the public desktop does not have everyone permissions, granting permissions" | Out-File -Append -FilePath $Log
		icacls "C:\Users\Public\Desktop\RPOS.lnk" /grant Everyone:F }
	Else {
		Write-Output "RPOS.lnk has everyone permissions not doing anything at this time." | Out-File -Append -FilePath $Log }
	
	#checking to see if Update RPOS is on all users desktop
	If (Test-Path -Path "C:\Users\Public\Desktop\Update RPOS.lnk") {
		Write-Output "Update RPOS.lnk is showing on the public desktop no action is needed" | Out-File -Append -FilePath $Log
	}
	Else {
		Write-Output "Update RPOS.lnk is showing missing at the moment copying it over from backup folder" | Out-File -Append -FilePath $Log
		Copy-Item "$RPOSAUDIRBAK\Update RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force
		icacls "C:\Users\Public\Desktop\Update RPOS.lnk" /grant Everyone:RX
	}
	#Checking to see if FlowModule Desktop Icon Exists
	If (Test-Path -Path "C:\Users\Public\Desktop\FlowModule.lnk") {
		Write-Output "FlowModule Icon was found on the public desktop removing it, to aviod confusion" | Out-File -Append -FilePath $Log
		Remove-Item "C:\Users\Public\Desktop\FlowModule.lnk" -Force }
	Else {
		Write-Output "There was no FlowModule Icon found on the public desktop" | Out-File -Append -FilePath $Log
	}
}

#########################################################################################################################################
#Function FindVersion
function Set-RPOSModuleFindVersion
{
	#Grabbing actual version of RPOS and FlowModule
	[xml]$xmlreadRPOSversion = Get-Content $RPOSVersionLabelXML
	$RPOSxmlversion = $xmlreadRPOSversion.application.versionLabel
	
	#Setting values for RPOS Versions in Registry
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version Label" -Value $RPOSxmlversion -Force
	$rposname2 = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version" -Value $rposname2 -Force
}


#####################################################################################################################################
# Function CheckReportXML
function Set-RPOSModuleCheckReportXML
{
	
	#Get Date for last inventory scan
	$InventoryTimeScan = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"
	
	#Getting Host IP address
	$HostIPAddress = "$ipmain1.$ipmain2.$ipmain3.$ipmain4"
	
	#Setting Up Network Share on TPOSDIST01
	New-PSDrive -Name O -PSProvider FileSystem -Root "\\tposdist01\MbPnTRr7DR$\Bucket"
	
	Write-Output "Grabbing Registry Value Data to send data back to XML Database on TPOSDIST01 for processing" | Out-File -Append -FilePath $Log
	
	#Grabbing Registry Values	
	$XMLName = $Env:COMPUTERNAME
	$RegAdobeAIRVer = (Get-ItemProperty -Path $LANDeskKeys -Name AdobeAIRVer).AdobeAIRVer
	$RegAdobeAIRDistro = (Get-ItemProperty -Path $LANDeskKeys -Name AdobeDistro).AdobeDistro
	$RegAuVerUpdateDate = (Get-ItemProperty -Path $LANDeskKeys -Name "AuVer UpdateDate")."AuVer UpdateDate"
	$RegBGInfoVersion = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoVersion).BGInfoVersion
	$RegBGInfoXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoXMLchecksum).BGInfoXMLchecksum
	$RegEMVIPaddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress")."EMVIPaddress"
	$RegEMVGateway = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVGateway")."EMVGateway"
	$RegFlowModulechecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "FlowModule Checksum")."FlowModule Checksum"
	$RegRPOSAutoUpdater = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Auto-Updater")."RPOS Auto-Updater"
	$RegRPOSChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Checksum")."RPOS Checksum"
	$RegRPOSDCount = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Dcount")."RPOS Dcount"
	$RegRPOSDLSource = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS DL Source")."RPOS DL Source"
	$RegRPOSUpdatedate = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Update Date")."RPOS Update Date"
	$RegRPOSVersion = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version")."RPOS Version"
	$RegRPOSVersionLabel = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version Label")."RPOS Version Label"
	$RegRPOSAUVer = (Get-ItemProperty -Path $LANDeskKeys -Name RPOSAUVer).RPOSAUVer
	$RegRPOSSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	$RegAdobeDownloadCount = (Get-ItemProperty -Path $LANDeskKeys -Name "Adobe DCount")."Adobe DCount"
	$RegEMVInstalled = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVVeriFoneInstalled")."EMVVeriFoneInstalled"
	$RegTeslaVer = (Get-ItemProperty -Path $LANDeskKeys -Name "TeslaVer")."TeslaVer"
	$RegTeslaDistro = (Get-ItemProperty -Path $LANDeskKeys -Name "TeslaDistro")."TeslaDistro"
	$RegTeslaDCount = (Get-ItemProperty -Path $LANDeskKeys -Name "Tesla DCount")."Tesla DCount"
	
	Write-Output "Starting to build XML file to send over to TPOSDIST01" | Out-File -Append -FilePath $Log
	
	#Creating New XML for Data Deposits
	[System.XML.XMLDocument]$RPOSInventory = New-Object System.XML.XMLDocument
	
	#Creating first Root Element	
	[System.XML.XMLElement]$RPOSRoot = $RPOSInventory.CreateElement("RPOS")
	$RPOSInventory.appendChild($RPOSRoot)
	
	# Append as child to an existing node
	[System.XML.XMLElement]$RPOSAttributes1 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("name"))
	$RPOSAttributes1.InnerText = "$XMLName"
	[System.XML.XMLElement]$RPOSAttributes2 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("address"))
	$RPOSAttributes2.InnerText = "$HostIPAddress"
	[System.XML.XMLElement]$RPOSAttributes3 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSVersion"))
	$RPOSAttributes3.InnerText = "$RegRPOSVersion"
	[System.XML.XMLElement]$RPOSAttributes4 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AutoUpdateVersion"))
	$RPOSAttributes4.InnerText = "$RegRPOSAUVer"
	[System.XML.XMLElement]$RPOSAttributes5 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSUpdateDate"))
	$RPOSAttributes5.InnerText = "$RegRPOSUpdatedate"
	[System.XML.XMLElement]$RPOSAttributes6 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSDownloadSource"))
	$RPOSAttributes6.InnerText = "$RegRPOSDLSource"
	[System.XML.XMLElement]$RPOSAttributes7 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSCheckSum"))
	$RPOSAttributes7.InnerText = "$RegRPOSChecksum"
	[System.XML.XMLElement]$RPOSAttributes8 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("FlowModuleCheckSum"))
	$RPOSAttributes8.InnerText = "$RegFlowModulechecksum"
	[System.XML.XMLElement]$RPOSAttributes9 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSVersionLabel"))
	$RPOSAttributes9.InnerText = "$RegRPOSVersionLabel"
	[System.XML.XMLElement]$RPOSAttributes10 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeAIRVersion"))
	$RPOSAttributes10.InnerText = "$RegAdobeAIRVer"
	[System.XML.XMLElement]$RPOSAttributes11 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeDownloadDistro"));
	$RPOSAttributes11.InnerText = "$RegAdobeAIRDistro"
	[System.XML.XMLElement]$RPOSAttributes12 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeDownloadCount"));
	$RPOSAttributes12.InnerText = "$RegAdobeDownloadCount"
	[System.XML.XMLElement]$RPOSAttributes13 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVInstalled"));
	$RPOSAttributes13.InnerText = "$RegEMVInstalled"
	[System.XML.XMLElement]$RPOSAttributes14 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVIP"));
	$RPOSAttributes14.InnerText = "$RegEMVIPaddress"
	[System.XML.XMLElement]$RPOSAttributes15 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVGatewayIP"));
	$RPOSAttributes15.InnerText = "$RegEMVGateway"
	[System.XML.XMLElement]$RPOSAttributes16 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSDownloadCt"));
	$RPOSAttributes16.InnerText = "$RegRPOSDCount"
	[System.XML.XMLElement]$RPOSAttributes17 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("BGInfoVersion"));
	$RPOSAttributes17.InnerText = "$RegBGInfoVersion"
	[System.XML.XMLElement]$RPOSAttributes18 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("BGInfoXMLChecksum"));
	$RPOSAttributes18.InnerText = "$RegBGInfoXMLchecksum"
	[System.XML.XMLElement]$RPOSAttributes19 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("SwitchXMLChecksum"));
	$RPOSAttributes19.InnerText = "$RegRPOSSwitchXMLchecksum"
	[System.XML.XMLElement]$RPOSAttributes20 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AuUpdateDate"));
	$RPOSAttributes20.InnerText = "$RegAuVerUpdateDate"
	[System.XML.XMLElement]$RPOSAttributes21 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("InventoryDate"));
	$RPOSAttributes21.InnerText = "$InventoryTimeScan"
	[System.XML.XMLElement]$RPOSAttributes22 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("TeslaVer"));
	$RPOSAttributes22.InnerText = "$RegTeslaVer"
	[System.XML.XMLElement]$RPOSAttributes23 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("TeslaDistro"));
	$RPOSAttributes23.InnerText = "$RegTeslaDistro"
	[System.XML.XMLElement]$RPOSAttributes24 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("TeslaDCount"));
	$RPOSAttributes24.InnerText = "$RegTeslaDCount"
	
	
	#Save File
	$RPOSInventory.Save("$RPOSAUDIRHOME\$XMLDeposit")
	
	#Copying Data file To TPOSDIST01
	Copy-Item "$RPOSAUDIRHOME\$XMLDeposit" -Destination "\\tposdist01\MbPnTRr7DR$\Bucket" -Force
	Write-Output "File $RPOSAUDIRHOME\$XMLDeposit has been processed and moved to TPOSDIST01" | Out-File -Append -FilePath $Log
	
	#Removing File from Local PC.
	Remove-Item "$RPOSAUDIRHOME\$XMLDeposit" -Force
	Write-Output "Removed $RPOSAUDIRHOME\$XMLDeposit from Local PC" | Out-File -Append -FilePath $Log
	
	Remove-PSDrive -Name O -Force
}

#####################################################################################################################################
# Function CheckBGInfoUpdatewithEMV
function Set-RPOSModuleCheckBGInfoUpdateandEMV
{
	
	#Reading to see if EMV is currently installed on the PC, if it is nothing will happen, otherwise it will start recording the proper reg keys
	[xml]$xmlread = Get-Content $XMLEMVfileRPOS
	$script:Reademv = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
	
	#Checking to see if EMV has been installed already on PC
	$script:ValidateEMV = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVVeriFoneInstalled")."EMVVeriFoneInstalled"
	Write-Output "Does the PC have the EMV files installed which is $ValidateEMV" | Out-File -Append -FilePath $Log
	Write-Output "Auto-Updater is checking to see if EMV XML files and BGInfo needs to be updated" | Out-File -Append -FilePath $Log
	
	#Checking to see if the site is complete and has the correct information in RPOS XML
	if (($ValidateEMV -eq $null) -and ($Result -ne $IPZero) -and ($Reademv -eq $null)) {
		Write-Output "Setup has determained the RPOS needs to switch over to EMV for chip and pin processing." | Out-File -Append -FilePath $Log
		Write-Output "Starting setupEMVxml fuction" | Out-File -Append -FilePath $Log
		Set-RPOSModulesetupEMVxml }
	Else {
		Write-Output "RPOS has already installed EMV on this PC" | Out-File -Append -FilePath $Log }
	
	#Checking to see if the checksum of UpdatedXML's is updated or not.
	if ($checkBGInfoxml -ne $RegBGInfoXMLchecksum)
	{
		
		#adding newer update hash in registry
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoXMLchecksum" -Value $checkBGInfoxml -Force
		
		Write-Output "Checking to see if BGInfo is at the most current verion" | Out-File -Append -FilePath $Log
		Write-Output "Auto-Updater is proceeding with check an newer version of BGInfo EMV Complete" | Out-File -Append -FilePath $Log
		Write-Output "RPOS Auto-Updater is starting to check to see if there is an update for BGInfo" | Out-File -Append -FilePath $Log
		$BGInfoUpdateKey = (Get-ItemProperty -Path $LANDeskKeys -Name "BGInfoVersion")."BGInfoVersion"
		Write-Output "The current version of BGInfo on this PC is $BGInfoUpdateKey" | Out-File -Append -FilePath $Log
		
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/BGInfo/bginfoupdater.xml"
		
		$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
		$m = $xmlread.BGInfoUpdate.$switchvalue.version
		$n = $xmlread.BGInfoUpdate.$switchvalue.hash
		$o = $xmlread.BGInfoUpdate.$switchvalue.URL
		
		Write-Output "Current version of BGinfo on rpos.tbccorp.com is $m" | Out-File -Append -FilePath $Log
		
		if ($BGInfoUpdateKey -ne $m)
		{
			Write-Output "Powershell has identified that PC is not running the current version of BGinfo from TBC starting backing up and downloading" | Out-File -Append -FilePath $Log
			Remove-Item "$RPOSAUDIRBAK\TBC.bgi.bak" -Force
			Write-Output "Removed old backup of BGInfo in backup folder" | Out-File -Append -FilePath $Log
			Copy-Item -Path "C:\bginfo\TBC.bgi" -Destination "$RPOSAUDIRBAK\TBC.bgi.bak" -PassThru -Force | Out-File -Append -FilePath $Log
			Write-Output "Backed up previous version of BGInfo to $RPOSAUDIRBAK"
			Remove-Item $BGInfoLocalFile -Force
			
			Write-Output "Starting download of New BGInfo file" | Out-File -Append -FilePath $Log
			
			#Downloading hash checksum for swutch value
			Copy-Item -Path "$o" -Destination "$BGInfoLocalFile" -Force
			Write-Output "TBC.bgi has been downloaded" | Out-File -Append -FilePath $Log
			
			Write-Output "Checking file hash of the new bginfo file" | Out-File -Append -FilePath $Log
			$LocalBGInfoHash = Get-FileHash -Path $BGInfoLocalFile -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$LocalBGInfoHash is the hash of the BGInfo file that was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$n is the hash of the BGInfo file on the server" | Out-File -Append -FilePath $Log
			
			if ($LocalBGInfoHash -ne $n)
			{
				while ($sumnumbginfo -lt 5)
				{
					$sumnumbginfo += 1
					Write-Output "The BGInfo file that was download is not the correct hash trying to re-download count equals $sumnumbginfo" | Out-File -Append -FilePath $Log
					Write-Output "Starting download of New BGInfo file" | Out-File -Append -FilePath $Log
					
					#Downloading hash checksum for swutch value
					Copy-Item -Path "$o" -Destination "$BGInfoLocalFile" -Force
					Write-Output "TBC.bgi has been downloaded" | Out-File -Append -FilePath $Log
					$LocalBGInfoHash = Get-FileHash -Path $BGInfoLocalFile -Algorithm MD5 | select -ExpandProperty hash
					if ($LocalBGInfoHash -eq $n) { break }
				}
			}
			
			
			
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoVersion" -Value $m -Force
			
			#Restarting PC to ensure that the newer BGInfo installs correctly
			Write-Output "Restarting PC to ensure settings on BGInfo display on store PC desktop Be Right Back!!!" | Out-File -Append -FilePath $Log
			Restart-Computer -Force
			Start-Sleep -Seconds 60
		}
		Else
		{
			#Value for Progress Bar
			#Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "1" -Force
			
			Write-Output "Powershell has identified this PC is running the current version of BGinfo which is $BGInfoUpdateKey" | Out-File -Append -FilePath $Log
		}
		
	}
	Else
	{
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "1" -Force
		
		Write-Output "Auto-Updater doesn't see any changes to BGInfo.xml files bypassing BGInfo check" | Out-File -Append -FilePath $Log
	}
	
	if ($Result -ne $IPZero)
	{
		Write-Output "Starting to check to see if any IP changes were made for the RPOS-Preferences file to BGInfo" | Out-File -Append -FilePath $Log
		$BGEMVIPAddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress")."EMVIPaddress"
		$EMVXMLVeriFone3digitIP = "$XMLip1$XMLip2$XMLip3$XMLip4"
		Write-Output "IP address of the RPOS-Preferences XML is $EMVXMLVeriFone3digitIP and current registry IP is $BGEMVIPAddress" | Out-File -Append -FilePath $Log
		
		#Checking to see if the IP address in registry for BGInfo EMV needs to be changed from the XML
		if ($BGEMVIPAddress -ne $EMVXMLVeriFone3digitIP)
		{
			Write-Output "BGInfo Registry key for IP address desktop was not matching updating with newer IP address" | Out-File -Append -FilePath $Log
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Value $EMVXMLVeriFone3digitIP -Force
			$RPOSEMVxmlgateway = "$XMLip1$XMLip2${XMLip3}001"
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Value $RPOSEMVxmlgateway -Force
		}
		Else
		{
			Write-Output "BGInfo Reigstry key for IP address desktop was matching the current RPOSXML" | Out-File -Append -FilePath $Log
		}
		
		#Checking to see if XML ETIM IP address is current with EMV Ip address in RPOS-preferences.xml
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$script:ResultEMV = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
		
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for NON-EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$Read = $xmlread.ApplicationPreferences.creditCardTerminalUrl | Select-Object -ExpandProperty "#cdata-section"
		$script:ResultNonEMV = $Read -replace ":9001"
		
		#If the IP address for the verifone device has changed change all the rest of the xml files and values.
		if (($ResultEMV -ne $ResultNonEMV) -and ($ResultEMV -ne $null))
		{
			Write-Output "Setup has found there has been a change in the RPOS-Perferences Verifone EMV IP address" | Out-File -Append -FilePath $Log
			Write-Output "Changing IP addresses from $ResultNonEMV to $ResultEMV" | Out-File -Append -FilePath $Log
			
			$ResultEMVtoNonEMV = "${ResultEMV}:9001"
			
			#putting current EMV IP address in Flow RPOS-Preferences.xml
			[xml]$Flowxml = get-content "$XMLEMVfileFlow"
			$Flowxml.applicationpreferences.pointSCAPaymentDeviceIPAddress.'#cdata-section' = "$ResultEMV"
			$Flowxml.Save("$XMLEMVfileFlow")
			
			#putting current EMV IP address with :9001 in RPOS RPOS-Preferences.xml
			[xml]$RPOSxml = get-content "$XMLEMVfileRPOS"
			$RPOSxml.applicationpreferences.creditcardterminalurl.'#cdata-section' = "$ResultEMVtoNonEMV"
			$RPOSxml.Save("$XMLEMVfileRPOS")
			
			#putting current EMV IP address with :9001 in FLOW RPOS-Preferences.xml
			[xml]$Flowxml = get-content "$XMLEMVfileFlow"
			$Flowxml.applicationpreferences.creditcardterminalurl.'#cdata-section' = "$ResultEMVtoNonEMV"
			$Flowxml.Save("$XMLEMVfileFlow")
			
		}
	}
	
	Write-Output "BGInfo IP address for RPOS Preferences and New BGInfo is complete exiting this section of the Auto-Updater" | Out-File -Append -FilePath $Log
}

#####################################################################################################################################
#Function setupEMVxml
function Set-RPOSModulesetupEMVxml
{
	
	Write-Output "Starting RPOS EMV preferences XML Encoding" | Out-File -Append -FilePath $Log
	Write-Output "Name of the user profile, in which PowerShell will be working with is $profilename" | Out-File -Append -FilePath $Log
	if ($Result -eq $IPZero)
	{
		Write-Output "The RPOS XML has an IP address of $Result not changing the xml's on the PC. Exiting the portion of RPOS Auto-Updater EMV XML Edit" | Out-File -Append -FilePath $Log
	}
	Else
	{
		Write-Output "RPOS preferences file is $XMLEMVfileRPOS" | Out-File -Append -FilePath $Log
		#Starting To Backup Preferences Files
		Write-Output "PowerShell is now backing up Store User Profile RPOS Preferences files" | Out-File -Append -FilePath $Log
		Copy-Item -Path $XMLEMVfileRPOS -Destination $XMLEMVRPOSbak -PassThru -Force | Out-File -Append -FilePath $Log
		
		if (Test-Path $XMLEMVfileFlow)
		{
			Write-Output "Flow RPOS preferences file was found" | Out-File -Append -FilePath $Log
			Write-Output "Flow RPOS preferences file is $XMLEMVfileFlow" | Out-File -Append -FilePath $Log
			#Starting To Backup Preferences Files
			Write-Output "PowerShell is now backing up Store User Profile RPOS Flow Preferences files" | Out-File -Append -FilePath $Log
			Copy-Item -Path $XMLEMVfileFlow -Destination $XMLEMVFLOWbak -PassThru -Force | Out-File -Append -FilePath $Log
		}
		Else
		{
			Write-Output "Flow RPOS preferences file was not found no backup needed" | Out-File -Append -FilePath $Log
		}
		
		#Starting To pull XML's into PowerShell to start editing
		Write-Output "$script:Result is the IP address that will be inserted into pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
		Write-Output "Starting to edit RPOS Preferences file with $Result IP address in Append Child pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
		
		#Starting to edit RPOS Preferences File
		$xmlrpos = [xml](Get-Content $XMLEMVfileRPOS)
		$child1 = $xmlrpos.CreateElement("pointSCAPaymentDeviceIPAddress")
		$child1.InnerXml = "<![CDATA[$Result]]>"
		$xmlrpos.ApplicationPreferences.AppendChild($child1)
		$xmlrpos.Save("$XMLEMVfileRPOS")
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Value "true" -Force
		
		if (Test-Path $XMLEMVfileFlow)
		{
			Write-Output "Starting to edit RPOS Flow Preferences file with $Result IP address in Append Child pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
			#Starting to edit RPOS Flow Preferences File
			$xmlflow = [xml](Get-Content $XMLEMVfileFlow)
			$child2 = $xmlflow.CreateElement("pointSCAPaymentDeviceIPAddress")
			$child2.InnerXml = "<![CDATA[$Result]]>"
			$xmlflow.ApplicationPreferences.AppendChild($child2)
			$xmlflow.Save("$XMLEMVfileFlow")
			
			
		}
		Else
		{
			
			Write-Output "Flow RPOS preferences file was not found no editing needed" | Out-File -Append -FilePath $Log
		}
		
		Write-Output "Script is done editing preferences files, it is complete" | Out-File -Append -FilePath $Log
		
		#Grabbing the IP address from perviously used value above
		$RPOSEMVxmlIPaddress = "$XMLip1$XMLip2$XMLip3$XMLip4"
		$RPOSEMVxmlgateway = "$XMLip1$XMLip2${XMLip3}001"
		
		#Exporting IP address to BGInfo text file
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Value $RPOSEMVxmlIPaddress -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Value $RPOSEMVxmlgateway -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Value "true" -Force
	}
}


##########################################################################################################################################
#Function What PC Am I?
function Set-RPOSModuleWhatPCAmI
{
	
	If ($name -match "S[0-9][0-9][0-9]") {
		Write-Output "Powershell has found out that this PC is Store PC" | Out-File -Append -FilePath $Log
		$script:pname = $name
		$script:pname = $pname.Substring(1, 3)
		$script:switchvalue = "store${pname}"
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchstore.xml"
		$script:XMLswitchchecksum = "switchstore"
		$script:XMLbginfochecksum = "switchbginfo" }
	
	if ($name -match "STAC[0-9][0-9][0-9]") {
		Write-Output "Powershell has found out that this PC is TAC Warehouse PC" | Out-File -Append -FilePath $Log
		$script:taclist = $name
		$script:taclist = $taclist.Substring(0, 7)
		$script:switchvalue = $taclist
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchtac.xml"
		$script:XMLswitchchecksum = "switchtac"
		$script:XMLbginfochecksum = "switchbginfo" }
	
	if ($name -match "CT[0-9][0-9]") {
		Write-Output "Powershell has found out that this PC is Carroll Tire PC" | Out-File -Append -FilePath $Log
		$script:ctpc = $name
		$script:ctpc = $ctpc.Substring(0, 4)
		$script:switchvalue = $ctpc
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchct.xml"
		$script:XMLswitchchecksum = "switchct"
		$script:XMLbginfochecksum = "switchbginfo" }
	
	if (($name -notmatch "S[0-9][0-9][0-9]") -and ($name -notmatch "STAC[0-9][0-9][0-9]") -and ($name -notmatch "CT[0-9][0-9]")) {
		Write-Output "Powershell has found out that this PC is Corporate, Personal and/or out of the normal PC naming scheme." | Out-File -Append -FilePath $Log
		$script:switchvalue = $name
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchcorp.xml"
		$script:XMLswitchchecksum = "switchcorp"
		$script:XMLbginfochecksum = "switchbginfo" }
}

##########################################################################################################################################
#Function Start Self Elect Process
function Set-RPOSModuleStartSelfElect
{
	#Checking to see if LiveNames.txt is already there
	
	If (Test-Path $LiveIP)
	{
		Write-Output "LiveIP text file was found in the auto-updater grabbing PC names from file" | Out-File -Append -FilePath $Log
		$selected = Get-Content $LiveIP
		Write-Output "$selected PC discovered" | Out-File -Append -FilePath $Log
	}
	Else
	{
		#Checking local PC IP address, then scan network, then convert IP Addresses to PC Names
		$finalIP = "$ipmain1.$ipmain2.$ipmain3."
		Write-Output "$finalIP is the subnet that this PC will be scanning" | Out-File -Append -FilePath $Log
		$ping = New-Object System.Net.Networkinformation.ping
		
		Write-Output "Starting scan of PC's on the subnet" | Out-File -Append -FilePath $Log
		1 .. 254 | % { $ping.Send("${finalIP}$_", 500) } | select-object -ExpandProperty Address | Select -ExpandProperty IPAddressToString | Out-File -Append -FilePath $LiveIP
		#Getting getting rid of all the 10.x.x.1 addresses
		$getrid = Get-Content $LiveIP
		$getrid | Where-Object { $_ -notcontains "${finalIP}1" } | Set-Content $LiveIP
		#########################################################
		#Changed on 2-21-2018 NetBIOS Names have been disabled  #
		#########################################################
		#End of getting rid of all the 10.x.x.1 addresses
		#$IPs = Get-Content $LiveIP
		#Write-Output "Starting to get PC names from IP list" | Out-File -Append -FilePath $Log
		#$IPs | ForEach-Object { ([system.net.dns]::GetHostByAddress($_)).hostname } | Out-File -Append -FilePath $LiveNames
		#########################################################
		$script:selected = Get-Content $LiveIP
		Write-Output "$script:selected PC discovered" | Out-File -Append -FilePath $Log
	}
}

##########################################################################################################################################
#Function Install POS - Default Installer Process for RPOS
function Set-RPOSModuleInstallPOS
{
	
	#Switching registry key to zero that tells the Auto-Updater desktop if it was a successful install or not
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "0" -Force
	
	#Adding updated date of RPOS update in registry for LANDesk Inventory
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Value $datereg -Force
	
	#Downloading Adobe Arh file if needed
	if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
	Else
	{
		$arh = "http://rpos.tbccorp.com/arh.exe"
		Invoke-WebRequest -Uri $arh -OutFile $arhfile
		Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
	}
	
	#Backing Up Current RPOS Build determaining if it is EXE or AIR file.
	Write-Output "Removing old bak files if there are in the BAK directory" | Out-File -Append -FilePath $Log
	Remove-Item "$RPOSfileBakair" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$flowfileBakair" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$RPOSfileBakexe" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$flowfileBakexe" -Force | Out-File -Append -FilePath $Log
	
	if ($drfile -eq "RPOS.air")
	{
		Write-Output "Powershell has found an older version of RPOS AIR on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$drfile" -Destination $RPOSfileBakair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$drfile has been copied to $RPOSfileBakair Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq "FlowModule.air")
	{
		Write-Output "Powershell has found an older version of FlowModule AIR on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$dffile" -Destination $flowfileBakair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$dffile has been copied to $flowfileBakair Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($drfile -eq "RPOS.exe")
	{
		Write-Output "Powershell has found an older version of RPOS EXE on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$drfile" -Destination $RPOSfileBakexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$drfile has been copied to $RPOSfileBakexe Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq "FlowModule.exe")
	{
		Write-Output "Powershell has found an older version of FlowModule EXE on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$dffile" -Destination $flowfileBakexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$dffile has been copied to $flowfileBakexe Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($drfile -eq $null)
	{
		Write-Output "Powershell did not find an older version of RPOS on this PC" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq $null)
	{
		Write-Output "Powershell did not find an older version of FlowModule on this PC" | Out-File -Append -FilePath $Log
	}
	
	#Removing Previous build files
	#If there is an EXE file delete AIR file and if there is an AIR file delete EXE files	
	if (($rfile -ne $drfile) -and ($drfile -eq "RPOS.air"))
	{
		Write-Output "Script found it is downloading EXE files deleting all AIR Files" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\rpos.air" -Force
		Remove-Item "$RPOSAUDIRHOME\FlowModule.air" -Force
		Remove-Item "$RPOSAUDIRBUILD\rpos.air" -Force
		Remove-Item "$RPOSAUDIRBUILD\FlowModule.air" -Force
	}
	if (($rfile -ne $drfile) -and ($drfile -eq "RPOS.exe"))
	{
		Write-Output "Script found it is downloading AIR files deleting all EXE Files" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\rpos.exe" -Force
		Remove-Item "$RPOSAUDIRHOME\FlowModule.exe" -Force
		Remove-Item "$RPOSAUDIRBUILD\rpos.exe" -Force
		Remove-Item "$RPOSAUDIRBUILD\FlowModule.exe" -Force
	}
	if (($rfile -eq $drfile) -and ($ffile -eq $dffile))
	{
		Write-Output "Script found the same AIR or EXE files deleting the same file" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\$drfile" -Force
		Remove-Item "$RPOSAUDIRHOME\$dffile" -Force
	}
	if (($drfile -eq $null) -and ($dffile -eq $null))
	{
		Write-Output "Powershell didn't find any files FlowModule or RPOS no need to delete" | Out-File -Append -FilePath $Log
	}
	
	#Testing to see if there was already a subnet scan completed, if so it will use the current LiveNames file and run it against it
	if (Test-Path $LiveIP)
	{
		Write-Output "LiveIP.txt has been found on the PC at this time proceeding grabbing content from text file" | Out-File -Append -FilePath $Log
		$selected = Get-Content $LiveIP
	}


	while ($sumnum -lt 5)
	{
			$sumnum += 1
			Write-Output "$sumnum is the current count of the amount of times script has tried redownloading max 5 times" | Out-File -Append -FilePath $Log
			#Setting download counter due to download count		
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value $sumnum -Force
			Write-Output "Starting re-download of RPOS and FlowModule for the $sumnum time" | Out-File -Append -FilePath $Log
			Set-RPOSModuledownloadRPOS
			#Running MD5 checksum to ensure files are NOT! corrupt
			$nhashRPOS = Get-FileHash -Path $RPOSfile -Algorithm MD5 | select -ExpandProperty hash
			$nhashFlow = Get-FileHash -Path $flowfile -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$nhashRPOS is the MD5 hash of RPOS of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$nhashFlow is the MD5 hash of FlowModule of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$xr is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
			Write-Output "$xf is the MD5 has of FlowModule that server has" | Out-File -Append -FilePath $Log
			if (($xr -eq $nhashRPOS) -and ($xf -eq $nhashFlow)) { Break }
	}
	
	
	if (($sumnum -ne "5") -and ($xr -eq $nhashRPOS) -and ($xf -eq $nhashFlow))
	{
		#Value For Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "1" -Force
		
		Write-Output "RPOS and FlowModule are matching the correct hash continuing" | Out-File -Append -FilePath $Log
		Write-Output "$nhashRPOS is the MD5 hash of RPOS of what was downloaded" | Out-File -Append -FilePath $Log
		Write-Output "$nhashFlow is the MD5 hash of FlowModule of what was downloaded" | Out-File -Append -FilePath $Log
		Write-Output "$xr is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
		Write-Output "$xf is the MD5 has of FlowModule that server has" | Out-File -Append -FilePath $Log
		
		#Coping file over to subnet file share
		Remove-Item $ShareFlow -Force
		Remove-Item $ShareRPOS -Force
		Write-Output "Starting to copy over for subnet sharing" | Out-File -Append -FilePath $Log
		Copy-Item $RPOSfile -Destination $ShareRPOS -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSfile has been copied for sharing on $ShareRPOS" | Out-File -Append -FilePath $Log
		Copy-Item $flowfile -Destination $ShareFlow -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "$flowfile has been copied for sharing on $ShareFlow" | Out-File -Append -FilePath $Log
		
		#Killing RPOS and Flow if running
		Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		
		#Value For Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "1" -Force
		
		#Uninstalling RPOS
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sur = $p.ExitCode
		Remove-Item "$RPOSHome" -Recurse -Force
		Write-Output "Arh silent RPOS uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		#Uninstalling Flow
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:suf = $p.ExitCode
		Write-Output "Arh silent FlowModule uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
		
		#Uninstalling Flow
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "1" -Force
		
		Start-Sleep -s 15
		
		if ($rfile -eq "RPOS.air")
		{
			#Installing RPOS AIR
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfile" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		if ($rfile -eq "RPOS.exe")
		{
			#Installing RPOS EXE
			$p = Start-Process "$RPOSAUDIRHOME\RPOS.exe" -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
			
		}
		
		Start-Sleep -s 15
		
		if ($ffile -eq "FlowModule.air")
		{
			#Installing FlowModule AIR
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfile" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		if ($ffile -eq "FlowModule.exe")
		{
			#Installing FlowModule EXE
			$p = Start-Process "$RPOSAUDIRHOME\FlowModule.exe" -ArgumentList "-silent" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
		}

		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "1" -Force
		
		#Logging amount of times download failed and file hashes to LANDesk Inventory scan and version of RPOS running on PC
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value $sumnum -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Checksum" -Value $nhashRPOS -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "FlowModule Checksum" -Value $nhashFlow -Force

		#Starting Removal Of Tesla
		Set-RPOSModuleTeslaDeleteProfile
			
		#Starting to install Tesla
		Set-RPOSModuleTeslaInstall
		
		#Setting RPOS Version Labels in Registry
		Set-RPOSModuleFindVersion
				
		#Removing IP address text files and DNS names of PC's
		Remove-Item $LiveIP -Force
		Remove-Item $LiveNames -Force
	}
	Else
	{
		Write-Output "RPOS was not successful on downloading files correctly - no install or action will be taken" | Out-File -Append -Filepath $Log
	}
}

##########################################################################################################################################
#Fuction Install POS Without backing up previous build - Default Installer Process for RPOS
function Set-RPOSModuleInstallPOSnobackup
{	
	#Adding updated date of RPOS update in registry for LANDesk Inventory
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Value $datereg -Force
	
	#Downloading Adobe Arh file if needed
	if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
	Else
	{
		$arh = "http://rpos.tbccorp.com/arh.exe"
		Invoke-WebRequest -Uri $arh -OutFile $arhfile
		Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
	}
	
	#Killing RPOS and Flow if running
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	
	#Value for Progress Bar
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "1" -Force
	
	#Uninstalling RPOS
	$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
	$p.HasExited
	$script:sur = $p.ExitCode
	Remove-Item "$RPOSHome" -Recurse -Force
	Write-Output "Arh silent RPOS uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
	
	Start-Sleep -s 15
	
	#Uninstalling Flow
	$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
	$p.HasExited
	$script:suf = $p.ExitCode
	Write-Output "Arh silent FlowModule uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
	
	Start-Sleep -s 15
	
	if ($rfile -eq "RPOS.air")
	{
		#Installing RPOS AIR
		$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfile" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sir = $p.ExitCode
		Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
	}
	
	if ($rfile -eq "RPOS.exe")
	{
		#Installing RPOS EXE
		$p = Start-Process "$RPOSAUDIRHOME\RPOS.exe" -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sir = $p.ExitCode
		Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
	}
	
	Start-Sleep -s 15
	
	if ($ffile -eq "FlowModule.air")
	{
		#Installing FlowModule AIR
		$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfile" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sif = $p.ExitCode
		Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
	}
	
	if ($ffile -eq "FlowModule.exe")
	{
		#Installing Flow
		$p = Start-Process "$RPOSAUDIRHOME\FlowModule.exe" -ArgumentList "-silent" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sif = $p.ExitCode
		Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
	}
	
	#############################################################################
	#      Tesla Install no POS Backup Added on 5-10-2018                       #
	#############################################################################
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log

	
	Start-Process $teslafileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
	
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	Copy-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Destination $PublicAppDataLocalFolder -Recurse -Force | Out-File -Append -FilePath $Log
	Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force | Out-File -Append -FilePath $Log
	
	$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
	$versionnumberTesla = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
	$teslausers = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
	
	icacls "C:\Users\Public\AppData\Local\tesla-electron" /grant Everyone:F
	
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaVer" -Value "$versionnumberTesla" -Force
	
	foreach ($teslauser in $teslausers) {
		
			if (Test-Path "C:\Users\$teslauser\AppData\Local") {
				Write-Output "There was a AppData Local folder in $teslauser account" | Out-File -Append -FilePath $Log
			}
			Else {
				New-Item "C:\Users\$teslauser\AppData\Local" -ItemType Directory -Force
				Write-Output "There was no AppDataLocal folder in $teslauser account creating folder" | Out-File -Append -FilePath $Log
			}
			
			if (($teslauser -eq "shprorpos") -or ($teslauser -eq "shprorpos2") -or ($teslauser -eq "Public")) {
				Write-Output "These are RPOS Auto-Updater system profiles, do not create a symoblic link for these profiles" | Out-File -Append -FilePath $Log
			}
			Else {
				New-Item -ItemType SymbolicLink "C:\Users\$teslauser\AppData\Local" -Name tesla-electron -Value "$TeslaPublicFold" -Force
				Write-Output "Tesla Electrong AppData Symbolic Link has been created for $teslauser" | Out-File -Append -FilePath $Log
			}
		
		
	}
	
	#Value for Progress Bar
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "1" -Force
	#Value for Progress Bar
	Set-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "1" -Force
	
	#Grabbing Values of versions for registry
	Set-RPOSModuleFindVersion

}

##########################################################################################################################################
#Fuction Remove RPOS Manually - This will remove all Registry Keys and files/folders if installer will not work
function Set-RPOSModuleManualRemove
{
	$RegRPOS64 = @("HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegRPOS64App = Get-ItemProperty $RegRPOS64 -EA 0
	$WantedRPOS64 = $RegRPOS64App | Where { $_.DisplayName -like "*RPOS*" }
	$WantedRPOS64 = $WantedRPOS64.PSChildName
	
	$RegRPOS32 = @("HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegRPOS32App = Get-ItemProperty $RegRPOS32 -EA 0
	$WantedRPOS32 = $RegRPOS32App | Where { $_.DisplayName -like "*RPOS*" }
	$wantedRPOS32 = $WantedRPOS32.PSChildName
	
	$RegRPOSInstall = @("HKLM:\Software\Classes\Installer\Products\*")
	$RegRPOSInstallApp = Get-ItemProperty $RegRPOSInstall -EA 0
	$WantedRPOSInstall = $RegRPOSInstallApp | Where { $_.ProductName -like "*RPOS*" }
	$WantedRPOSInstall = $WantedRPOSInstall.PSChildName
	
	$RegFLOW64 = @("HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegFLOW64App = Get-ItemProperty $RegFLOW64 -EA 0
	$WantedFlow64 = $RegFLOW64App | Where { $_.DisplayName -like "*FlowModule*" }
	$WantedFlow64 = $WamtedFlow64.PSChildName
	
	$RegFLOW32 = @("HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegFLOW32App = Get-ItemProperty $RegFLOW32 -EA 0
	$WantedFlow32 = $RegFLOW32App | Where { $_.DisplayName -like "*FlowModule*" }
	$wantedFlow32 = $WantedFlow32.PSChildName
	
	foreach ($key in $WantedRPOS64) {
		if ($key -eq "RPOSAutoUpdater") {
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	foreach ($key in $WantedRPOS32) {
		if ($key -eq "RPOSAutoUpdater") {
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	foreach ($key in $WantedRPOSInstall) {
		
		Remove-Item "HKLM:\Software\classes\Installer\Products\$key" -Recurse -ErrorAction SilentlyContinue -Force
		Write-Output "Registry Key HKLM:\Software\classes\Installer\Products\$key was deleted" | Out-File -Append -FilePath $Log
	}
	
	foreach ($key in $WantedFlow64) {
		if ($key -eq "RPOSAutoUpdater") {
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\classes\Installer\Products\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	foreach ($key in $WantedFlow32) {
		if ($key -eq "RPOSAutoUpdater")
		{
			Write-Output "Found HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	Remove-Item -Path "$RPOSHOME" -Recurse -Force
	Write-OutPut "Folder C:\Program Files (x86\RPOS was deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "$FLOWHOME" -Recurse -Force
	Write-OutPut "Folder C:\Program Files (x86\FlowModule was deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "C:\Users\Public\Desktop\RPOS.lnk" -Force
	Write-Output "C:\Users\Public\Desktop\RPOS.lnk has been deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "C:\Users\Public\Desktop\FlowModule.lnk" -Force
	Write-Output "C:\Users\Public\Desktop\FlowModule.lnk has been deleted" | Out-File -Append -FilePath $Log
	
	##################################################################################
	#           Adding Tesla Manual Removal Section                                  #
	##################################################################################
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	Write-Output "Starting the process of removing tesla proton and electron manually" | Out-File -Append -FilePath $Log
	
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	Remove-Item "C:\Users\Public\AppData\Local\tesla-electron" -Force -Recurse
	Write-Output "Tesla Electron Public AppData folder has been deleted from PC" | Out-File -Append -FilePath $Log
	
	$teslausersdels = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
	
	foreach ($teslauserdel in $teslausersdels) {
		if (Test-Path "C:\Users\$teslauserdel\AppData\Local\tesla-electron") {
			Remove-Item "C:\Users\$teslauserdel\AppData\Local\tesla-electron" -Recurse -Force
			Write-Output "Removed C:\Users\$teslauserdel\AppData\Local\tesla-electron Symbolic Link" | Out-File -Append -FilePath $Log
		}
		Else {
			Write-Output "$teslauserdel had no symbolic link attached to the profile, no need to remove anything" | Out-File -Append -FilePath $Log	
		}
	}
	
}

##########################################################################################################################################
#Fuction check install - Checking to make sure no bad installs are there
function Set-RPOSModulecheckinstall
{
	Write-Output "Checking to see if Install was successful" | Out-File -Append -FilePath $Log
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	################################################################################################
	#   Checking Tesla Installer Added on 5/10/2018                                                #
	################################################################################################
	
	#Grabbing current tesla installer name to verify if there is the correct version of Tesla Installed and directories are setup.
	$TeslaHashURL = "$server/hashes.xml"
	$Teslaxmlread = [xml](New-Object System.Net.WebClient).downloadstring($TeslaHashURL)
	$teslafolderver = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron" | Where-Object { $_.Name -like "app*" } | Select-Object -ExpandProperty Name
	
	#$teslaveronline = $Teslaxmlread.checksum.file | ? { $_.filename -like "tesla-electron-[0-9].[0-9].[0-9] *" } | Select-Object -ExpandProperty "filename"
	#$teslaveronline = ($teslaveronline).Substring(15, 5)
	
	#$teslaverlocal = (Get-Item "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\tesla-electron.exe").VersionInfo | Select-Object -ExpandProperty FileVersion
	
	Write-Output "Checking Tesla Install....." | Out-File -Append -Filepath $Log
	
	$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
	$TeslaProtonversionWeb = (Get-Content "$RPOSAUDIRHOME\TESLA-PROTON.VERSION").Substring(8)
	$TeslaElectronversionWeb = (Get-Content "$RPOSAUDIRHOME\TESLA-ELECTRON.VERSION").Substring(8)
	
	$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
	$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
	
	#End of Tesla Installer check
	
	#Checking Symoblic Links
	Set-RPOSModuleTeslaCheckSymbolicLink
	
	#If RPOS or FlowModule are blank reinstall them if not blank do nothing
	If (($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($verRPOS -ne $x) -or ($verFLOW -ne $x) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal) -or ($TeslaProtonversionWeb -ne $TeslaProtonversionLocal)) 
	{
		Write-Output "Found Install was not successful" | Out-File -Append -FilePath $Log
		Write-Output "After install RPOS and Flow Status" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is what the check pulled for RPOS" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is what the check pulled for FlowModule" | Out-File -Append -FilePath $Log
		Write-Output "Running installer again for Flow and RPOS" | Out-File -Append -FilePath $Log
		#Write-Output "Tesla version downloaded from server is $teslaveronline and version found on PC hash $teslaverlocal" | Out-File -Append -FilePath $Log
		Write-Output "Tesla install directory found on PC? $CheckingTeslaDirectory" | Out-File -Append -FilePath $Log
		Write-Output "Tesla electron exe found in tesla install directory? $CheckingTeslaElectronexe" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "1" -Force
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "1" -Force
		
		Set-RPOSModuleInstallPOS
	}
	Else
	{
		Write-Output "RPOS and FlowModule Installed checkinstall is complete and passed" | Out-File -Append -FilePath $Log
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "1" -Force
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "1" -Force
		
	}
}

##########################################################################################################################################
#Fuction download RPOS - actual download process for RPOS and FlowModule
function Set-RPOSModuledownloadRPOS
{
	
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "1" -Force
	
	#Removing Previous build files
	Remove-Item $flowfile -Force
	Remove-Item $RPOSfile -Force
	
	#Removing IP scan and DNS names text files	
	Remove-Item $LiveIP -Force
	Remove-Item $LiveNames -Force
	
	#Downloading RPOS and Flow Files
	$RPOS = "$server/$rfile"
	Invoke-WebRequest -Uri $RPOS -OutFile $RPOSfile
	Write-Output "$rfile has been downloaded" | Out-File -Append -FilePath $Log
	$Flow = "$server/$ffile"
	Invoke-WebRequest -Uri $Flow -OutFile $flowfile
	Write-Output "$ffile has been downloaded" | Out-File -Append -FilePath $Log
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Value "DistroServer" -Force
	
}

##########################################################################################################################################
#Fuction switchbuild - This controls the switching of RPOS builds
function Set-RPOSModuleswitchbuild
{
	#Checking to see if there is a switch in the builds
	$URL = $XMLSwitchURL
	
	#Getting switch build response
	$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
	$w = $xmlread.serverswitch.$switchvalue.name
	$x = $xmlread.serverswitch.$switchvalue.statement
	$y = $xmlread.serverswitch.$switchvalue.server
	Write-Output "Switch build revision equals $x" | Out-File -Append -FilePath $Log
	
	#If switch build equals true start downloading distrubtion package on specified server if false continue with version check and install on same server.
	if (($x -eq "true") -and ($rposname -ne $w))
	{
		Write-Output "Switching RPOS Distros was detected switching to $w on $y on $switchvalue" | Out-File -Append -FilePath $Log
		#Setting switch server from XML file
		$server = "http://$y"
		
		#Downloading hash checksum for swutch value
		$URL = "$server/checksum.xml"
		Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash
		
		#Getting HD5 hash numbers
		[xml]$xmlread = Get-Content $XMLhash
		$xr = $xmlread.checksum.RPOS
		[xml]$xmlread = Get-Content $XMLhash
		$xf = $xmlread.checksum.Flow
		Write-Output "$xr is the checksum of RPOS version on $server" | Out-File -Append -FilePath $Log
		Write-Output "$xf is the checksum of FlowModule version on $server" | Out-File -Append -FilePath $Log
		
		#Downloading update XML for switch build
		$URL = "$server/update.xml"
		Invoke-WebRequest -Uri $URL -OutFile $XML
		[xml]$xmlread = Get-Content $xml
		$script:x = $xmlread.update.versionNumber
		Write-Output "$x is the version pulled on $server" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is the version of RPOS pulled on PC" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is the version of Flow pulled on PC" | Out-File -Append -FilePath $Log
			
			
		####################################################################################################
		#Adding New Tesla Module 5-22-2018                                                                 #
		####################################################################################################
		$URLTeslaProton = "$server/TESLA-PROTON.VERSION"
		$URLTeslaElectron = "$server/TESLA-ELECTRON.VERSION"
		Invoke-WebRequest -Uri "$URLTeslaProton" -OutFile $TeslaProtonversion
		Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversion
		$TeslaProtonversionWeb = (Get-Content "$RPOSAUDIRHOME\TESLA-PROTON.VERSION").Substring(8)
		$TeslaElectronversionWeb = (Get-Content "$RPOSAUDIRHOME\TESLA-ELECTRON.VERSION").Substring(8)
			
		$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		$CheckingTeslaDirectory = Test-path "C:\Users\Public\AppData\Local\tesla-electron"
		$CheckingTeslaElectronexe = Test-Path "C:\Users\Public\AppData\Local\tesla-electron\$teslafolderver\tesla-electron.exe"
			
		$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
		$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
		Write-Output "$TeslaElectronversionWeb is the version of Tesla Electron on the server" | Out-File -Append -FilePath $Log
		Write-Output "$TeslaProtonversionWeb is the version of Tesla Proton on the server" | Out-File -Append -FilePath $Log
		Write-Output "$TeslaProtonversionLocal is the version of Tesla Proton on the PC now" | Out-File -Append -FilePath $Log
		Write-Output "$TeslaElectronversionLocal is the version of Tesla Electron on the PC now" | Out-File -Append -FilePath $Log
		
		#Finding out if RPOS and FlowModule are AIR or EXE
		Invoke-Expression "Invoke-webrequest $server/RPOS.exe -DisableKeepAlive -UseBasicParsing -Method head" -ErrorVariable resultexeair
		$resultexeair | Out-File -FilePath $results
		
		#If statement for EXE or AIR files	
		If ((Get-Content $results) -eq $null)
		{
			$rfile = "RPOS.exe"
			$ffile = "FlowModule.exe"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		If ((Get-Content $results) -ne $null)
		{
			$rfile = "RPOS.air"
			$ffile = "FlowModule.air"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		
		#Creating values for the default RPOS folder and Default share folder	
		$RPOSfile = "$RPOSAUDIRHOME\$rfile"
		$flowfile = "$RPOSAUDIRHOME\$ffile"
		$ShareRPOS = "$RPOSAUDIRBUILD\$rfile"
		$ShareFlow = "$RPOSAUDIRBUILD\$ffile"
		
		#Starting to install RPOS
		Set-RPOSModuleInstallPOS
		
		$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
		$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
		if (($sir -gt "0") -or ($sif -gt "0") -or ($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($TeslaProtonversionWeb -ne $TeslaProtonversionLocal) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal))
		{
			Write-Output "Where was an error detected with the automatic installer - switching to manual mode and reinstalling" | Out-File -Append -FilePath $Log
			Set-RPOSModuleManualRemove
			Set-RPOSModuleInstallPOSnobackup
			Set-RPOSModulecheckinstall
			#Setting Checksum registry Key if passed
			{ Break }
		}
		Else
		{
			Write-Output "Version passed validation of successful install" | Out-File -Append -FilePath $Log
			Set-RPOSModulecheckinstall
			#Setting Checksum registry Key if passed
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value $checkswitchxml -Force
			{ Break }
		}
	}
	Else
	{
		Write-Output "There was a newer version of switchxml, however the site was not matching what was changed" | Out-File -Append -FilePath $Log
		#Setting Checksum registry Key if passed
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value $checkswitchxml -Force
		$script:RegSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	}
	
	#CheckRPOSIcon Checking Desktop Icon
	Set-RPOSModuleCheckRPOSIcon
}

#########################################################################################################################################
#                                                         * END OF MODULES *                                                            # 
#########################################################################################################################################
#########################################################################################################################################
# Function Get-RPOSChooseBuild
function Get-RPOSChooseBuild
{
	Set-RPOSVar

	#Testing to see if Roaming folder is in profile.
	$rposqaprofilefolder = Test-Path "C:\TBC_Scripts\RPOSAU"
	
	If (($rposqaprofilefolder -eq $false) -or (!$rposqaprofilefolder))
	{
		New-Item -Path "C:\TBC_Scripts" -ItemType Directory -Force
		New-Item -Path "C:\TBC_Scripts\RPOSAU" -ItemType Directory -Force
	}
	
	#Finding out if task is enabled or disabled in scheduled task
	$showtaska = schtasks /Query /TN "Update RPOS Local A" | Select-String -Pattern "Disabled"
	$showtaskb = schtasks /Query /TN "Update RPOS Local B" | Select-String -Pattern "Disabled"
	
	if ($showtaska -eq $null)
	{
		$task = "A"
	}
	
	if ($showtaskb -eq $null)
	{
		$task = "B"
	}
	
	New-Item "C:\Temp" -ItemType Directory -Force
	$OutputFile = "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
	
	$script:rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	$script:verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$script:verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	
	
	#------------------------------------------------------------------------
	# Source File Information (DO NOT MODIFY)
	# Source ID: 2ce2b477-eecd-40fe-94c4-f899a4cb0920
	# Source File: C:\Users\rolsen\Documents\SAPIEN\PowerShell Studio\Projects\RPOSButton\RPOSButton.psproj
	#------------------------------------------------------------------------
<#
    .NOTES
    --------------------------------------------------------------------------------
     Code generated by:  SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.141
     Generated on:       5/23/2018 12:14 PM
     Generated by:       rolse
    --------------------------------------------------------------------------------
    .DESCRIPTION
        Script generated by PowerShell Studio 2017
#>
	
	
	
	#region Source: Startup.pss
	#----------------------------------------------
	#region Import Assemblies
	#----------------------------------------------
	[void][Reflection.Assembly]::Load('System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
	[void][Reflection.Assembly]::Load('System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
	[void][Reflection.Assembly]::Load('System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a')
	[void][Reflection.Assembly]::Load('System.DirectoryServices, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a')
	#endregion Import Assemblies
	
	#Define a Param block to use custom parameters in the project
	#Param ($CustomParameter)
	
	function Main
	{
<#
    .SYNOPSIS
        The Main function starts the project application.
    
    .PARAMETER Commandline
        $Commandline contains the complete argument string passed to the script packager executable.
    
    .NOTES
        Use this function to initialize your script and to call GUI forms.
		
    .NOTES
        To get the console output in the Packager (Forms Engine) use: 
		$ConsoleOutput (Type: System.Collections.ArrayList)
#>
		Param ([String]$Commandline)
		
		#--------------------------------------------------------------------------
		#TODO: Add initialization script here (Load modules and check requirements)
		
		
		#--------------------------------------------------------------------------
		
		if ((Show-MainForm_psf) -eq 'OK')
		{
			
		}
		
		$script:ExitCode = 0 #Set the exit code for the Packager
	}
	
	
	
	
	
	
	
	#endregion Source: Startup.pss
	
	#region Source: Globals.ps1
	#--------------------------------------------
	# Declare Global Variables and Functions here
	#--------------------------------------------
	
	
	#Sample function that provides the location of the script
	function Get-ScriptDirectory
	{
	<#
		.SYNOPSIS
			Get-ScriptDirectory returns the proper location of the script.
	
		.OUTPUTS
			System.String
		
		.NOTES
			Returns the correct path within a packaged executable.
	#>
		[OutputType([string])]
		param ()
		if ($null -ne $hostinvocation)
		{
			Split-Path $hostinvocation.MyCommand.path
		}
		else
		{
			Split-Path $script:MyInvocation.MyCommand.Path
		}
	}
	
	#Sample variable that provides the location of the script
	[string]$ScriptDirectory = Get-ScriptDirectory
	
	
	
	#endregion Source: Globals.ps1
	
	#region Source: MainForm.psf
	function Show-MainForm_psf
	{
		#----------------------------------------------
		#region Import the Assemblies
		#----------------------------------------------
		[void][reflection.assembly]::Load('System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
		[void][reflection.assembly]::Load('System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
		[void][reflection.assembly]::Load('System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a')
		#endregion Import Assemblies
		
		#----------------------------------------------
		#region Generated Form Objects
		#----------------------------------------------
		[System.Windows.Forms.Application]::EnableVisualStyles()
		$formRPOSVersionSwitcherV = New-Object 'System.Windows.Forms.Form'
		$buttonRPOSDEVPILOT3 = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSDEVPILOT2 = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSDEVPILOT = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSDEV = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSALPHA = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSQAPILOT3 = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSQAPILOT2 = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSQAPILOT = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSQA = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSPilot3 = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSPilot2 = New-Object 'System.Windows.Forms.Button'
		$RPOSPilot = New-Object 'System.Windows.Forms.Button'
		$RPOSTRAINGING = New-Object 'System.Windows.Forms.Button'
		$buttonRPOSProduction = New-Object 'System.Windows.Forms.Button'
		$labelPleaseSelectYourVers = New-Object 'System.Windows.Forms.Label'
		$InitialFormWindowState = New-Object 'System.Windows.Forms.FormWindowState'
		#endregion Generated Form Objects
		
		#----------------------------------------------
		# User Generated Script
		#----------------------------------------------
		New-Item "C:\Temp" -ItemType Directory -Force
		$OutputFile = "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
		
		
		$formRPOSVersionSwitcherV_Load = {
			#TODO: Initialize Form Controls here
			
		}
		
		$textbox1_TextChanged = {
			#TODO: Place custom script here
			
		}
		
		$labelPleaseSelectYourVers_Click = {
			#TODO: Place custom script here
			
		}
		
		$buttonRPOSProduction_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				
				[System.Environment]::Exit(0)
					
					
			}
		}
		
		$RPOSTRAINGING_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-training.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}

				[System.Environment]::Exit(0)
			}
		}
		
		$RPOSPilot_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-pilot.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$RPOSPilot2_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-pilot2.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$RPOSPilot3_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-pilot3.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		$buttonRPOSQA_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-qa.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$buttonRPOSQAPILOT_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-qapilot.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$buttonRPOSQAPILOT2_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-qapilot2.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$buttonRPOSQAPILOT3_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-qapilot3.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$buttonRPOSALPHA_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-alpha.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		$buttonRPOSDEV_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-dev.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$buttonRPOSDEVPILOT_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-devpilot.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$buttonRPOSDEVPILOT2_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-devpilot2.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		
		$buttonRPOSDEVPILOT3_Click = {
			$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			if ($processcheck -ne $null)
			{
				[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
				[System.Environment]::Exit(0)
			}
			Else
			{
				#TODO: Place custom script here
				Write-Output "http://rpos-devpilot3.tbccorp.com" | Out-File -FilePath $OutputFile
				
				#Grabbing information from the menu selection for server
				$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
				$URL = "$server/update.xml"
				Invoke-WebRequest -Uri $URL -OutFile $XML
				
				#Getting version number 
				[xml]$xmlread = Get-Content $xml
				$script:x = $xmlread.update.versionNumber
				
				if (($x -ne $verRPOS) -or ($x -ne $verFLOW))
				{
					schtasks /Run /TN "Update RPOS Local $task"
					Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				}
				Else
				{
					Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * You have selected an version of RPOS that has already that Distro Installed, checking for updates for that distro."
					schtasks /Run /TN "Update RPOS Local $task"
				}
				
				[System.Environment]::Exit(0)
			}
		}
		# --End User Generated Script--
		#----------------------------------------------
		#region Generated Events
		#----------------------------------------------
		
		$Form_StateCorrection_Load =
		{
			#Correct the initial state of the form to prevent the .Net maximized form issue
			$formRPOSVersionSwitcherV.WindowState = $InitialFormWindowState
		}
		
		$Form_StoreValues_Closing =
		{
			#Store the control values
		}
		
		
		$Form_Cleanup_FormClosed =
		{
			#Remove all event handlers from the controls
			try
			{
				$buttonRPOSDEVPILOT3.remove_Click($buttonRPOSDEVPILOT3_Click)
				$buttonRPOSDEVPILOT2.remove_Click($buttonRPOSDEVPILOT2_Click)
				$buttonRPOSDEVPILOT.remove_Click($buttonRPOSDEVPILOT_Click)
				$buttonRPOSDEV.remove_Click($buttonRPOSDEV_Click)
				$buttonRPOSALPHA.remove_Click($buttonRPOSALPHA_Click)
				$buttonRPOSQAPILOT3.remove_Click($buttonRPOSQAPILOT3_Click)
				$buttonRPOSQAPILOT2.remove_Click($buttonRPOSQAPILOT2_Click)
				$buttonRPOSQAPILOT.remove_Click($buttonRPOSQAPILOT_Click)
				$buttonRPOSQA.remove_Click($buttonRPOSQA_Click)
				$buttonRPOSPilot3.remove_Click($RPOSPilot3_Click)
				$buttonRPOSPilot2.remove_Click($RPOSPilot2_Click)
				$RPOSPilot.remove_Click($RPOSPilot_Click)
				$RPOSTRAINGING.remove_Click($RPOSTRAINGING_Click)
				$buttonRPOSProduction.remove_Click($buttonRPOSProduction_Click)
				$labelPleaseSelectYourVers.remove_Click($labelPleaseSelectYourVers_Click)
				$formRPOSVersionSwitcherV.remove_Load($formRPOSVersionSwitcherV_Load)
				$formRPOSVersionSwitcherV.remove_Load($Form_StateCorrection_Load)
				$formRPOSVersionSwitcherV.remove_Closing($Form_StoreValues_Closing)
				$formRPOSVersionSwitcherV.remove_FormClosed($Form_Cleanup_FormClosed)
			}
			catch { Out-Null <# Prevent PSScriptAnalyzer warning #> }
		}
		#endregion Generated Events
		
		#----------------------------------------------
		#region Generated Form Code
		#----------------------------------------------
		$formRPOSVersionSwitcherV.SuspendLayout()
		#
		# formRPOSVersionSwitcherV
		#
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSDEVPILOT3)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSDEVPILOT2)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSDEVPILOT)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSDEV)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSALPHA)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSQAPILOT3)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSQAPILOT2)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSQAPILOT)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSQA)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSPilot3)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSPilot2)
		$formRPOSVersionSwitcherV.Controls.Add($RPOSPilot)
		$formRPOSVersionSwitcherV.Controls.Add($RPOSTRAINGING)
		$formRPOSVersionSwitcherV.Controls.Add($buttonRPOSProduction)
		$formRPOSVersionSwitcherV.Controls.Add($labelPleaseSelectYourVers)
		$formRPOSVersionSwitcherV.AutoScaleDimensions = '8, 17'
		$formRPOSVersionSwitcherV.AutoScaleMode = 'Font'
		$formRPOSVersionSwitcherV.BackColor = 'ControlLight'
		$formRPOSVersionSwitcherV.ClientSize = '727, 329'
		$formRPOSVersionSwitcherV.Name = 'formRPOSVersionSwitcherV'
		$formRPOSVersionSwitcherV.Text = 'RPOS Version Switcher v1.3 for Auto-Updater 3.8'
		$formRPOSVersionSwitcherV.add_Load($formRPOSVersionSwitcherV_Load)
		#
		# buttonRPOSDEVPILOT3
		#
		$buttonRPOSDEVPILOT3.BackColor = 'ActiveCaptionText'
		$buttonRPOSDEVPILOT3.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSDEVPILOT3.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSDEVPILOT3.ImageAlign = 'TopLeft'
		$buttonRPOSDEVPILOT3.Location = '513, 213'
		$buttonRPOSDEVPILOT3.Margin = '4, 4, 4, 4'
		$buttonRPOSDEVPILOT3.Name = 'buttonRPOSDEVPILOT3'
		$buttonRPOSDEVPILOT3.Size = '195, 46'
		$buttonRPOSDEVPILOT3.TabIndex = 15
		$buttonRPOSDEVPILOT3.Text = 'RPOS-DEVPILOT3'
		$buttonRPOSDEVPILOT3.UseVisualStyleBackColor = $False
		$buttonRPOSDEVPILOT3.add_Click($buttonRPOSDEVPILOT3_Click)
		#
		# buttonRPOSDEVPILOT2
		#
		$buttonRPOSDEVPILOT2.BackColor = 'ActiveCaptionText'
		$buttonRPOSDEVPILOT2.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSDEVPILOT2.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSDEVPILOT2.ImageAlign = 'TopLeft'
		$buttonRPOSDEVPILOT2.Location = '513, 152'
		$buttonRPOSDEVPILOT2.Margin = '4, 4, 4, 4'
		$buttonRPOSDEVPILOT2.Name = 'buttonRPOSDEVPILOT2'
		$buttonRPOSDEVPILOT2.Size = '196, 51'
		$buttonRPOSDEVPILOT2.TabIndex = 14
		$buttonRPOSDEVPILOT2.Text = 'RPOS-DEVPILOT2'
		$buttonRPOSDEVPILOT2.UseVisualStyleBackColor = $False
		$buttonRPOSDEVPILOT2.add_Click($buttonRPOSDEVPILOT2_Click)
		#
		# buttonRPOSDEVPILOT
		#
		$buttonRPOSDEVPILOT.BackColor = 'ActiveCaptionText'
		$buttonRPOSDEVPILOT.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSDEVPILOT.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSDEVPILOT.ImageAlign = 'TopLeft'
		$buttonRPOSDEVPILOT.Location = '513, 102'
		$buttonRPOSDEVPILOT.Margin = '4, 4, 4, 4'
		$buttonRPOSDEVPILOT.Name = 'buttonRPOSDEVPILOT'
		$buttonRPOSDEVPILOT.Size = '196, 42'
		$buttonRPOSDEVPILOT.TabIndex = 13
		$buttonRPOSDEVPILOT.Text = 'RPOS-DEVPILOT'
		$buttonRPOSDEVPILOT.UseVisualStyleBackColor = $False
		$buttonRPOSDEVPILOT.add_Click($buttonRPOSDEVPILOT_Click)
		#
		# buttonRPOSDEV
		#
		$buttonRPOSDEV.BackColor = 'ActiveCaptionText'
		$buttonRPOSDEV.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSDEV.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSDEV.ImageAlign = 'TopLeft'
		$buttonRPOSDEV.Location = '511, 43'
		$buttonRPOSDEV.Margin = '4, 4, 4, 4'
		$buttonRPOSDEV.Name = 'buttonRPOSDEV'
		$buttonRPOSDEV.Size = '199, 47'
		$buttonRPOSDEV.TabIndex = 12
		$buttonRPOSDEV.Text = 'RPOS-DEV'
		$buttonRPOSDEV.UseVisualStyleBackColor = $False
		$buttonRPOSDEV.add_Click($buttonRPOSDEV_Click)
		#
		# buttonRPOSALPHA
		#
		$buttonRPOSALPHA.BackColor = 'ActiveCaptionText'
		$buttonRPOSALPHA.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSALPHA.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSALPHA.ImageAlign = 'TopLeft'
		$buttonRPOSALPHA.Location = '278, 269'
		$buttonRPOSALPHA.Margin = '4, 4, 4, 4'
		$buttonRPOSALPHA.Name = 'buttonRPOSALPHA'
		$buttonRPOSALPHA.Size = '214, 48'
		$buttonRPOSALPHA.TabIndex = 11
		$buttonRPOSALPHA.Text = 'RPOS-ALPHA'
		$buttonRPOSALPHA.UseVisualStyleBackColor = $False
		$buttonRPOSALPHA.add_Click($buttonRPOSALPHA_Click)
		#
		# buttonRPOSQAPILOT3
		#
		$buttonRPOSQAPILOT3.BackColor = 'ActiveCaptionText'
		$buttonRPOSQAPILOT3.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSQAPILOT3.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSQAPILOT3.ImageAlign = 'TopLeft'
		$buttonRPOSQAPILOT3.Location = '276, 211'
		$buttonRPOSQAPILOT3.Margin = '4, 4, 4, 4'
		$buttonRPOSQAPILOT3.Name = 'buttonRPOSQAPILOT3'
		$buttonRPOSQAPILOT3.Size = '217, 49'
		$buttonRPOSQAPILOT3.TabIndex = 10
		$buttonRPOSQAPILOT3.Text = 'RPOS-QAPILOT3'
		$buttonRPOSQAPILOT3.UseVisualStyleBackColor = $False
		$buttonRPOSQAPILOT3.add_Click($buttonRPOSQAPILOT3_Click)
		#
		# buttonRPOSQAPILOT2
		#
		$buttonRPOSQAPILOT2.BackColor = 'ActiveCaptionText'
		$buttonRPOSQAPILOT2.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSQAPILOT2.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSQAPILOT2.ImageAlign = 'TopLeft'
		$buttonRPOSQAPILOT2.Location = '276, 153'
		$buttonRPOSQAPILOT2.Margin = '4, 4, 4, 4'
		$buttonRPOSQAPILOT2.Name = 'buttonRPOSQAPILOT2'
		$buttonRPOSQAPILOT2.Size = '218, 50'
		$buttonRPOSQAPILOT2.TabIndex = 9
		$buttonRPOSQAPILOT2.Text = 'RPOS-QAPILOT2'
		$buttonRPOSQAPILOT2.UseVisualStyleBackColor = $False
		$buttonRPOSQAPILOT2.add_Click($buttonRPOSQAPILOT2_Click)
		#
		# buttonRPOSQAPILOT
		#
		$buttonRPOSQAPILOT.BackColor = 'ActiveCaptionText'
		$buttonRPOSQAPILOT.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSQAPILOT.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSQAPILOT.ImageAlign = 'TopLeft'
		$buttonRPOSQAPILOT.Location = '275, 101'
		$buttonRPOSQAPILOT.Margin = '4, 4, 4, 4'
		$buttonRPOSQAPILOT.Name = 'buttonRPOSQAPILOT'
		$buttonRPOSQAPILOT.Size = '220, 44'
		$buttonRPOSQAPILOT.TabIndex = 8
		$buttonRPOSQAPILOT.Text = 'RPOS-QAPILOT'
		$buttonRPOSQAPILOT.UseVisualStyleBackColor = $False
		$buttonRPOSQAPILOT.add_Click($buttonRPOSQAPILOT_Click)
		#
		# buttonRPOSQA
		#
		$buttonRPOSQA.BackColor = 'ActiveCaptionText'
		$buttonRPOSQA.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSQA.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSQA.ImageAlign = 'TopLeft'
		$buttonRPOSQA.Location = '275, 44'
		$buttonRPOSQA.Margin = '4, 4, 4, 4'
		$buttonRPOSQA.Name = 'buttonRPOSQA'
		$buttonRPOSQA.Size = '221, 47'
		$buttonRPOSQA.TabIndex = 7
		$buttonRPOSQA.Text = 'RPOS-QA'
		$buttonRPOSQA.UseVisualStyleBackColor = $False
		$buttonRPOSQA.add_Click($buttonRPOSQA_Click)
		#
		# buttonRPOSPilot3
		#
		$buttonRPOSPilot3.BackColor = 'ActiveCaptionText'
		$buttonRPOSPilot3.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSPilot3.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSPilot3.ImageAlign = 'TopLeft'
		$buttonRPOSPilot3.Location = '21, 269'
		$buttonRPOSPilot3.Margin = '4, 4, 4, 4'
		$buttonRPOSPilot3.Name = 'buttonRPOSPilot3'
		$buttonRPOSPilot3.Size = '235, 49'
		$buttonRPOSPilot3.TabIndex = 6
		$buttonRPOSPilot3.Text = 'RPOS-Pilot 3'
		$buttonRPOSPilot3.UseVisualStyleBackColor = $False
		$buttonRPOSPilot3.add_Click($RPOSPilot3_Click)
		#
		# buttonRPOSPilot2
		#
		$buttonRPOSPilot2.BackColor = 'ActiveCaptionText'
		$buttonRPOSPilot2.ForeColor = 'ButtonFace'
		#region Binary Data
		$buttonRPOSPilot2.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSPilot2.ImageAlign = 'TopLeft'
		$buttonRPOSPilot2.Location = '20, 211'
		$buttonRPOSPilot2.Margin = '4, 4, 4, 4'
		$buttonRPOSPilot2.Name = 'buttonRPOSPilot2'
		$buttonRPOSPilot2.Size = '236, 50'
		$buttonRPOSPilot2.TabIndex = 5
		$buttonRPOSPilot2.Text = 'RPOS-Pilot 2'
		$buttonRPOSPilot2.UseVisualStyleBackColor = $False
		$buttonRPOSPilot2.add_Click($RPOSPilot2_Click)
		#
		# RPOSPilot
		#
		$RPOSPilot.BackColor = 'ActiveCaptionText'
		$RPOSPilot.ForeColor = 'ButtonFace'
		#region Binary Data
		$RPOSPilot.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$RPOSPilot.ImageAlign = 'TopLeft'
		$RPOSPilot.Location = '20, 154'
		$RPOSPilot.Margin = '4, 4, 4, 4'
		$RPOSPilot.Name = 'RPOSPilot'
		$RPOSPilot.Size = '236, 49'
		$RPOSPilot.TabIndex = 4
		$RPOSPilot.Text = 'RPOS-Pilot'
		$RPOSPilot.UseVisualStyleBackColor = $False
		$RPOSPilot.add_Click($RPOSPilot_Click)
		#
		# RPOSTRAINGING
		#
		$RPOSTRAINGING.BackColor = 'ActiveCaptionText'
		$RPOSTRAINGING.BackgroundImageLayout = 'None'
		$RPOSTRAINGING.ForeColor = 'ButtonFace'
		#region Binary Data
		$RPOSTRAINGING.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$RPOSTRAINGING.ImageAlign = 'TopLeft'
		$RPOSTRAINGING.Location = '20, 100'
		$RPOSTRAINGING.Margin = '4, 4, 4, 4'
		$RPOSTRAINGING.Name = 'RPOSTRAINGING'
		$RPOSTRAINGING.Size = '236, 46'
		$RPOSTRAINGING.TabIndex = 3
		$RPOSTRAINGING.Text = 'RPOS-Training'
		$RPOSTRAINGING.UseVisualStyleBackColor = $False
		$RPOSTRAINGING.add_Click($RPOSTRAINGING_Click)
		#
		# buttonRPOSProduction
		#
		$buttonRPOSProduction.BackColor = 'ActiveCaptionText'
		$buttonRPOSProduction.ForeColor = 'ControlLightLight'
		#region Binary Data
		$buttonRPOSProduction.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAABm+SURBVHhe1VsJmE7l355imO19Z9/3MbuxFdEM
ZYtGyF4IY0mkQmTNEmHsQlFZsg+pUJbsxEgYxaRNhX9K/0wLg9l/330/7/vMnJl5Z6ov1fed67qv
c+asv/v+bc9zzjt2f+dy4MABh/feey9i9+7drbdv3z5h8+bNr7/55ptvY/3BW2+99dnGjRsvrlmz
5gLw8euvv77n5ZdfXrd06dIF2B64bNmy+jjuLSJ3Wm/3/2M5efKk0/vvv19306ZNw9atWwdua77a
sGHDzyCTD/KC/QIBZMuWLWqdlpYma9eulZUrVwoEkPnz58uLL754c/z48d8DB4cNGzb3+eef77Rk
yZJAiFHF+pj/ewu87bd169YUEHxr9erVV0A8f/369YogiRrJc5vgcZLHufLGG28IvF8swrRp02Ts
2LEyZMgQ6du3bzZwesCAAbOffvrpRIjlYH3sv78cOXIkAOE9BMSOg0iOJrRixQpZsGCBTJ8+XSZN
miRTpkyRGTNmKHIkyuMkzTW8K3PnzpWpU6cKvC4jRoyQ4cOHKwwdOlSefPJJ6dOnj3Tt2rWobdu2
P3bs2HHlE0880fRfFQIed9m1a1cPePUYPJ6LMFdk6Dka3KNHD+nVq5cMHjxYeXL27NmC3FYRsW3b
NkFdkL179wruIYgcFQ0UZubMmTJmzBgBQenevbu0b99eAaSlW7duav3AAw9Is2bNfkpOTl6akpJS
C6lxh9Wsf2ZBjie8/fbbq0DmOj1Ow2l0p06dpGnTptKmTRslAMN3woQJMm/ePCUOw5+EDx8+LB9+
+KEcP35crQ8dOiQ7duwQ1Al59dVXJTU1VUaPHi2DBg2SRx55RFq1aiWNGjWSBg0aSOPGjaVly5YU
QJKSkoqAz1q3bj0IortYzfv7FhQ4e1T1rsjfM/QkipwK7ebNm0utWrXk3nvvFYSoPPbYY8p4kmDY
M8R5Lj29b98+OXr0qHz00Udy4sQJJQLSSEUEOoMqhhRs4sSJgnyX3r17K6+3aNFC6tevLzExMVKj
Rg2pW7euJCYmKlHq1KmTjWcvRZqEWU29/QuMNCF0x8BLWfTkK6+8Il26dJHIyEiJjY1V5BGS8uij
j0r//v2V8Qx9CsDCxrqAqFERgPRRpCkEo4GioI6oCED7kzlz5qhawHsw9zt37iwPPvig8j6JUwA/
Pz8JDg6WhIQEqVevntSsWbMI24cQLYlWk2/fkp6e7vHuu+/ORZ7eYK6yWFF5f39/iY+PV55gmHbo
0EHlLQVg+I8cOVJeeOEFVeBee+01FQUUD/dShHfu3KnWbIkkT+8vXLhQ3f+5555TUcRUevjhh1Xe
UwBGAchKeHi4eHp6iru7u9pmBEZHR9MhmXBG69tWF/bs2eMJo5fAg7nMYxa1sLAw5QGqT8+TPPOe
hjIqevbsKY8//rjy4KhRo1Sa0KuMBHp41apVSgwWTj0GYB1h6GMMoFLH2v7U/RhZzHnWgbvvvltq
164tUVFREhgYKG5ubuLk5CQ+Pj5qH6MjNDT0PKKi3V8WAQXKDEPnw+A8GslwJHFfX1+Ji4uThg0b
qvxHEVIhShFYsdGuVAdA35annnpKtTWGNMmxG7AV0tOLFi2Sl156SYnDdsmCyahhFyF5FkDWFD6D
UUbvM9y1t0FUvL29xcXFRapXr66igc5haiA6v0S6tLRS+fNLZmZmNRStCSB/k+2LociQI3nmPI25
7777VHFieLIyMxIoBI1m3vIaEmFLYzRQCHoXIzs1NmChI2nWChLnOYwcikcReR/e30ie3mfaUQCS
pT2MAkdHR7G3txez2awig46CvR/fhcVK6c8tKHg9OITlYIU57eXlpdRm0aMhzEcKcP/996vwJGgs
QTEYDawJJMKugH6tyDGvGd4ky+jgNtOK0cKKzyLK6xhVvCdTjORZ/DR53QkYAaxD9Lyzs7OKgqpV
q6qIsAogHh4e7zVp0sTfSuuPLcj7esj5c8uXL1ejMT4EN1KKswCxAKL/Cm6shDCKwXGAFoQRwfzV
tYEhzSLJGkEvExSH+ygUiVM4Csh7M8WY83Xr1lFhrz1P8ix8DHUSpW0mk0kcHByUAHfeeaf6m05D
RBSgPqRC8OpWepUv6PWu8Px6FisWLz7Q1dVVQkJClPL0BD1C4xia9BBBQTQoiBaIgjCHdXqQIEO7
Xbt2WFvA/Xpww2stxa4++7siTtGZdpq8zvOAgACVAloAHQEUoEqVKmqfNTp+wjXtrRQrX9Cr+6Hg
3WCBIgHmFCOAoc+qT6OQVkoE4p577lFiJCTUhkjhEhRE40oQEkKEI1wjYDiNp/ciFcLCCIZyhDqH
5wcGhoAYPRsEBIJgACq8P9LPDx71RVj7gJQX8t5Twd3d0goZ9owA1gEKcMcddygxaD+FwPogUjfA
StP2giFuOMb1J9mzmZP0PPOeirPq0xPMQ4rASKAQDNHIyGgY4wUD4IXbDnMJHEvDAXBycgVBV5sC
gJJUq1ZNH8sHl+cqbI08gNAfDfIFHL0x3Fhdg4KC1Dbzj9AiaNDDLi7uyhgPIMbJJHFA7N+MaMCt
WAQTuoCTSgEKoMkT3KYwVpxFKkUrwmUXjMrCUPFPL168WE1qGDrML+YcBWD+Mw8JLQZDlR6gEb5A
cxjyhMlNpnn4Sqq7n8yoBKkegJcNeAI4ZusaI3hefzzHy/p8Bwdn5W3mvlEAgvus4uQjXUaRb7kF
o73BqPp5HIaywtL7bDMcXTH/uSYoBAVhTjriwUQwkAzyI8wesg3p8mWHGnJxQLhc6F8eF7nuGy7n
mkZKRlS0nAyNkZNhgHX9ce1oOd+5hlweHiaXR9jGxafD5OhdEXIsNFqGeAeICwUAqlVzVOEPOuXA
yCAgxAkU1iDsK1kwOXHDMPd9DkfZkuh9tpeIiAhFlgIwEjRYlEjcCYhyMktHZ5M8jxqwP66GXBkd
JwX7YqQoPVqKjlaMvB0xkjU7Tr7uEi9n68XKZ83jQDperq+Jl4KDsSInYipEEfDpzEg5EBcph0Oj
pCMigfY4OJpQ+KrZFIDCQIAiFMabGB/0xL6SBdPbJhinZ/FlBPOa3mfbI1k9vqYYXLMgqoKHB0aC
/CPOZpkBAz5sECs/L7xHCo8nStEp4OTvgOdkJEre4Xvl09Q68tu2RtiXJEWnAawLKwGP5+E5J0bV
knRE0c6QSElytdjk4OBSYRQgFYqYDiiKazCsL3mbhLY3gW2PozNWfU4sSFST1wIwJUJCQnEDTEDw
sEQIMMsT5MOi5cpkGJbZVgrPPCSFn1hxpq0UnbWgeJ8B3P/du81kYbtw+flAK/zdTl1TCtin9pc5
VpTZTn4+1FrWNa4hx0OiZJRPkIqC6hCgSpXShVADwhRRHIjwVXExxKjPFd7fPmvWLDVIYevjAMNI
nDVBIyAgUJyc0XrwsGR4f5N/qJytGS+/roOBXzwmBeeAzyzIPdtDfjrcWbKOdJH8T3sW79fgvg1P
1pOFUcFybVdnXN+r9Dm4V/bJR+WX9K5yI6M79pUcL/y8F+7dRSbWDJQ9QRHS3ytAqlGA6k6qFlSt
Wr2sACRPFADXMXjqgn12dmlpaXUwI7vI2Rj7OwcVDH8SJxgJBL3PtY+Ppfi5A93Rf3fj4ZkNasuN
QymS//UQyT//pELhN0Pky63dZHLtYNnU/W65cWagFFiP6eNfbOkmE6JAICJSspZ3kYJvn8Ix3mOI
FOBe2WcHysp+d8u0huHy0VJ43nC88Jun5NzmrjI+2E/2BNeQZHcfJQDJUwSOSe68s0opARAVSgCu
kQaz1JgAfb87pqjXOG/nLIopQLLa8xSDfxMcfnp7+6pcCwAGu3rIkeBI+aJdotzKHCH5l8ZK3sUx
CoXfjZP0Zd1krl+AfDU8WQrwd+Hl8QpF34+XW1+PkqWPNZBXUMVPoZpffrGj5F4aJznfjFHIuzhW
fjo1VMbHh8jW2Gj5Zf+TUvTDBFz/vIL8OEEOLu4k01Gsd6EGJJg81eDI3t4BAjgrJ1EMOztLKpC0
FRSgEGOCrRjEudoh92dhnl7EkR/H1Dr/y3qf5DkocgVpPigKbe95D2/5EAJ8O7gNjH9R8n6YAUxX
yMf21U8nyCcTO8snfVvKjrFtZNvYZNlCjEuWtYObyqTgADkE40+j/X3aqpHsfKqlrOzfRN7o11jW
Pd1cNo55UMYE+suh+DjZNbyVbB77oGzGPuIt3GNaqwRZ6O0vmxEBgc5uauRob19deV/VA0QCyNoS
IB+R8Dk4Rtkh9zex93PGxvznuF8TZwRwrcmzNpjR6xkBdSHAAjw8A977bmYfyfvlZcm9uqgU8rIW
S96PC+WHd0bIweQkeccvVNK8gyTNJ1jSfIPlfaRPRniMAqNgp3+YpHkFydt3JcgHs3rJpomdZCo8
fAytbiuuXYY8X+4VaEWArPAJlL0gvyggTDwwINICOKIdUgBGg04BqxBagDwUwu8hQJJdamrqEb6Y
4GxMF0B63Bj6JM/08PX1UyM/Z9z8PhTAlb5B8kl0vFxZN1Lybq6R3GurJOe30uC+3OzVkv3FIrk0
sY+crVNXPgZh4rSVvAIHQTE15esBD8tvx2ZKwY01snHyIzIXLZbHea6+zogzEbEyBnZYPO6sSHOb
MI4JQLrIIEAuBMgCp652GPefHzdunJrSsgCSqBaAxPW0k5Hh4+OrBDDj5u1dXGVLYJicrhErGSlt
5cKxVLmVDRFurpOc7LXlwf2/rpJLq4bLkdh4RdhI/nRMvPxn9uNy88pyyb21Xq7//IYs7t1ClqNG
kGjxuTbQx8vfWgAtHcAigAntrmqxAFbyRD4FAK5hmjzEbvLkyVnPPvts8QCIhEneGPYkT3DqyZt7
Ar0x5t8fUkN55gTC91CjuyRjQV+5enEuRHgVpJeWQ96t12TPsgHycmCwnMLYoZgEvV+zllzdORrk
l0rujSW4zxyZ0qSmvIM2WypSDOD+o7hPCzdvVZd0B6CNFXSBQity8PcNzB3G240dO/bWM888o6a7
jADtdUYCyeuXoJZ3b5b859j/GRTDdORmsXEQIb1GjBzudp98ueNpyf5lJojMkZzrsxVys+dI1n+m
yqzkurIRNYBePR1hvZbRgFD+cd69aG8tpPDrFvJTehOZGOcr+4MsIhuJa3D/HtgQa7IUZoY/R4IU
gOkAosUCYFsLwBSgALcwL5hqN3LkyFt8C0sB2AVIWgtAr2sBvLy8Me9GpcXNYxFeUzx95AS9iEnM
qRALMoIRDYHRcjChphyf0E6unBslt65Pg/fRGW7OkMPreskUDKQymt4jZwa1kvT4WPnm0VD5onWE
nMK1l4cGYKzvInLSRS6muciMCE9JD8YzQmKLn1H8LEYNBNgYHCH+NjqAsQXaECAX+3KUAMOGDcvi
i0q+emIKUADtfQpA8myNHB84YwTICLgbHWAxczMyRr7pHio/jA6QH0YFyvfAD6MC5PIzgXKqXqQc
7VhfrmW0lsKLyXL90wfltZR42Z0SIDn77pVDmDes5ghwqY/kbneXb3uFyIWUECk86iqSYZYP55hk
XoCvZDaJkEuPB8ulgSW4MAAplBiu2ud8pIi76gAuFXaACgS4gYnRWLuhQ4d+RQH4doddgB7X5LlN
8oSnpxcKoFlNPZujA6z1C5azCdFyfbWXyMcmZbQCtvN2e0jmPVFyDsbn7cE+ePT6Xhc5v9pN8tNJ
0EXeHmSWtJggyd3ioa4rOOQm11Z4S+ERiwDbhptkiWegXIGocsIsRcctkI/McuuQWVa38ZADQZEl
c4BKOoBVAK6LiyDGAdcwGhxsN2LEiA+YAnwRyWkwyRrJW8Kf7+A8xBECuOLmndEB3g3AnL5hpOTu
dAdBGAYjFU6ZJXuDt2Ti2IU+Vo/q4zjG7dyjZnm9rZvsSwqWgv1uluMaOK8QRNP6muWt2GD5dZGv
RVjD/X/ZbZbpdXA9BOjp6VeuA1gKYKkOoEVgBCgB0AavIqo72aEDbBw4cKB6ra0FMBY+hj7frzP/
Gf4+QD+zmxzEw89jFkfPFRO0gvtubPRUoU2PGY/x3N/2mmXuXR5ypnuwFDEijMcBevq/W0A0zcNy
f+NxCPDtBkzBw70wB4iU+60dwN6eHcAyBGYhvOOO0lNikKb3CT0QuhwZGdnIbvDgwTP69evHX16o
LkBv67zntvXjgnrxSAHCkWPPuXvJMRSnS8hFFZZGAw2GlhVGAd689KZJZof7yLfDEd48r+w5BK+1
dY/TZjk6C6PQAMscIPoPdAArjAJwKJyJdh9uhxb4aEpKyjV+yiJpikCva/IE9+kCyJeR07181Sus
z1tGSM425HBFJMoC513fb5Y3eptkoa+/nO8aJvk6BWydXxa4/qf3zLIg2SQrMPpLQwfwq7ADlCJf
VoBCdIDNiYmJJrbB2hDgAr/jcezPNNDENXk3NwrAV05m9SZ2HOYAanSGKvxtzxDJPwAS2lsVwWr8
ko4mmQhxOYVlF7k8LFCKjiEN/sD1F9LMknq/WaahBe/BIGywfh9Y3AF0ASz3LqCcABgFvoh9dnaT
Jk0y9+nT512+C+Q7fr5DZ8iXkHcD+O6dr58RXnhALYTdJqivBjMg8d2zgfLzdrNc3WECuC6NrJ1m
+XKVWWZajT/IGaBVwE/iouXKVD/JsnGdxq/oJBmLTDKhjlnme/rLDoiXguEv2x+dwgJo7AB8I2Ql
XQwredUGEf6/gl877LcsPXv2HM/vdvzKq0lTBK7ZGhkVXLMO8AEUoRmKz57QSCXCiagoWRPnLwti
vOUlA/h3apSnTAcmh3qqsQNndko4DYhwPCZSVsT5qfOJmTh/mhXToz1lQT1PmRruKcvR8raAfDtM
kDghKyHP8Lfkv40hsAYnQ4VcowCei4qKisC2ZUEEJLVv3/7qQw89pEaBjAKL590UeeunJbV2drbk
GdEDLegACKVjRHg4JEoOBANca+DvvajUrNZcf2A9l+P3UuBcQl9vuKYY6Dj7EDVrMX1OcvVWxC3k
+Umcxa/EJksHKFcAlQCMAm47Ojou79q1azVsW5bRo0e7ogvs4idpvhbjp2btdU2conA/wS8xNMAV
Ici3sa3cfOQBgOvKwHMqg61rNHg8CqlnIW9S5C19v4Q8wcmQcQhcBhQhG7w6Y7v00qFDh0EtW7bM
4wdR5j+JWsK+hDx/ilICy4OZDmog8g+A5EmYJPWszwhGgq38t0J5HzPADxISEnyxXXpBFwht1qxZ
Br/x8+sPSZYlz19hEFoER0fmnUmJwQJJsFtouGDEWB5uChxYEZa/y5+n76HvaxGcvwFwVmFenjxf
h5cf/RkB7+chrZ/Btu0FETCycePGBRwWcxxAskbPlxXAycmSDiVGWlAiAomUJadJG1FyXF9TXgAS
tYhQljwHPraGvmWB6n8yNDS04t8StmnTJhQpcIJvh3QUaOL6C6tRAEt0WAQoa9Q/BUZE1aq2P4QY
geM5qGdDsF35kpSU1LtBgwbZHBNwTqCJ8+uqUQBGBgWwFMnSnuJrs8pQ4uXSsHWuBSX3Lku+WrWS
X4VUJoK9vf326OhoL2xXvqAdmurVq7eWP4TiJ3AWQpLXAmgRjAKwXXL84O7ugfGDF9LHG8NpTqM5
ofKDkHylFqCm2fyyRAQG8kWrBXofz+G5BK/j9bwXp+IeHvxFiDvsYe1gejAV2AmqkRw/d1UoAMS5
jHHNH//JXMOGDWujUmbyJQmHx/pBWggdBRSAApE8OwfnEJxIcTpNsvr1Gl+y6tfsBL85GKH3628Q
Zd9J6qk56xKfZelOLsoG2sMIqEgA7MvFeeNQ3Kvi7z++1KxZ85HY2Ngs/iCCRvBBFIHQUVBWABpJ
8nyjROP1q3VNmt8ajb81MEJ/gteCGN9MaxEoMEeofCafrR1TWQrgnPW4hwe2/9wycOBAexg1BrhB
w0hOh5sWgWnAVskUoAD0vtHz9CgJadL6VyZ8/2gE9+kfXvA8LQSv1yJQWKMAfDZtqCz8IcoHsCsG
2/+7BTXABURmwYgceoQhqBU3isA6wCiggTRUe1+TJzGS1L8xQnqVAvfxGMWgEFoERoIWgBHA51Ns
iv573sexUzinPnn8pQXE3eD9l+DZHBpCT9sSgV6hdxgFTAGjACRFgiTLukLoH1npv7UQWgSjAIwq
RiDvT7F17tP7tsjDttNwRBPaf1sWkHOD+jNgRDY9QW/r4kMjjJGgU4EiMGooAiOAxEjQSN4oAgWg
SIwURoAmz/vYIl+R55EOxxAhDWn3bV1AyhlCDEMI/pfGMBRpDAUgaBTDkuGp04GeYy0gGR0JFEJH
Q9nwp1D0vP4gq/Oe99NhX5HnsZ2HY+/Atnhl8N+xYApZBYY8hAr8EcQookfoeRqko4EG6migUMa6
oDsCSVIQDWO40+PGgqefoYmz4JUlj7+v4vgUXPf7A53bsUCEKHh/KfALjdOe0SLo2sAI0RHBAsZQ
JjmC0aHBv5k2JM0U0oWO1/M+ZYudJo91AQQ5iud3QBSVzO//ocUBD34YBu4F+Zs0UqNsRFAgCqVH
jiyYBIlyzX0Ej5O0MdRteRzbhdh/Hs+aBHGDlTX/1sKwQ6j2htF7YfB1EqfRZUEyBAUiOYLe1dtG
4Xg+Sdsgno9jn0PMaYimWtj350Z3f+eCHPeACB2BZSDxFYzP0cZzraGJ2YLxPF5nvbYQx34E3sd9
hyM1Yv70sPafXJiLECMS4f0YPDsPnk2H8RfhuRsgw1fSfC1VTJDg39Z9fGnJHy/8F9ecxbXrQfpZ
hHkSUsUNx/7Z/xD9qws9heLmgwJ4F9AfhCYiz19ClGzA4f1AOnAUhLdBoGXAdPw9HOclQ7wwpJcT
/v4bFzu7/wEA4/QPXleRpwAAAABJRU5ErkJggg==')
		#endregion
		$buttonRPOSProduction.ImageAlign = 'TopLeft'
		$buttonRPOSProduction.Location = '21, 43'
		$buttonRPOSProduction.Margin = '4, 4, 4, 4'
		$buttonRPOSProduction.Name = 'buttonRPOSProduction'
		$buttonRPOSProduction.Size = '237, 49'
		$buttonRPOSProduction.TabIndex = 1
		$buttonRPOSProduction.Text = 'RPOS-Production'
		$buttonRPOSProduction.TextImageRelation = 'ImageBeforeText'
		$buttonRPOSProduction.UseVisualStyleBackColor = $False
		$buttonRPOSProduction.add_Click($buttonRPOSProduction_Click)
		#
		# labelPleaseSelectYourVers
		#
		$labelPleaseSelectYourVers.AutoSize = $True
		$labelPleaseSelectYourVers.Location = '16, 10'
		$labelPleaseSelectYourVers.Margin = '4, 0, 4, 0'
		$labelPleaseSelectYourVers.Name = 'labelPleaseSelectYourVers'
		$labelPleaseSelectYourVers.Size = '242, 17'
		$labelPleaseSelectYourVers.TabIndex = 0
		$labelPleaseSelectYourVers.Text = 'Please Select Your Version Of RPOS'
		$labelPleaseSelectYourVers.add_Click($labelPleaseSelectYourVers_Click)
		$formRPOSVersionSwitcherV.ResumeLayout()
		#endregion Generated Form Code
		
		#----------------------------------------------
		
		#Save the initial state of the form
		$InitialFormWindowState = $formRPOSVersionSwitcherV.WindowState
		#Init the OnLoad event to correct the initial state of the form
		$formRPOSVersionSwitcherV.add_Load($Form_StateCorrection_Load)
		#Clean up the control events
		$formRPOSVersionSwitcherV.add_FormClosed($Form_Cleanup_FormClosed)
		#Store the control values when form is closing
		$formRPOSVersionSwitcherV.add_Closing($Form_StoreValues_Closing)
		#Show the Form
		return $formRPOSVersionSwitcherV.ShowDialog()
		
	}
	#endregion Source: MainForm.psf
	
	#Start the application
	Main ($CommandLine)
	
	
	
}

###############################################################################################################
# Function RPOS-UpdatePOS
function Get-RPOSUpdateRPOS
{
	#Setting up varibles from above
	Set-RPOSVar
	
	##########################################################################################################################################
	
	#Setting All Registry Keys for Status Window Back To Zero
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRNI" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRInstalling" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUGatherReportingData" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingTesla" -Value "0" -Force
	
	#Testing to see if Roaming folder is in profile.
	$rposqaprofilefolder = Test-Path "C:\TBC_Scripts\RPOSAU"
	
	If (($rposqaprofilefolder -eq $false) -or (!$rposqaprofilefolder)) {
		New-Item -Path "C:\TBC_Scripts" -ItemType Directory -Force
		New-Item -Path "C:\TBC_Scripts\RPOSAU" -ItemType Directory -Force
	}
	
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d" | Out-File -Append -FilePath $Log
	Write-Output "RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
	
	Write-Output "Checking internet connection, please wait......" | Out-File -Append -FilePath $Log
	
	If (($networkcheck -eq $null) -or ($networkcheck -ne $true))
	{
		Write-Output "There is currnetly no internet connection RPOS Auto-Updater will check again for an update another time" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "2" -Force
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N /W=$InventoryRunTime
		Write-Output "Waiting $InventoryRunTime until starting the LANDesk Inventory Scan" | Out-File -Append -FilePath $Log
		
		Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:60 * RPOS Auto-Updater $ver has discovered there is no internet connection back to TBC Corporation. Please connect to VPN and/or connect your PC into the corporate network through WiFi or Direct Cable Connection and try again"
		#Getting time of end of script for log file
		$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
		Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname powershell* | Out-File -Append -Filepath $Log
	}
	Else
	{
		
		Write-Output "There is currently an active and running internet connection on this PC proceeding with setup" | Out-File -Append -FilePath $Log
		
		if ($dateofdelayhour -ge "00" -and $dateofdelayhour -le "06")
		{
			#Starting Random Delay Up To One Hour
			Write-Output "Starting delay of $StartTimeDelay seconds before running RPOS Auto-Updater due to after hours" | Out-File -Append -FilePath $Log
			Start-Sleep -Seconds $StartTimeDelay
		}
		
		Write-Output "Starting Auto-Updater during normal business hours without the delay" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "1" -Force
		
		
		#setting up registry search
		New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
		
		#Grabbing version from RPOS and FLOW Registry
		$script:rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
		$script:verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$script:verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		
		#Grabbing files names in RPOS directory
		$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
		$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
		
		#Checking to see what PC name is assoicated with a Store, TAC, Carroll Tire, Or Corprate Owned
		Set-RPOSModuleWhatPCAmI
				
		#Preventing install of BGInfo and EMV on Carroll Tire, TAC, Personal PC - Checking to see if EMV is installed and BGinfo is up to date.
		if ($name -match "S[0-9][0-9][0-9]")
		{
			
			Write-Output "Checking for EMV install and current BGInfo" | Out-File -Append -FilePath $Log
			Set-RPOSModuleCheckBGInfoUpdateandEMV
		}
		

		#Grabbing information from the menu selection for server
		$server = Get-Content "C:\TBC_Scripts\RPOSAU\switchbuild.txt"
		$URL = "$server/update.xml"
		Invoke-WebRequest -Uri $URL -OutFile $XML
		
		#Getting version number 
		[xml]$xmlread = Get-Content $xml
		$script:x = $xmlread.update.versionNumber
		Write-Output "$x is the version pulled on $server" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is the version of RPOS pulled on PC" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is the version of Flow pulled on PC" | Out-File -Append -FilePath $Log
		
		#Finding out if RPOS and FlowModule are AIR or EXE
		Invoke-Expression "Invoke-webrequest $server/RPOS.exe -DisableKeepAlive -UseBasicParsing -Method head" -ErrorVariable resultexeair
		$resultexeair | Out-File -FilePath $results
		
		#If statement for EXE or AIR files	
		If ((Get-Content $results) -eq $null)
		{
			$rfile = "RPOS.exe"
			$ffile = "FlowModule.exe"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		If ((Get-Content $results) -ne $null)
		{
			$rfile = "RPOS.air"
			$ffile = "FlowModule.air"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		
		#Creating values for the default RPOS folder and Default share folder	
		$RPOSfile = "$RPOSAUDIRHOME\$rfile"
		$flowfile = "$RPOSAUDIRHOME\$ffile"
		$ShareRPOS = "$RPOSAUDIRBUILD\$rfile"
		$ShareFlow = "$RPOSAUDIRBUILD\$ffile"

		
		$URL = "$server/checksum.xml"
		Invoke-WebRequest -Uri "$URL" -OutFile $XMLHash
		
		#Getting HD5 hash numbers
		[xml]$xmlread = Get-Content $XMLhash
		$xr = $xmlread.checksum.RPOS
		[xml]$xmlread = Get-Content $XMLhash
		$xf = $xmlread.checksum.Flow
		Write-Output "$xr is the checksum of RPOS version on $server" | Out-File -Append -FilePath $Log
		Write-Output "$xf is the checksum of FlowModule version on $server" | Out-File -Append -FilePath $Log
		
		####################################################################
		# Checking Tesla Version Added 5/11/2018                           #
		####################################################################
		
		$URLTeslaProton = "$server/TESLA-PROTON.VERSION"
		$URLTeslaElectron = "$server/TESLA-ELECTRON.VERSION"
		Invoke-WebRequest -Uri "$URLTeslaProton" -OutFile $TeslaProtonversion
		Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversion
		$TeslaProtonversionWeb = (Get-Content "$RPOSAUDIRHOME\TESLA-PROTON.VERSION").Substring(8)
		$TeslaElectronversionWeb = (Get-Content "$RPOSAUDIRHOME\TESLA-ELECTRON.VERSION").Substring(8)
		
		$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		
		$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
		$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
		
		Write-Output "$TeslaElectronversionWeb is the version of Tesla Electron on the server" | Out-File -Append -FilePath $Log
		Write-Output "$TeslaProtonversionWeb is the version of Tesla Proton on the server" | Out-File -Append -FilePath $Log
		Write-Output "$TeslaProtonversionLocal is the version of Tesla Proton on the PC now" | Out-File -Append -FilePath $Log
		Write-Output "$TeslaElectronversionLocal is the version of Tesla Electron on the PC now" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "1" -Force
		
		#Downloading Adobe Arh file if needed
		if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
		Else
		{
			$arh = "http://rpos.tbccorp.com/arh.exe"
			Invoke-WebRequest -Uri $arh -OutFile $arhfile
			Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
		}
		
		#Checking if RPOS and Flow Equal or Not Equal to server
		if (($x -eq $verRPOS) -and ($x -eq $verFLOW) -and ($TeslaProtonversionWeb -eq $TeslaProtonversionLocal) -and ($TeslaElectronversionWeb -eq $TeslaElectronversionLocal))
		{
			Write-Output "Nothing to download versions of RPOS and flow and Tesla are current" | Out-File -Append -FilePath $Log
			Set-RPOSModulecheckinstall
			Set-RPOSModuleFindVersion
		}
		Else
		{
			#Starting RPOS and Flow Module Install
			Write-Output "RPOS and Flow and Tesla are different versions - Starting New Install" | Out-File -Append -FilePath $Log
			Set-RPOSModuleInstallPOS
			
			$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
			$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
			$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
			$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
			$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
			if (($sir -gt "0") -or ($sif -gt "0") -or ($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($TeslaProtonversionWeb -ne $TeslaProtonversionLocal) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal))
			{
				Write-Output "There was an error detected with the automatic installer - switching to manual mode and reinstalling" | Out-File -Append -FilePath $Log
				Set-RPOSModuleManualRemove
				Set-RPOSModuleInstallPOSnobackup
				Set-RPOSModulecheckinstall
			}
			Else
			{
				Write-Output "Version passed validation of successful install" | Out-File -Append -FilePath $Log
				Set-RPOSModulecheckinstall
				Set-RPOSModuleFindVersion
			}
			
			
			
			#Checking Install One last time
			$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
			$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
			$testpospath = (Test-Path "$RPOSHOME\RPOS.exe")
			$testflowpath = (Test-Path "$FLOWHOME\FlowModule.exe")
			$TeslaAppDirectory = Get-ChildItem "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
			$CheckingTeslaDirectory = Test-path "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron"
			$CheckingTeslaElectronexe = Test-Path "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron\$teslafolderver\tesla-electron.exe"
			$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
			$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
			if (($verRPOS -ne $x) -or ($verFLOW -ne $x) -or ($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($testflowpath -eq $false) -or ($testpospath -eq $false) -or ($CheckingTeslaDirectory -eq $false) -or ($CheckingTeslaElectronexe -eq $false) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal) -or ($TeslaProtonversionWeb -ne $TeslaProtonversionLocal))
			{
				New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "0" -Force
			}
			Else
			{
				New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "1" -Force
			}
			
			#Value for Progress Bar
			Set-ItemProperty -path "$LANDeskKeys" -Name "AUGatherReportingData" -Value "1" -Force
			
			#CheckRPOSIcon Checking Desktop Icon
			Set-RPOSModuleCheckRPOSIcon
			
		}
		
		#Checking Reporting XML
		Set-RPOSModuleCheckReportXML
		
		Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:600 * RPOS Auto-Updater $ReleaseInfo has finished installing/updating/checking RPOS! The installer is now completed at this time. Thank you TBC EOC Department."
		
		#Getting time of end of script for log file
		$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
		Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log
		
		#Removing IP address text files and DNS names of PC's
		Remove-Item $LiveIP -Force
		Remove-Item $LiveNames -Force
		#cleaning house
		Remove-PSDrive -Name HKLM -Force
		Remove-Item $XMLhash -Force
		Remove-Item $results -Force
		Remove-Item $TeslaProtonversion -Force
		Remove-Item $TeslaElectronversion -Force
	}
}

#######################################################################################################
#Function RPOS-RemoveAULogs
function Get-RPOSRemoveAULogs {
	$Path = "$RPOSAUDIRLOGS"
	$Daysback = "-60"
	
	$CurrentDate = Get-Date
	$DatetoDelete = $CurrentDate.AddDays($Daysback)
	Get-ChildItem $Path | Where-Object { $_.LastWriteTime -lt $DatetoDelete } | Remove-Item
}

#########################################################################################################
#Function RPOS-Rollback
function Get-RPOSRollback
{
	#Setting up varibles from above
	Set-RPOSVar
	
	#ALL VARIABLES THAT ARE IN THIS POWERSHELL SCRIPT CAN BE CHANGED FROM THE FIRST COUPLE ROWS TO REFLECT THE ENTIRE SCRIPT!#
	#Setting Up Varibles
	$date = (get-date).ToString("yyyyMMdd-hhmmss")
	$Log = "$RPOSAUDIRLOGS\RPOS_Rollback_$date.txt"
	$builddir = "$RPOSAUDIRBUILD"
	
	#Fuction Install POS - Default Installer Process for RPOS
	$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
	$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
	$script:drfilebak = Get-ChildItem -path "$RPOSAUDIRBAK" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
	$script:dffilebak = Get-ChildItem -path "$RPOSAUDIRBAK" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
	
	function InstallPOS
	{
		#Removing old RPOS and FlowModule files
		if (($drfile -eq $null) -and ($dffile -eq $null))
		{
			Write-Output "There are no files found in the root directory of $RPOSAUDIRHOME not deleting anything" | Out-File -Append -FilePath $Log
		}
		if (($drfile -eq "RPOS.exe") -or ($dffile -eq "FlowModule.exe") -or ($drfile -eq "RPOS.air") -or ($dffile -eq "FlowModule.air"))
		{
			Write-Output "Deleting RPOS and FlowModule from $RPOSAUDIRHOME" | Out-File -Append -FilePath $Log
			Remove-Item "$RPOSAUDIRHOME\$drfile" -Force
			Remove-Item "$RPOSAUDIRHOME\$dffile" -Force
			Remove-Item "$RPOSAUDIRBUILD\*" -Recurse -Force
		}
		
		#Copying old RPOS Build to install directory and back to the Build directory
		if ($drfilebak -eq "RPOS.bakair")
		{
			Copy-Item -Path $RPOSfileBakair -Destination $RPOSfileair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "RPOS.bakair has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $flowfileBakair -Destination $flowfileair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "FlowModule.bakair has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $RPOSfileair -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
			Copy-Item -Path $flowfileair -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
		}
		
		if ($drfilebak -eq "RPOS.bakexe")
		{
			Copy-Item -Path $RPOSfileBakexe -Destination $RPOSfileexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "RPOS.bakexe has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $flowfileBakexe -Destination $flowfileexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "FlowModule.bakexe has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $RPOSfileexe -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
			Copy-Item -Path $flowfileexe -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
		}
		
		#Downloading Adobe Arh file if needed
		if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
		Else
		{
			$arh = "http://tposdist01.tbccorp.com/arh.exe"
			Invoke-WebRequest -Uri $arh -OutFile $arhfile
			Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
		}
		
		#Killing RPOS and Flow if running
		Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		
		#Uninstalling RPOS
		Write-Output "Starting Uninstall of RPOS" | Out-File -Append -FilePath $Log
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
		$p.HasExited
		$sur = $p.ExitCode
		Write-Output "RPOS silent uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		#Uninstalling Flow
		Write-Output "Starting Uninstall of FlowModule" | Out-File -Append -FilePath $Log
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
		$p.HasExited
		$suf = $p.ExitCode
		Write-Output "FlowModule silent uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		if ($drfilebak -eq "RPOS.bakair")
		{
			#Installing RPOS AIR
			Write-Output "Starting install of RPOS.air" | Out-File -Append -FilePath $Log
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfileair" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "RPOS.air silent install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		if ($drfilebak -eq "RPOS.bakexe")
		{
			#Installing RPOS EXE
			Write-Output "Starting install of RPOS.exe" | Out-File -Append -FilePath $Log
			$p = Start-Process $RPOSfileexe -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "RPOS.exe silent install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		Start-Sleep -s 15
		
		if ($dffilebak -eq "FlowModule.bakair")
		{
			#Installing Flow AIR
			Write-Output "Starting install of FlowModule.air" | Out-File -Append -FilePath $Log
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfileair" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "FlowModule.air silent install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		if ($dffilebak -eq "FlowModule.bakexe")
		{
			#Installing Flow EXE
			Write-Output "Starting install of FlowModule.exe" | Out-File -Append -FilePath $Log
			$p = Start-Process $flowfileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "FlowModule.exe silent install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /V /F
	}
	
	function InstallTesla
	{
		Write-Output "Taking the orginal Tesla file and removing it out of the default directory" | Out-File -Append -FilePath $Log
		Remove-Item $teslafileexe -Force
		Write-Output "Tesla EXE has been removed from C:\Windows\Scripts\RPOS\" | Out-File -Append -FilePath $Log
		Copy-Item $teslafileBakexe -Destination $teslafileexe -Force
		Write-Output "Tesla Backup file has been moved to the default install directory" | Out-File -Append -FilePath $Log
		
		Write-Output "Closing Tesla to start the removing old Tesla Installer" | Out-File -Append -FilePath $Log
		Write-Output "Was Electron running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Write-Output "Was Proton Running?" | Out-File -Append -FilePath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		
		#Start uninstalling and installing tesla
		Remove-Item "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron" -Recurse -Force
		Write-Output "Tesla has been removed from Public AppData folder" | Out-File -Append -Filepath $Log
		Remove-Item "$env:HOMEDRIVE\Users\$env:USERNAME\AppData\Local\tesla-electron" -Recurse -Force
		
		#Starting to install tesla
		Start-Process $teslafileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
		Write-Output "Tesla has completed installing" | Out-File -Append -Filepath $Log
		
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		
		Move-Item "$env:HOMEDRIVE\Users\$env:USERNAME\AppData\Local\tesla-electron" -Destination $PublicAppDataLocalFolder -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "Tesla has been moved over to the user public folder for everyone to use" | Out-File -Append -FilePath $Log
		
	}
	
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d" | Out-File -Append -FilePath $Log
	
	#setting up registry search
	Set-Location Registry::\HKEY_LOCAL_MACHINE
	New-PSDrive HKLM Registry HKEY_LOCAL_MACHINE
	Set-Location HKLM:
	
	#Grabbing version from RPOS and FLOW Registry
	$rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	#Starting RPOS and Flow Module Install
	Write-Output "Starting Roll Back UI Install" | Out-File -Append -FilePath $Log
	InstallPOS
	
	#Starting Tesla Rollback Install
	Write-Output "Starting Roll Back Of Tesla Install" | Out-File -Append -FilePath $Log
	InstallTesla
	
	#Checking for successful install
	Write-Output "Checking to see if Install was successful" | Out-File -Append -FilePath $Log
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	$CheckingTeslaDirectory = Test-path "C:\Users\Public\AppData\Local\tesla-electron"
	$CheckingTeslaElectronexe = Test-Path "C:\Users\Public\AppData\Local\tesla-electron\$teslafolderver\tesla-electron.exe"
	
	
	If (($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($sif -ne "0") -or ($sir -ne "0") -or ($CheckingTeslaDirectory -eq $false) -or ($CheckingTeslaElectronexe -eq $false))
	{
		Write-Output "Found Install was not successful" | Out-File -Append -FilePath $Log
		Write-Output "After install RPOS and Flow Status" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is what the check pulled for RPOS" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is what the check pulled for FlowModule" | Out-File -Append -FilePath $Log
		Write-Output "Running installer again for Flow and RPOS" | Out-File -Append -FilePath $Log
		Remove-Item $flowfileair -ErrorAction SilentlyContinue -Force
		Remove-Item $flowfileexe -ErrorAction SilentlyContinue -Force
		Remove-Item $RPOSfileair -ErrorAction SilentlyContinue -Force
		Remove-Item $RPOSfileexe -ErrorAction SilentlyContinue -Force
		InstallPOS
		InstallTesla
	}
	Else
	{
		Write-Output "RPOS and FlowModule Installed correctly exiting script" | Out-File -Append -FilePath $Log
	}
	
	#Checking the version of RPOS on PC and importing them into the registry
	Set-RPOSModuleFindVersion
	
	#Checking Reporting XML
	Set-RPOSModuleCheckReportXML
	
	#Checking the RPOS Icons on the desktop
	Set-RPOSModuleCheckRPOSIcon
	
	#Getting time of end of script for log file
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log
	
	#cleaning house
	Remove-PSDrive -Name HKLM -Force
	cd C:
	
}

#########################################################################################################
#function RPOS-UpdateAU
function Get-RPOSUpdateAU
{
	#Setting up varibles from above
	Set-RPOSVar
	
	$pcname = $env:COMPUTERNAME
	$pcname = $pcname -replace 'S[0-9][0-9][0-9]'
	$date = (get-date).ToString("yyyyMMdd-hhmmss")
	$Log = "$RPOSAUDIRLOGS\UpdateCheck_$date.txt"
	$install = "$RPOSAUDIRUPDATE\setup.exe"
	$regpath = "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
	New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
	$ver = (Get-ItemProperty -Path $regpath -Name RPOSAUVer).RPOSAUVer
	$dateinstall = (Get-Date).ToString("MM/dd/yyyy")
	
	#Function download update
	function redownload
	{
		Write-Output "Downloading update for RPOS AU" | Out-File -Append -FilePath $Log
		Write-Output "Downloading File $setup from updater server" | Out-File -Append -FilePath $Log
		Invoke-WebRequest -Uri "$setup" -OutFile $install
	}
	
	If (($networkcheck -eq $null) -or ($networkcheck -ne $true))
	{
		Write-Output "AutoUpdater for RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
		Write-Output "RPOS Auto-Updater $ver is showing that there is no current internet connection" | Out-File -Append -FilePath $Log
		Write-Output "with the core server, the Auto-Updater will check again tomorrow" | Out-File -Append -FilePath $Log
	}
	Else
	{
		
		#Getting Update.xml from TLDMS02 to check status
		Write-Output "AutoUpdater for RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
		Write-Output "Downloading Update.xml to check for new install or not" | Out-File -Append -FilePath $Log
		$xml = "$RPOSAUDIRUPDATE\updater.xml"
		$URL = "http://rpos.tbccorp.com/rposautoupdater/updatefiles/updaterqa.xml"
		Invoke-WebRequest -Uri "$URL" -OutFile $xml
		
		#Getting update status TRUE or FALSE
		Write-Output "Starting to read xml files to see if there is an update" | Out-File -Append -FilePath $Log
		[xml]$xmlread = Get-Content $xml
		$x = $xmlread.update.statement
		$y = $xmlread.update.version
		$setup = $xmlread.update.setup
		$hash = $xmlread.update.hash
		
		Write-Output "$x is the statment of what was pulled from updater.exe" | Out-File -Append -FilePath $Log
		Write-Output "$y is the update version that it should be running" | Out-File -Append -FilePath $Log
		Write-Output "$ver is the update verison that is running on the PC" | Out-File -Append -FilePath $Log
		Write-Output "$hash is the hash that has to match the server for PCs to download" | Out-File -Append -FilePath $Log
		
		
		if (($x -eq "true") -and ($y -ne $ver))
		{
			Write-Output "Updater has found that a new version needs to be updated starting download" | Out-File -Append -FilePath $Log
			#Starting download and hash checks for PC1
			
			Write-Output "Starting to download newer copy of RPOS Auto" | Out-File -Append -FilePath $Log
			redownload
			$pchash = Get-FileHash -Path $install -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$pchash is the file hash that was just downloaded from update server" | Out-File -Append -FilePath $Log
			if ($pchash -eq $hash)
			{
				Write-Output "PC hash equals download has download was successful!" | Out-File -Append -FilePath $Log
				Write-Output "Starting install from update file........." | Out-File -Append -FilePath $Log
				Start-Process $install
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value $dateinstall -Force
			}
			Else
			{
				Write-Output "Hash of the download file was not correct attempting to re-download" | Out-File -Append -FilePath $Log
				Start-Sleep -Seconds 60
				redownload
				$pchash = Get-FileHash -Path $install -Algorithm MD5 | select -ExpandProperty hash
				Write-Output "$pchash is the file hash that was just re-downloaded from update server" | Out-File -Append -FilePath $Log
				if ($pchash -eq $hash)
				{
					Write-Output "PC hash equals download has download was successful!" | Out-File -Append -FilePath $Log
					Write-Output "Starting install from update file........." | Out-File -Append -FilePath $Log
					Start-Process $install
					Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value $dateinstall -Force
				}
				Else
				{
					Write-Output "Redownload was not successful twice, updater will wait until tomorrow" | Out-File -Append -FilePath $Log
				}
			}
		}
		Else
		{
			Write-Output "Updater has not found an update at this time exiting updater" | Out-File -Append -FilePath $Log
		}
		
		#################################################################
		# Added 5/22/2018 for Tesla Supported Install                   #
		#################################################################
		# Clearing Tesla Database during check of RPOS AU Update
		Set-RPOSModuleTeslaClearDatabase
		
		#Checking RPOS AutoHeal to make sure that the install is broken
		Start-Process "Powershell.exe" -ArgumentList "Get-RPOSAutoHeal"
		
		Remove-PSDrive -Name HKLM -Force
	}
}

################################################################################################################
#function Get-RPOSStatusInstall
function Get-RPOSStatusInstall
{
	Set-RPOSVar
	
	#################################################START FUNCTION INSTALLCHECK###########################################
	
	$Host.UI.RawUI.WindowTitle = 'RPOS Install Progress Screen'
	
	#Checking to see if Scheduled Task Has started yet
	while ($checkstart -lt "10000000")
	{
		$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
		if ($checkinternetreg -eq "0")
		{
			$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Please Wait For Installer To Start" -PercentComplete 0
			Start-Sleep -Seconds 1
		}
		if ($checkinternetreg -eq "1")
		{
			Break
		}
	}
	
	
	function InstallCheck
	{
		<#Checking for Copying Files over from Local PC or Internet and self elected subnet sharing
		while ($checkselfelectdownload -lt "10000000")
		{
			$checkselfelectdownload += 1
			$checkselfelectdownloadreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckSelfSubDownload")."AUCheckSelfSubDownload"
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Copying Setup Files" -PercentComplete 40
			if ($checkselfelectdownloadreg -eq "0")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Scanning Local PCs And/Or Download RPOS Setup Files." -PercentComplete 45
				Start-Sleep -Seconds 1
			}
			if ($checkselfelectdownloadreg -eq "1")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Setup Files Have Been Copied Over And/Or Download Ready For Install." -PercentComplete 50
				Start-Sleep -Seconds 1
				break
			}
			
		}#>
		
		#Checking any open RPOS and FlowModule Windows that may be open
		while ($checkopenwindows -lt "10000000")
		{
			$checkopenwindows += 1
			$checkopenwindowsreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUClosingWindows")."AUClosingWindows"
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking Open Sessions" -PercentComplete 52
			if ($checkopenwindowsreg -eq "0")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Waiting To Close Active Sessions." -PercentComplete 55
				Start-Sleep -Seconds 1
			}
			if ($checkopenwindowsreg -eq "1")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "RPOS And FlowModule Have Been Closed." -PercentComplete 60
				Start-Sleep -Seconds 1
				break
			}
			
		}
		
		#Showing Uninstall Process of RPOS and FlowModule
		while ($checkuninstallrpos -lt "10000000")
		{
			$checkuninstallrpos += 1
			$checkuninstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUUninstallRPOS")."AUUninstallRPOS"
			
			if ($checkuninstallrposreg -eq "0")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Uninstalling RPOS" -PercentComplete 65
				Start-Sleep -Seconds 1
			}
			if ($checkuninstallrposreg -eq "1")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "RPOS Uninstall Complete" -PercentComplete 70
				Start-Sleep -Seconds 1
				break
			}
		}
		
		#Showing Install Process of RPOS and FlowModule
		while ($checkinstallrpos -lt "10000000")
		{
			$checkinstallrpos += 1
			$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingRPOS")."AUInstallingRPOS"
			
			if ($checkinstallrposreg -eq "0")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Installing RPOS" -PercentComplete 72
				Start-Sleep -Seconds 1
			}
			if ($checkinstallrposreg -eq "1")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "RPOS Install Complete" -PercentComplete 75
				Start-Sleep -Seconds 1
				break
			}
		}
		
		#Showing Install Process of Tesla
		while ($checkinstalltesla -lt "10000000")
		{
			$checkinstalltesla += 1
			$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingTesla")."AUInstallingTesla"
			
			if ($checkinstallrposreg -eq "0")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Installing Tesla Middleware" -PercentComplete 77
				Start-Sleep -Seconds 1
			}
			if ($checkinstallrposreg -eq "1")
			{
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Tesla Middleware Install Complete" -PercentComplete 80
				Start-Sleep -Seconds 1
				break
			}
		}
		
	}
	
	####################################################END OF INSTALLCHECK#################################################
	
	#Checking to see if the internet connection is found on the PC and showing status
	while ($checkinternet -lt "10000000")
	{
		$checkinternet += 1
		$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
		if ($checkinternetreg -eq "0")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking Internet Connection" -PercentComplete 5
			Start-Sleep -Seconds 1
		}
		if ($checkinternetreg -eq "1")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Internet Connection Was Found" -PercentComplete 10
			Start-Sleep -Seconds 1
			break
		}
		
		if ($checkinternetreg -eq "2")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Internet Connection No Internet Install Exiting" -PercentComplete 1
			Start-Sleep -Seconds 1
			exit
		}
		
	}
	
	#Checking to see if Adobe AIR is installed and current - Showing Status on PowerShell
	while ($checkadobeair -lt "10000000")
	{
		$checkadobeair += 1
		$checkadobeairreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckAdobeAIR")."AUCheckAdobeAIR"
		if ($checkadobeairreg -eq "0")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking For Current Configuration RPOS, FlowModule, Tesla!" -PercentComplete 15
			Start-Sleep -Seconds 1


		}
		if ($checkadobeairreg -eq "1")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking Information For Current Configuration Complete!" -PercentComplete 20
			Start-Sleep -Seconds 1
			Break
		}
	}

	
	#Checking to see all settings varibles are going into the Update
	while ($checkbginfoemv -lt "10000000")
	{
		$checkbginfoemv += 1
		$checkbginfoemvreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckBGInfoEMV")."AUCheckBGInfoEMV"
		Write-Progress -Activity "Installing RPOS " -CurrentOperation "Getting Ready To Download RPOS and FlowModule." -PercentComplete 22
		
		if ($checkbginfoemvreg -eq "0")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Download of RPOS and FlowModule In Process!" -PercentComplete 25
			Start-Sleep -Seconds 1
		}
		if ($checkbginfoemvreg -eq "1")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Download of RPOS and FlowModule Are Complete!" -PercentComplete 30
			Start-Sleep -Seconds 1
			break
		}
		
	}


#Calling Install Check Progress Bar
	InstallCheck
	
	#Validating that install was successful or not
	while ($checkinstall -lt "1000")
	{
		$checkinstall += 1
		$checkinstallreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInstall")."AUCheckInstall"
		$aufailedlocal = (Get-ItemProperty -Path $LANDeskKeys -Name "AUFailedLocal")."AUFailedLocal"
		Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking For Successful Install" -PercentComplete 82
		
		if ($checkinstallreg -eq "0")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Validating Install" -PercentComplete 85
			Start-Sleep -Seconds 1
		}
		
		
		#If Validation is not SUCCESSFUL 	
		if ($aufailedlocal -eq "1")
		{
			while ($aufailedcount -lt "10000000")
			{
				$aufailedcount += 1
				$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
				Write-Progress -Activity "Installing RPOS " -CurrentOperation "Validation Failed - Removing Install And Reinstalling." -PercentComplete 62
				
				if ($LocalRPOSReInstall -eq "1")
				{
					$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
					$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
					Write-Progress -Activity "Installing RPOS " -CurrentOperation "Removing Install Files And Reinstalling." -PercentComplete 65
					Start-Sleep -Seconds 1
					
					#If Local Install Failed 
					if (($LocalRPOSReInstall -eq "1") -and ($AUCompleteReinstalReg -eq "1"))
					{
						Write-Progress -Activity "Installing RPOS " -CurrentOperation "Removing Install Files And Reinstalling." -PercentComplete 35
						Start-Sleep -Seconds 1
						Set-ItemProperty -path $LANDeskKeys -Name "AUCheckSelfSubDownload" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUClosingWindows" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUUninstallRPOS" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUInstallingRPOS" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUCheckInstall" -Value "0" -Force
						
						#Running Install Check to Run the Installers again.
						InstallCheck
					}
					
					if (($LocalRPOSReInstall -eq "1") -and ($AUCompleteReinstalReg -eq "1"))
					{
						$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
						$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
						Write-Progress -Activity "Installing RPOS " -CurrentOperation "Reinstall Complete" -PercentComplete 80
						Start-Sleep -Seconds 1
						Break
					}
					
				}
				
				if ($LocalRPOSReInstall -eq "1")
				{
					#Value for Progress Bar
					$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
					Write-Progress -Activity "Installing RPOS " -CurrentOperation "Reinstall Completed" -PercentComplete 80
					Start-Sleep -Seconds 1
					Break
				}
				
				Break
				
			}
			Break
		}
		
		if ($checkinstallreg -eq "1")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Install Completed" -PercentComplete 90
			Start-Sleep -Seconds 1
			Break
		}
	}
	
	
	#Gathering Install Data and Sending it off to RPOS.TBCCORP.COM
	while ($checkreporting -lt "10000000")
	{
		$checkreporting += 1
		$checkreportingdatareq = (Get-ItemProperty -Path $LANDeskKeys -Name "AUGatherReportingData")."AUGatherReportingData"
		
		if ($checkreportingdatareq -eq "0")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Gathering Report Data Information" -PercentComplete 93
			Start-Sleep -Seconds 1
			
		}
		if ($checkreportingdatareq -eq "1")
		{
			Write-Progress -Activity "Installing RPOS " -CurrentOperation "Reporting Complete" -PercentComplete 95
			Start-Sleep -Seconds 1
			break
		}
	}
	
	Write-Progress -Activity "Installing RPOS " -CurrentOperation "Software Installed" -PercentComplete 100
	Start-Sleep -Seconds 20
	Exit
}

################################################################################################################
<#function RPOS-DesktopUpdate
function Get-RPOSDesktopUpdate
{
	#Setting up varibles from above
	Set-RPOSVar
	
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
	
	##########################################################################################################################################
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d"
	Write-Output "RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
	
	#Finding out if task is enabled or disabled in scheduled task
	$showtaska = schtasks /Query /TN "Update RPOS Local A" | Select-String -Pattern "Disabled"
	$showtaskb = schtasks /Query /TN "Update RPOS Local B" | Select-String -Pattern "Disabled"
	
	if ($showtaska -eq $null) {
	   $task = "A"}
	
	if ($showtaskb -eq $null) {
		$task = "B" }
	
	#setting up registry search
	New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE

	#Checking to see if the desktop installer is running already to not create a duplicate install
	$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
	
	if ($processcheck -ne $null)
	{
		[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
	}
	if ($processcheck -eq $null)
	{
		#Grabbing version from RPOS and FLOW Registry
		$script:rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
		$script:verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$script:verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		
		#Grabbing files names in RPOS directory
		$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
		$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
		
		#Checking to see what PC name is assoicated with a Store, TAC, Carroll Tire, Or Corprate Owned
		Set-RPOSModuleWhatPCAmI
		
		#Checking XML update files checksums
		$URL = $checkupdatedXMLs
		$script:xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
		$checkswitchxml = $xmlread.filechecksum.switchxml.$XMLswitchchecksum
		$checkBGInfoxml = $xmlread.filechecksum.BGInfoxml.$XMLbginfochecksum
		
		
		Write-Output "$switchvalue is the switch value."
		Write-Output "$rposname is the version of RPOS running on this PC"
		
		#checking to see if switch file has changed and if it has to switch the RPOS version
		if ($checkswitchxml -ne $RegSwitchXMLchecksum)
		{
			
			Write-Output "There is a different switchxml on RPOS.tbccorp.com checking xml and seeing if there is a new distro change"
			
			#Checking to see if there is a switch in the builds
			$URL = $XMLSwitchURL
			
			#Getting switch build response
			$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			$w = $xmlread.serverswitch.$switchvalue.name
			$x = $xmlread.serverswitch.$switchvalue.statement
			$y = $xmlread.serverswitch.$switchvalue.server
			Write-Output "Switch build revision equals $x" | Out-File -Append -FilePath $Log
			
			#Getting Tesla version information from server
			$URLTeslaProton = "http://$y/TESLA-PROTON.VERSION"
			$URLTeslaElectron = "http://$y/TESLA-ELECTRON.VERSION"
			Invoke-WebRequest -Uri "$URLTeslaProton" -OutFile $TeslaProtonversionstandard
			Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversionstandard
			$TeslaProtonversionWeb = (Get-Content "$TeslaProtonversionstandard").Substring(8)
			$TeslaElectronversionWeb = (Get-Content "$TeslaElectronversionstandard").Substring(8)
			
			$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
			
			$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
			$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
			if ($w -ne $rposname)
			{
				[System.Windows.Forms.MessageBox]::Show("WARNING!!!`n `nRPOS needs to be updated to $w and Tesla needs to be updated to $TeslaElectronversionWeb. `n `nComplete all work before starting the update process. `n `nRPOS will be closed and unavailable during this update. `n `nAllow 5-10 minutes for the process to complete. `nA message will be displayed when the update is completed. `nClick OK to begin the update process.", "Update RPOS")
			}
			
			$usersprofile1 = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
			foreach ($userprofile1 in $usersprofile1)
			{
				Remove-Item "C:\Users\$userprofile1\Desktop\RPOS.lnk"
				Remove-Item "C:\Users\$userprofile1\Desktop\FlowModule.lnk"
				
			}
			
			schtasks /Run /TN "Update RPOS Local $task"
			Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
			Start-Sleep -Seconds 120
			$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
			
			#$process = Get-Process updaterpos | Select-Object CPU, Name, Starttime
			while ($true)
			{
				if ($process -eq $null)
				{
					$installsuccess1 = (Get-ItemProperty -Path $LANDeskKeys -Name InstallComplete).InstallComplete
					
					if ($installsuccess1 -eq "1")
					{
						[System.Windows.Forms.MessageBox]::Show("RPOS was successfully updated to the latest version. ", "Update RPOS")
						Break
					}
					if ($installsuccess1 -eq "0")
					{
						[System.Windows.Forms.MessageBox]::Show("RPOS was NOT successfully updated to the latest version. Please try again in a few minutes.", "Update RPOS")
						Break
					}
				}
				Else
				{
					#Starting sleepto check the scheduled task again and if RPOS and/or FlowModule are running to close it.
					Start-Sleep -Seconds 30
					$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
				}
			}
		}
		
		if ($checkswitchxml -eq $RegSwitchXMLchecksum)
		{
			Write-Output "There is no new switch.xml on RPOS.tbccorp.com, bypassing switching distro to checking for newer version of RPOS without switching."
			Write-Output "Powershell has found no distro change needed. Version upgrade check starting"
			
			#Pulling update xml file from server to compare local and server update.xml
			if ($rposname -eq "RPOS - PRODUCTION")
			{
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - TRAINING")
			{
				$URL = "http://$train/update.xml"
				$server = "http://$train"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - PILOT")
			{
				$URL = "http://$pilot/update.xml"
				$server = "http://$pilot"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - PILOT2")
			{
				$URL = "http://$pilot2/update.xml"
				$server = "http://$pilot2"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - PILOT3")
			{
				$URL = "http://$pilot3/update.xml"
				$server = "http://$pilot3"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QA")
			{
				$URL = "http://$qa/update.xml"
				$server = "http://$qa"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq $null)
			{
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QAPILOT")
			{
				$URL = "http://$qapilot/update.xml"
				$server = "http://$qapilot"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QAPILOT2")
			{
				$URL = "http://$qapilot2/update.xml"
				$server = "http://$qapilot2"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - QAPILOT3")
			{
				$URL = "http://$qapilot3/update.xml"
				$server = "http://$qapilot3"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - ALPHA")
			{
				$URL = "http://$alpha/update.xml"
				$server = "http://$alpha"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEV")
			{
				$URL = "http://$dev/update.xml"
				$server = "http://$dev"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEVPILOT")
			{
				$URL = "http://$devpilot/update.xml"
				$server = "http://$devpilot"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEVPILOT2")
			{
				$URL = "http://$devpilot2/update.xml"
				$server = "http://$devpilot2"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			if ($rposname -eq "RPOS - DEVPILOT3")
			{
				$URL = "http://$devpilot3/update.xml"
				$server = "http://$devpilot3"
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			}
			
			#Getting version number 
			$x = $xmlread.update.versionNumber
			$y = $xmlread.update.versionLabel
			Write-Output "$x is the version pulled on $server"
			Write-Output "$verRPOS is the version of RPOS pulled on PC"
			Write-Output "$verFLOW is the version of Flow pulled on PC"
			
			#Getting Tesla version information from server
			$URLTeslaProton = "$server/TESLA-PROTON.VERSION"
			$URLTeslaElectron = "$server/TESLA-ELECTRON.VERSION"
			Invoke-WebRequest -Uri "$URLTeslaProton" -OutFile $TeslaProtonversionstandard
			Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversionstandard
			$TeslaProtonversionWeb = (Get-Content "$TeslaProtonversionstandard").Substring(8)
			$TeslaElectronversionWeb = (Get-Content "$TeslaElectronversionstandard").Substring(8)
			
			$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
			
			$TeslaProtonversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-PROTON.VERSION").Substring(8)
			$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
			#Checking if RPOS and Flow Equal or Not Equal to server
			if (($x -eq $verRPOS) -and ($x -eq $verFLOW) -and ($TeslaElectronversionWeb -eq $TeslaElectronversionLocal) -and ($TeslaProtonversionWeb -eq $TeslaElectronversionLocal))
			{
				[System.Windows.Forms.MessageBox]::Show("No new version available. ", "Update RPOS")
			}
			Else
			{
				
				[System.Windows.Forms.MessageBox]::Show("WARNING!!!`n `nRPOS needs to be updated to the latest version $y and Tesla needs to be updated to $TeslaElectronversionWeb. `n `nComplete all work before starting the update process. `n `nRPOS will be closed and unavailable during this update. `n `nAllow 5-10 minutes for the process to complete. `nA message will be displayed when the update is completed. `nClick OK to begin the update process.", "Update RPOS")
				Stop-Process -force -passthru -processname rpos*
				Stop-Process -force -passthru -processname FlowModule*
				
				$usersprofile1 = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
				foreach ($userprofile1 in $usersprofile1)
				{
					Remove-Item "C:\Users\$userprofile1\Desktop\RPOS.lnk"
					Remove-Item "C:\Users\$userprofile1\Desktop\FlowModule.lnk"
					
				}
				
				schtasks /Run /TN "Update RPOS Local $task"
				Start-Process "Powershell.exe" -ArgumentList "Get-RPOSStatusInstall" -WindowStyle Normal
				Start-Sleep -Seconds 120
				$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
				
				#$process = Get-Process updaterpos | Select-Object CPU, Name, Starttime
				while ($true)
				{
					if ($process -eq $null)
					{
						$installsuccess1 = (Get-ItemProperty -Path $LANDeskKeys -Name InstallComplete).InstallComplete
						
						if ($installsuccess1 -eq "1")
						{
							[System.Windows.Forms.MessageBox]::Show("RPOS was successfully updated to the latest version. ", "Update RPOS")
							Break
						}
						if ($installsuccess1 -eq "0")
						{
							[System.Windows.Forms.MessageBox]::Show("RPOS was NOT successfully updated to the latest version. Please try again in a few minutes.", "Update RPOS")
							Break
						}
					}
					Else
					{
						#Starting sleepto check the scheduled task again and if RPOS and/or FlowModule are running to close it.
						Start-Sleep -Seconds 30
						$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
						
					}
					
				}
			}
		}
		
		
		#Removing IP address text files and DNS names of PC's
		Remove-Item $LiveIP -Force
		Remove-Item $LiveNames -Force
		#cleaning house
		Remove-PSDrive -Name HKLM -Force
		Remove-Item $XMLhash -Force
		Remove-Item $results -Force
		Remove-Item $TeslaProtonversion -Force
		Remove-Item $TeslaElectronversion -Force
		Remove-Item $TeslaElectronversionstandard -Force
		Remove-Item $TeslaProtonversionstandard -Force
	}
}
#>
